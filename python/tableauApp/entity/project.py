from app import db

class Project(db.Model):
    __tablename__ = 'project'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    project_id = db.Column(db.String())
    parent_id = db.Column(db.String())
    content_permisions = db.Column(db.String())
    server = db.Column(db.String())


    def __init__(self, name, description, project_id, parent_id, content_permisions, server):
        self.name = name
        self.description = description
        self.project_id = project_id
        self.parent_id = parent_id
        self.content_permisions = content_permisions
        self.server = server

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'project id': self.project_id,
            'project name': self.name,
            'project description': self.description
        }