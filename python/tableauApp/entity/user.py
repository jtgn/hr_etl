from app import db

class User(db.Model):
    __tablename__ = 'user_tableau'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    password = db.Column(db.Binary())
    site = db.Column(db.String())
    server =db.Column(db.String())
    slug = db.Column(db.String())
    login_user = db.Column(db.String())

    def __init__(self, name, password, server, slug, login_user):
        self.name = name
        self.password = password
        self.server = server
        self.slug = slug
        self.login_user = login_user

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'password': self.password,
            'site':self.site
        }
