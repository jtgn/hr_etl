from app import db

class Workbook(db.Model):
    __tablename__ = 'workbook'

    id = db.Column(db.Integer, primary_key=True)
    content_url = db.Column(db.String())
    workbook_id= db.Column(db.String())
    work_name = db.Column(db.String())
    work_owner_id = db.Column(db.String())
    preview_image = db.Column(db.Binary())
    image_str = db.Column(db.String())
    project_id = db.Column(db.String())
    project_name = db.Column(db.String())
    size = db.Column(db.String())
    slug = db.Column(db.String())
    git_status = db.Column(db.String())
    user = db.Column(db.String())
    server = db.Column(db.String())

    def __init__(self, content_url, work_id, name, owner, preview_image, img, project_id, project_name, slug, git_status, user, server):
        self.content_url = content_url
        self.workbook_id = work_id
        self.work_name = name
        self.work_owner_id = owner
        self.preview_image = preview_image
        self.image_str = img
        self.project_id = project_id
        self.project_name = project_name
        self.slug = slug
        self.git_status = git_status
        self.user = user
        self.server = server


    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'Workbook id': self.workbook_id,
            'Workbook name': self.work_name,
            'project id': self.project_id,
            'project name':self.project_name
        }