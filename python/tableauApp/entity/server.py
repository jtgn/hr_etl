from app import db

class Server(db.Model):
    __tablename__ = 'server'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    url = db.Column(db.String())
    slug = db.Column(db.String())
    ip_address = db.Column(db.String())
    ssh_user = db.Column(db.String())
    ssh_pass = db.Column(db.String())
    os = db.Column(db.String())
    uptime = db.Column(db.String())
    cpu = db.Column(db.String())
    boot_time = db.Column(db.String())
    disk_available = db.Column(db.String())
    disk_used = db.Column(db.String())
    disk_used_percent = db.Column(db.String())
    disk_device = db.Column(db.String())
    mem_available = db.Column(db.String())
    barColor = db.Column(db.String())
    mem_total = db.Column(db.String())
    mem_used = db.Column(db.String())
    mem_used_perc = db.Column(db.String())
    mem_barColor = db.Column(db.String())


    def __init__(self, name, url, slug, ip, user, passw):
        self.name = name
        self.url = url
        self.slug = slug
        self.ip_address = ip
        self.ssh_user = user
        self.ssh_pass = passw

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name
        }