from app import db

class Datasource(db.Model):
    __tablename__ = 'datasource'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    datasource_id = db.Column(db.String())
    datasource_type = db.Column(db.String())
    project_id = db.Column(db.String())
    project_name = db.Column(db.String())
    content_url = db.Column(db.String())
    updated_at = db.Column(db.String())
    git_status = db.Column(db.String())
    user = db.Column(db.String())
    server = db.Column(db.String())

    def __init__(self, name, datsource_id, datasource_type, project_id, project_name, content_url, updated_at, git_status, user, server):
        self.name = name
        self.datasource_id = datsource_id
        self.datasource_type = datasource_type
        self.project_id = project_id
        self.project_name = project_name
        self.content_url = content_url
        self.updated_at = updated_at
        self.git_status = git_status
        self.user = user
        self.server = server

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'datasource id': self.datasource_id,
            'datasource name': self.name,
            'project id': self.project_id,
            'project_name':self.project_name
        }