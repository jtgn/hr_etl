import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = 'secret'
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    REPO_PATH_URL = os.environ['REPO_PATH']
    LDAP_HOST = 'black'
    LDAP_BASE_DN = 'dc=xpand-it,dc=com'
    LDAP_USER_DN = 'ou=People'
    DAP_USER_LOGIN_ATTR = 'uid'
    LDAP_BIND_USER_DN = 'cn=tableau,dc=xpand-it,dc=com'
    LDAP_BIND_USER_PASSWORD = 'IgKoM5Lbhs'
    LDAP_SEARCH_FOR_GROUPS = False
    LDAP_USER_OBJECT_FILTER = '(objectclass=inetOrgPerson)'


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
