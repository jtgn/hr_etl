from flask import Flask, request, jsonify, render_template, abort, url_for, redirect, Blueprint
import urllib.parse
from flask_login import current_user
from extensions import User
from ssh.client import SSH_Client
import json


server_blueprint = Blueprint('servervw', __name__)

from model.serverRepository import ServerRepository

list_servers = lambda: ServerRepository.listServer()


@server_blueprint.route("/server/list", methods=["GET"])
def listservers():
    '''
    List of servers
    :return: template that lists the users available
    '''
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    server = updateServerInfo(list_servers())
    if request.method == "GET":
        return render_template("/tableau/server/serverlist.html", servers=server, image=image)
    else:
        abort(404)



@server_blueprint.route("/server/add", methods=["GET", "POST"])
def addserver():
    '''
    Form to add new servers
    :return:
    '''
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    if request.method == "GET":
        return render_template("/tableau/server/servercreate.html")
    try:
        exists = ServerRepository.serverExists(url=urllib.parse.quote(request.form.get('url')))
        if (exists is None):
            server = ServerRepository.addServer(request,name=request.form.get('serverName'), url=request.form.get('url'), ip=request.form.get('ip-add'), user=request.form.get('ssh-username'), passw=request.form.get('ssh-password'))
            return redirect(url_for(".listservers"))
        else:
            '''Handle exception print message if the user exists'''
            return redirect(url_for(".listservers"))
    except Exception as e:
        return(str(e))


@server_blueprint.route("/server/<name>/delete", methods=["POST"])
def deleteuser(name):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    server = ServerRepository.deleteServer(name=name)
    if not server:
        abort(404)
    return redirect(url_for(".listservers"))

@server_blueprint.route("/server/<name>/edit", methods=["GET", "POST"])
def serveredit(name):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    server = ServerRepository.getserverbyname(name)

    if not server.name:
        abort(404)

    if request.method == "GET":
        return render_template("tableau/server/serveredit.html", server=server, image=image)

    ServerRepository.updateServer(server,name=request.form.get('servername'), url=request.form.get('serverurl'), ip=request.form.get('ip-add'), user=request.form.get('ssh-username'), passw=request.form.get('ssh-password'))
    return redirect(url_for(".listservers"))


def updateServerInfo(connections):
    for conn in connections:
        server = ServerRepository.getserverbyurl(conn.url)
        if not (server.ip_address is None):
            ssh = SSH_Client(ip=server.ip_address, user=server.ssh_user, passw=server.ssh_pass)
            shel = ssh.connect()
            if(shel != b''):
                value = shel.decode('utf8')
                jsonObj = json.loads(value)
                conn.os = jsonObj['OS']
                conn.uptime = jsonObj['Uptime']
                conn.cpu = jsonObj['CPU Name']
                conn.mem_available = jsonObj['Memory Available']
                conn.disk_available = jsonObj['Disk Available']
                conn.disk_used = jsonObj['Disk Used']
                conn.disk_used_percent = jsonObj['Disk Used Perc']
                conn.disk_device = jsonObj['Disk Device']
                conn.mem_total = jsonObj['Memory Total']
                conn.mem_used = jsonObj['Memory Used']
                conn.mem_used_perc = jsonObj['Memory Used Perc']
                conn.barColor = formatColor(conn.disk_used_percent)
                conn.mem_barColor = formatColor(conn.mem_used_perc)
            else:
                conn.os=''
                conn.uptime=''
                conn.cpu=''
                conn.mem_available=''
                conn.disk_available=''
                conn.disk_used = ''
                conn.disk_used_percent = '0'
                conn.disk_device = ''
                conn.mem_total = ''
                conn.mem_used = ''
                conn.mem_used_perc = ''
        else:
            conn.os=''
            conn.uptime=''
            conn.cpu=''
            conn.mem_available=''
            conn.disk_available=''
            conn.disk_used = ''
            conn.disk_used_percent = '0'
            conn.disk_device = ''
            conn.mem_total = ''
            conn.mem_used = ''
            conn.mem_used_perc = ''

    return connections


def formatColor(value):
    if (value < 50):
        return 'success'
    elif (value > 50 and value < 75):
        return 'warning'
    else:
        return 'danger'