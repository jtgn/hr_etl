from flask import Flask, request, jsonify, render_template, abort, url_for, redirect, Blueprint
from model.DatasourceRepository import DatasourceRepository
from model.userRepository import UserRepository
from tableau.authenticate import Auth
from tableau.datasources import Datasources
from github.gitsync import GitSync
from tableau.datasources import repo
from model.serverRepository import ServerRepository
from model.projectRepository import ProjectRepository
from tableau.projects import Projects
from config import Config
from flask_login import current_user
from extensions import User

datasource_blueprint = Blueprint('datasourcevw', __name__)

list_users = lambda: UserRepository.listusers()
list_server = lambda: ServerRepository.listServer()
list_project = lambda: ProjectRepository.listuniqueprojects()

auth = Auth()


@datasource_blueprint.route("/datasource/<slug>/list", methods=["GET"])
def listdatasources(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)

    if request.method == "GET":
        addalldatasource(slug)
        addallprojects(slug)
        user, server = getuserandserver(slug)
        list_datasources = lambda: DatasourceRepository.list_datasources(user.name, server.server_address)
        updatedatasourcesstatus(list_datasources())
        return render_template("/tableau/listdatasources.html", datasources=list_datasources(), user=slug, users=list_users(), servers=list_server(), projects=list_project(), image=image)
    else:
        abort(404)


@datasource_blueprint.route("/datasource/<slug>/<datasource>/download", methods=["GET"])
def downloaddatasource(slug, datasource):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            auth = Auth()
            server = auth.tableauSignIn(user)
            data = Datasources()
            data.downloadDatasource(datasource, server=server);
            return redirect(url_for(".listdatasources", slug=slug, image=image))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


@datasource_blueprint.route("/datasource/<slug>/addall")
def addalldatasource(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            server = auth.tableauSignIn(user)
            data = Datasources()
            data = data.addAllDatasources(server=server, user=user)
            return "There was loaded {} datasources from site: ".format(len(data[0]))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


@datasource_blueprint.route("/datasource/<slug>/<data>/commit",methods=["GET","POST"])
def datasourcecommit(slug, data):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        return print("datasourcename")
    else:
        try:
            path = 'python/datasources/'+data+'.tdsx'
            commit_message = request.form.get('commit')
            print(commit_message)
            git = GitSync.git_push(repo, path, commit_message)
            print(git)
            return redirect(url_for(".listdatasources", slug=slug, image=image))
        except Exception as e:
            return str(e)


@datasource_blueprint.route("/datasource/<slug>/<data>/upload",methods=["GET","POST"])
def datasourceupload(slug, data):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        return print("datasourcename")
    else:
        try:
            path = Config.REPO_PATH_URL+'/python/datasources/'+data+'.tdsx'
            datasource_name = data
            user = UserRepository.getuserbyname(request.form.get('username'))
            project = ProjectRepository.getprojectbynameserver(request.form.get('projectname'), request.form.get('servername'))
            if(project is None) :
                return str("project does not exists on server")
            else:
                if not (user is None):
                    auth = Auth()
                    server = auth.tableauSignIn(user)
                    data = Datasources()
                    project_id = project.project_id
                    data.publishdatasource(server, project_id, path, datasource_name)
                    return redirect(url_for(".listdatasources", slug=slug, image=image))
                else:
                    return str("user doesn't exist")
                return redirect(url_for(".listdatasources", slug=slug, image=image))
        except Exception as e:
            return str(e)


def addallprojects(slug):
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            server = auth.tableauSignIn(user)
            data = Projects()
            data = data.addAllProjects(server=server)
            return "There was loaded {} datasources from site: ".format(len(data[0]))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


def updatedatasourcesstatus(list):
    for datasource in list :
        Datasources.updatedatasourcestatus(Datasources(), datasource.name)
    return 'true'


def getuserandserver(slug):
    user = UserRepository.getuserbyslug(slug)
    server = auth.tableauSignIn(user)
    return user, server


