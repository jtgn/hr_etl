from flask import Flask, request, jsonify, render_template, abort, url_for, redirect, Blueprint
from model.workbookRepository import WorkbookRepository
from model.userRepository import UserRepository
from tableau.authenticate import Auth
from tableau.workbooks import Workbooks
from github.gitsync import GitSync
from tableau.workbooks import repo
from model.serverRepository import ServerRepository
from model.projectRepository import ProjectRepository
from tableau.projects import Projects
from config import Config
from flask_login import current_user
from extensions import User
import os

workbook_bp = Blueprint('workbookvw', __name__)

'''Lambda function to get the list of workbooks'''
list_users = lambda: UserRepository.listusers()
list_server = lambda: ServerRepository.listServer()
list_project = lambda: ProjectRepository.listuniqueprojects()

auth = Auth()

@workbook_bp.route("/workbooks/<slug>/list", methods=["GET", "POST"])
def listworkbooks(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        addallworkbooks(slug)
        addallprojects(slug)
        user, server = getuserandserver(slug)
        list_workbooks = lambda : WorkbookRepository.list_workbooks(user.name, server.server_address)
        updateworkbookstatus(list_workbooks())
        return render_template("/tableau/listworkbooks.html", workbooks=list_workbooks(), user=slug , users=list_users(), servers=list_server(), projects=list_project(), image=image)
    else:
        abort(404)


@workbook_bp.route("/workbooks/<slug>/upload", methods=["GET", "POST"])
def listworkbooksapp(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    if request.method == "POST":
        return "Hello"
    else:
        abort(404)


@workbook_bp.route("/workbooks/<slug>/<workbook>/download", methods=["GET"])
def downloadworkbooks(slug, workbook):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            auth = Auth()
            server = auth.tableauSignIn(user)
            workb = Workbooks()
            workb.downloadworkbook(workbook, server=server)
            return redirect(url_for(".listworkbooks", slug=slug, image=image))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


@workbook_bp.route("/workbooks/<slug>/addall")
def addallworkbooks(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            server = auth.tableauSignIn(user)
            data = Workbooks()
            data = data.getallworkbooks(server=server, user=user)
            return "There was loaded {} workbooks from site: ".format(len(data[0]))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


@workbook_bp.route("/workbooks/<slug>/<data>/commit", methods=["GET", "POST"])
def workbookcommit(slug, data):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        return print(data)
    else:
        try:
            path1, path2 = 'python/workbooks/'+data+'.twbx', 'python/workbooks/'+data+'.twb'
            if os.path.exists((Config.REPO_PATH_URL+'/'+path1).replace('(', '').replace(')', '').replace('+','')):
                path = path1
            else:
                path = path2

            commit_message = request.form.get('commit_workbook')
            print(commit_message)
            git = GitSync.git_push(repo, path, commit_message)
            print(git)
            return redirect(url_for(".listworkbooks", slug=slug, image=image))
        except Exception as e:
            return str(e)


@workbook_bp.route("/workbooks/<slug>/<data>/upload", methods=["GET", "POST"])
def workbookupload(slug, data):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        return print("datasourcename")
    else:
        try:
            path1, path2 = Config.REPO_PATH_URL+'/python/workbooks/'+data+'.twbx', Config.REPO_PATH_URL+'/python/workbooks/'+data+'.twb'
            if os.path.exists(path1):
                path = path1
            else:
                path = path2
            #path = Config.REPO_PATH_URL+'/python/datasources/'+data+'.tdsx'
            workbook_name = data
            user = UserRepository.getuserbyname(request.form.get('username_work'))
            project = ProjectRepository.getprojectbynameserver(request.form.get('projectname_work'), request.form.get('servername_work'))
            if(project is None) :
                return str("project does not exists on server")
            else:
                if not (user is None):
                    auth = Auth()
                    server = auth.tableauSignIn(user)
                    data = Workbooks()
                    project_id = project.project_id
                    data.publishworkbook(server, project_id, path, workbook_name)
                    return redirect(url_for(".listworkbooks", slug=slug, image=image))
                else:
                    return str("user doesn't exist")
                return redirect(url_for(".listworkbooks", slug=slug, image=image))
        except Exception as e:
            return str(e)


def addallprojects(slug):
    try:
        user = UserRepository.getuserbyslug(slug)
        if not (user is None):
            server = auth.tableauSignIn(user)
            data = Projects()
            data = data.addAllProjects(server=server)
            return "There was loaded {} datasources from site: ".format(len(data[0]))
        else:
            return str("user doesn't exist")
    except Exception as e:
        return str(e)


def getuserandserver(slug):
    user = UserRepository.getuserbyslug(slug)
    server = auth.tableauSignIn(user)
    return user, server


def updateworkbookstatus(list):
    for wb in list:
        new_status = Workbooks.get_workbook_status(Workbooks(), wb.work_name)

        if wb.git_status != new_status:
            Workbooks.updateworkbookstatus(Workbooks(), wb.work_name, new_status)
        print("estou a passar aqui")
    return True
