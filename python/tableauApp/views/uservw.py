from flask import Flask, request, jsonify, render_template, abort, url_for, redirect, Blueprint
import base64
from flask_login import current_user
from extensions import User


user_blueprint = Blueprint('uservw', __name__)

from model.userRepository import UserRepository

from model.serverRepository import ServerRepository

list_users = lambda: UserRepository.listusers()

list_server = lambda : ServerRepository.listServer()


@user_blueprint.route("/user/list", methods=["GET"])
def listusers():
    '''
    List of users
    :return: template that lists the users available
    '''
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    list_users_lg = lambda : UserRepository.list_users_bylogin(current_user.dn)
    if request.method == "GET":
        return render_template("/tableau/userslist.html", users=list_users_lg(), image=image)
    else:
        abort(404)



@user_blueprint.route("/user/add", methods=["GET", "POST"])
def adduser():
    '''
    Form to add new users to perform the tableau authentication
    :return:
    '''
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    if request.method == "GET":
        return render_template("/tableau/usercreate.html", servers=list_server(), image=image)
    try:
        exists = UserRepository.userexists(request.form.get('username'))
        if (exists is None):
            encoded = base64.b64encode(request.form.get('password').encode())
            user = UserRepository.adduser(request,name=request.form.get('username'), password=encoded, server=request.form.get('server'), user_login=current_user.dn)
            return redirect(url_for(".listusers"))
        else:
            '''Handle exception print message if the user exists'''
            return render_template("/tableau/usercreate.html", image=image)
    except Exception as e:
        return(str(e))


@user_blueprint.route("/user/<slug>/delete", methods=["POST"])
def deleteuser(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    user = UserRepository.deleteuser(slug)
    if not user:
        abort(404)
    return redirect(url_for(".listusers"))

@user_blueprint.route("/user/<slug>/edit", methods=["GET", "POST"])
def useredit(slug):
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)

    user = UserRepository.getuserbyslug(slug)

    if not user.name:
        abort(404)


    if request.method == "GET":
        encoded = base64.b64decode(user.password).decode()
        return render_template("tableau/useredit.html", user=user, password=encoded, servers=list_server(), image=image)

    encoded = base64.b64encode(request.form.get('password').encode())
    UserRepository.update(user, name=request.form.get('username'), password=encoded, server=request.form.get('server_edit'))
    return redirect(url_for(".listusers"))

@user_blueprint.route("/user/addapi")
def adduserapi():
    '''
    Get method to add users
    http://127.0.0.1:5000/user/addapi?username=xpand-it.com\dams&password=password
    :return:
    '''
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    try:
        exists = UserRepository.userexists(request.args.get('username'))
        if (exists is None):
            user = UserRepository.adduser(request,name=request.args.get('username'), password=request.args.get('password'))
            return "User saved. User id={}".format(user.id)
        else:
            return "The user already exists with the id={}".format(exists.id)
    except Exception as e:
        return(str(e))
