import os
from flask import Flask, request, jsonify, render_template, abort, url_for, redirect, Blueprint, session, render_template_string
from flask_sqlalchemy import SQLAlchemy
from extensions import login_manager, ldap_manager, User
from flask_login import login_user, current_user, logout_user
from flask_ldap3_login.forms import LDAPLoginForm

app = Flask(__name__)

'''Configurations set from the environment variables
   The APP_SETTINGS have as value the possible values in the config class depending of the profile
'''
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

'''Instantiate the database'''
db = SQLAlchemy(app)

from entity.project import Project

'''Registering the Blueprint views'''
from views.uservw import user_blueprint
app.register_blueprint(user_blueprint)

from views.datasourcevw import datasource_blueprint
app.register_blueprint(datasource_blueprint)

from views.workbookvw import workbook_bp
app.register_blueprint(workbook_bp)

from views.servervw import server_blueprint
app.register_blueprint(server_blueprint)

login_manager.init_app(app)
ldap_manager.init_app(app)

@app.route("/")
def index():
    '''The index page, for each html view is availeble in the python package views using Flask Blueprint'''
    # Redirect users who are not logged in.
    if not current_user or current_user.is_anonymous:
        return redirect(url_for('login'))
    image = User.userpicture(current_user)
    return render_template("tableau/index.html", image=image)


@app.errorhandler(404)
def page_not_found(e):
    '''Render a 404 page'''
    return render_template("404.html")


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    # Instantiate a LDAPLoginForm which has a validator to check if the user
    # exists in LDAP.
    form = LDAPLoginForm()

    if form.validate_on_submit():
        # Successfully logged in, We can now access the saved user object
        # via form.user.
        login_user(form.user)  # Tell flask-login to log them in.
        return redirect('/')  # Send them home

    return render_template("./login.html", form=form)


if __name__ == '__main__':
    app.run(Debug=True)
