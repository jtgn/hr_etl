from github.gitsync import GitSync
from model.projectRepository import ProjectRepository
from config import Config
import os

repo = GitSync.defineRepo(Config.REPO_PATH_URL)

class Projects():
    def addAllProjects(self, server):
        data, pagination = server.projects.get()
        dtrepo = ProjectRepository()
        for project in data:
            exists = dtrepo.projectexists(str(project.name), str(server.server_address))
            if (exists is None):
                dtrepo.addProject(project, server.server_address)

        return data