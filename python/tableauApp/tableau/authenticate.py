import tableauserverclient as TSC
import base64
class Auth():

    def tableauSignIn(self, user):
        password = base64.b64decode(user.password).decode()
        tableau_auth = TSC.TableauAuth(user.name, password)
        server = TSC.Server(user.server)
        server.auth.sign_in(tableau_auth)
        print("User Authenticated")
        return server


    def tableauSignOut(self, server):
        server.auth.sign_out()
