from model.workbookRepository import WorkbookRepository
from base64 import b64encode
from config import Config
import tableauserverclient as TSC
from github.gitsync import GitSync
import os

repo = GitSync.defineRepo(Config.REPO_PATH_URL)

class Workbooks():
    def getallworkbooks(self, server, user):
        data = server.workbooks.get()
        wbrepo = WorkbookRepository()
        for wb in data[0]:
            exists = wbrepo.workbookexists(wb.name, server.server_address, user.name)
            if exists is None:
                server.workbooks.populate_preview_image(wb)
                image = b64encode(wb.preview_image)
                wbrepo.addworkbook(wb, image, image.decode('utf8'),server.server_address, user.name)

    def downloadworkbook(self, workbook_id, server):
        data = server.workbooks.download(workbook_id, no_extract=False, filepath=Config.REPO_PATH_URL+'/python/workbooks')
        return data

    def get_workbook_status(self, name):
        name_without = name.replace('(', '').replace(')', '')
        str1, str2 = 'python/workbooks/'+name_without+'.twbx', 'python/workbooks/'+name_without+'.twb'
        if os.path.exists((Config.REPO_PATH_URL+'/'+str1).replace('(', '').replace(')', '').replace('+', '')):
            path = str1
        else:
            path = str2
        status = GitSync.getgitstatus(repo, path)
        return status

    def updateworkbookstatus(self, name, status):
        WorkbookRepository.updategitstatus(WorkbookRepository(), name, status)
        return status

    def publishworkbook(self, server, project_id, file_path, file_name):
        workbooks = WorkbookRepository.getworkbook_name_server(file_name, server.server_address)
        if workbooks is None :
            flag = 'CreateNew'
        else:
            flag = 'Overwrite'
        new_workbook = TSC.WorkbookItem(project_id)
        new_workbook = server.workbooks.publish(new_workbook, file_path, flag)
        return new_workbook
