from github.gitsync import GitSync
from model.DatasourceRepository import DatasourceRepository
from config import Config
import tableauserverclient as TSC
from files.read_file import ConnectionsFileReader
import os

repo = GitSync.defineRepo(Config.REPO_PATH_URL)

class Datasources():
    def addAllDatasources(self, server, user):
        data = server.datasources.get()
        dtrepo = DatasourceRepository()
        for datasource in data[0]:
            #dt_id = server.datasources.get_by_id(datasource.id)
            #server.datasources.populate_connections(dt_id)
            exists = dtrepo.datasourceexists(str(datasource.name), str(server.server_address), str(user.name))
            if (exists is None):
                dtrepo.addDatasource(datasource, server.server_address, user.name)

        return data

    def downloadDatasource(self, datasource_id, server):
        data = server.datasources.download(datasource_id, include_extract=False, filepath=Config.REPO_PATH_URL+'/python/datasources')
        return data

    def updatedatasourcestatus(self, name):
        namewithout = name.replace('(', '').replace(')', '')
        str = 'python/datasources/'+namewithout+'.tdsx'
        status = GitSync.getgitstatus(repo, str)

        flag = False
        for f_status in DatasourceRepository.getstatus(name):
            if f_status.git_status != status:
                flag = True

        if flag:
            DatasourceRepository.updategitstatus(DatasourceRepository(), name, status)

        return status

    def publishdatasource(self, server, project_id, file_path, file_name):
        datasources = DatasourceRepository.getdatasource_name_server(file_name, server.server_address)
        if datasources is None :
            flag = 'CreateNew'
        else:
            flag = 'Overwrite'
        new_datasource = TSC.DatasourceItem(project_id)
        new_datasource = server.datasources.publish(new_datasource, file_path, flag)
        data=server.datasources.get()
        for datasource in data[0]:
            if datasource.name==file_name:
                dt_id = server.datasources.get_by_id(datasource.id)
                server.datasources.populate_connections(dt_id)
                connections = dt_id.connections
                for connection in connections:
                    password = ConnectionsFileReader.findpassword(Config.REPO_PATH_URL+'/python/tableauApp/files/connections.txt', connection.server_address, connection.server_port, connection.username)
                    connection.password = password
                    connection.embed_password=True
                    server.datasources.update_connection(dt_id, connection)
        return new_datasource
