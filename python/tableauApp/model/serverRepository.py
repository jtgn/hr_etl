from app import db
from entity.server import Server

class ServerRepository():
    def addServer(self,name, url, ip, user, passw):
        server = Server(name=name, url=url, slug=name, ip=ip, user=user, passw=passw)
        db.session.add(server)
        db.session.commit()
        return server

    def deleteServer(name):
        try:
            server = Server.query.filter_by(name=name).first()
            db.session.delete(server)
            db.session.commit()
            return server
        except Exception as e:
            return (str(e))

    def updateServer(server, name, url, ip, user, passw):
        try:
            server.name = name
            server.url = url
            server.ip_address = ip
            server.ssh_user = user
            server.ssh_pass = passw
            db.session.commit()
            return server
        except Exception as e:
            return (str(e))

    def serverExists(url):
        print("")
        try:
            exist=Server.query.filter_by(url=url).first()
            return exist
        except Exception as e:
            return(str(e))


    @staticmethod
    def getserverbyurl(url):
        try:
            server = Server.query.filter_by(url=url).first()
            return server
        except Exception as e:
            return (str(e))

    @staticmethod
    def getserverbyname(name):
        try:
            server = Server.query.filter_by(name=name).first()
            return server
        except Exception as e:
            return (str(e))

    @staticmethod
    def listServer():
        return Server.query.order_by(Server.name.desc())
