from app import db
from entity.datasource import Datasource

class DatasourceRepository():
    def addDatasource(self,data, server, user):
        dtsrc = Datasource(data.name,data._id,data._datasource_type,data._project_id, data._project_name,data._content_url, str(data._updated_at), "nothing", user, server)
        db.session.add(dtsrc)
        db.session.commit()

    def datasourceexists(self, name, server, user_name):
        print("")
        try:
            exist=Datasource.query.filter_by(name=name, server=server, user=user_name).first()
            return exist
        except Exception as e:
            return(str(e))

    def updategitstatus(self, name, status):
        print("Update Status")
        datasource = db.session.query(Datasource).filter(Datasource.name == name).all()
        for d in datasource:
            d.git_status = status
            db.session.commit()
        return "true"

    @staticmethod
    def getdatasource_name_server(name, server):
        return Datasource.query.with_entities(Datasource.name).filter_by(name=name, server=server).all()

    @staticmethod
    def getstatus(name):
        return Datasource.query.with_entities(Datasource.git_status).filter_by(name=name).all()

    @staticmethod
    def list_datasources(name, server):
        return Datasource.query.filter_by(user=name, server=server)

    @staticmethod
    def list_datasources_byserver(server):
        return Datasource.query.filter_by(server=server)

    @staticmethod
    def listdatasources():
        return Datasource.query.order_by(Datasource.name.desc())
