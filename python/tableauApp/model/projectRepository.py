from app import db
from entity.project import Project

class ProjectRepository():
    def addProject(self,data, server):
        dtsrc = Project(data._name,data.description,data._id, str(data.parent_id), data._content_permissions, server)
        db.session.add(dtsrc)
        db.session.commit()

    def projectexists(self, name, server):
        print("")
        try:
            exist=Project.query.filter_by(name=name, server=server).first()
            return exist
        except Exception as e:
            return str(e)

    def getprojectbynameserver(name, server):
        try:
            project = Project.query.filter_by(name=name, server=server).first()
            return project
        except Exception as e:
            return str(e)

    @staticmethod
    def listuniqueprojects():
        return db.session.query(Project).distinct(Project.name).all()

    @staticmethod
    def listprojects():
        return Project.query.order_by(Project.name.desc())
