from app import db
from entity.workbook import Workbook

class WorkbookRepository():
    '''Method for add the workbook metadata in the database'''
    def addworkbook(self,data, image, img, server, user):
        work = Workbook(data.content_url, data.id, data.name, data.owner_id, image, img, data.project_id, data.project_name, data.name, "nothing", user, server)
        db.session.add(work)
        db.session.commit()

    def workbookexists(self, name, server, user_name):
        try:
            exist=Workbook.query.filter_by(work_name=name, server=server, user=user_name).first()
            return exist
        except Exception as e:
            return (str(e))

    def updategitstatus(self, name, status):
        print("Update Status")
        workbook = db.session.query(Workbook).filter(Workbook.work_name == name).all()
        for w in workbook:
            w.git_status = status
            db.session.commit()
        return "true"


    @staticmethod
    def getworkbook_name_server(name, server):
        return Workbook.query.with_entities(Workbook.work_name).filter_by(work_name=name, server=server).all()


    @staticmethod
    def getstatus(name):
        return Workbook.query.with_entities(Workbook.git_status).filter_by(work_name=name).all()

    @staticmethod
    def list_workbooks(name, server):
        return Workbook.query.filter_by(user=name, server=server)

    @staticmethod
    def list_workbooks_byserver(server):
        return Workbook.query.filter_by(server=server)

    @staticmethod
    def listworkbooks():
        return Workbook.query.order_by(Workbook.work_name.desc())
