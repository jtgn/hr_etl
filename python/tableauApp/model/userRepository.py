from app import db
from entity.user import User
from slugify import slugify

class UserRepository():
    def adduser(self,name,password, server, user_login):
        user = User(name=name, password=password, server=server, slug=slugify(name), login_user=user_login)
        db.session.add(user)
        db.session.commit()
        return user


    def userexists(name):
        print("")
        try:
            exist=User.query.filter_by(name=name).first()
            return exist
        except Exception as e:
            return(str(e))

    def deleteuser(slug):
        try:
            user = User.query.filter_by(slug=slug).first()
            db.session.delete(user)
            db.session.commit()
            return user
        except Exception as e:
            return (str(e))

    def getuserbyslug(slug):
        try:
            user = User.query.filter_by(slug=slug).first()
            return user
        except Exception as e:
            return (str(e))

    def getuserbyname(name):
        try:
            user = User.query.filter_by(name=name).first()
            return user
        except Exception as e:
            return (str(e))

    def update(user, name, password, server):
        try:
            user.name = name
            user.password = password
            user.server = server
            db.session.commit()
            return user
        except Exception as e:
            return (str(e))

    @staticmethod
    def listusers():
        return User.query.order_by(User.name.desc())

    @staticmethod
    def list_users_bylogin(user):
        return User.query.filter_by(login_user=user)
