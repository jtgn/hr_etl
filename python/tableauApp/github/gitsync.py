import os
from git import Repo
from config import Config

COMMITS_TO_PRINT = 5
class GitSync :
    def print_commit(commit):
        print('----')
        print(str(commit.hexsha))
        print("\"{}\" by {} ({})".format(commit.summary,
                                         commit.author.name,
                                         commit.author.email))
        print(str(commit.authored_datetime))
        print(str("count: {} and size: {}".format(commit.count(),
                                                  commit.size)))

    def print_repository(repo):
        print('Repo description: {}'.format(repo.description))
        print('Repo active branch is {}'.format(repo.active_branch))
        for remote in repo.remotes:
            print('Remote named "{}" with URL "{}"'.format(remote, remote.url))
        print('Last commit for repo is {}.'.format(str(repo.head.commit.hexsha)))


    def defineRepo(path):
        # repo_path = os.getenv(path)
        # Repo object used to programmatically interact with Git repositories
        repo = Repo(path, search_parent_directories=True)
        return repo

    def getgitstatus(repo, path):

        changed = [item.a_path for item in repo.index.diff(None)]
        fullpath = (Config.REPO_PATH_URL+'/'+path).replace('(', '').replace(')', '').replace('+', '')
        untracked_files = repo.untracked_files
        if path in untracked_files:
            return 'untracked'
        elif path in changed:
            return 'changed'
        elif os.path.exists(fullpath) == False:
            return 'notdownloaded'
        else:
            return 'updated'

    def git_push(repo, path, message):
        try:
            #repo = Repo(PATH_OF_GIT_REPO)
            repo.git.add(path)
            repo.index.commit(message)
            origin = repo.remote(name='origin')
            origin.push()
        except:
            print('Some error occured while pushing the code')
        finally:
            print('Code push from script succeeded')