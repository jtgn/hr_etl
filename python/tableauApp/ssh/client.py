import paramiko

class SSH_Client():
    def __init__(self, ip, user, passw):
        self.ipaddr = ip
        self.ssh_user = user
        self.ssh_passw = passw
        self.ssh_port = '22'

    def connect(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # Make connection and create shell.
        client.connect(self.ipaddr, self.ssh_port, self.ssh_user, self.ssh_passw)
        shell = client.invoke_shell()

        # Execute command and get results.
        _, stdout, stderr = client.exec_command("(cd python_scripts; ./cpu.py info)")
        ssh_output = stdout.read()
        ssh_error = stderr.read()

        # Close connection.
        shell.close()
        client.close()
        return ssh_output