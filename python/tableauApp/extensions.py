from flask_ldap3_login import LDAP3LoginManager
from flask_login import LoginManager, UserMixin
from base64 import b64encode

ldap_manager = LDAP3LoginManager()
login_manager = LoginManager()

# Create a dictionary to store the users in when they authenticate
# This example stores users in memory.
users = {}

# Declare an Object Model for the user, and make it comply with the
# flask-login UserMixin mixin.
class User(UserMixin):
    def __init__(self, dn, username, data):
        self.dn = dn
        self.username = username
        self.data = data

    def __repr__(self):
        return self.dn

    def get_id(self):
        return self.dn


    @staticmethod
    def userpicture(user):
        ima = b64encode(user.data['jpegPhoto'][0])
        image = ima.decode('utf8')
        return image

# Declare The User Saver for Flask-Ldap3-Login
# This method is called whenever a LDAPLoginForm() successfully validates.
# Here you have to save the user, and return it so it can be used in the
# login controller.
@ldap_manager.save_user
def save_user(dn, username, data, memberships):
    user = User(dn, username, data)
    users[dn] = user
    return user


# Declare a User Loader for Flask-Login.
# Simply returns the User if it exists in our 'database', otherwise
# returns None.
@login_manager.user_loader
def load_user(id):
    if id in users:
        return users[id]
    return None

