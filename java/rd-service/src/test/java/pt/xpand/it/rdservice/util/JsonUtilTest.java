package pt.xpand.it.rdservice.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;

public class JsonUtilTest {

    JsonUtil jsonUtil = new JsonUtil();

    @Test
    public void haveKey() throws Exception {
        LinkedHashMap<String, String>  obj = new LinkedHashMap<>();
        obj.put("uuid", "120-204-134");
        String x = "uuid";
        Boolean a = jsonUtil.haveKey(obj, x);
        Assert.assertEquals(a, true);
    }

}