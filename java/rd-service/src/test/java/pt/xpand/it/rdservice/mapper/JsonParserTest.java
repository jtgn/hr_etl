package pt.xpand.it.rdservice.mapper;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pt.xpand.it.rdservice.config.Ymlconfig;
import pt.xpand.it.rdservice.entity.Leads;
import pt.xpand.it.rdservice.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;

public class JsonParserTest {

    Ymlconfig app;
    JsonUtil jsonUtil = new JsonUtil();

    String str;
    Leads lead = new Leads();
    List<Leads> leadsList = new ArrayList<>();
    JsonParser jsonParser = new JsonParser();


    @Before
    public void setUp() throws Exception {
            str = "{\"leads\":\n" +
                    "[{\"id\":\"1\",\n" +
                    "  \"uuid\":\"c2f3d2b3-7250-4d27-97f4-eef38be32f7f\",\n" +
                    "  \"email\":\"suporte@resultadosdigitais.com.br\",\n" +
                    "  \"name\":\"Bruno Ghisi\",\n" +
                    "  \"company\":\"Resultados Digitais\",\n" +
                    "  \"job_title\":\"IT\",\n" +
                    "  \"bio\":\"This is my bio\",\n" +
                    "  \"public_url\":\"http://rdstation.com.br/leads/public/e8d64a22-17a8-40e3-bb8c-c16af1861708\",\n" +
                    "  \"created_at\":\"2012-06-04T15:31:35-03:00\",\n" +
                    "  \"opportunity\":\"false\",\n" +
                    "  \"number_conversions\":\"3\",\n" +
                    "  \"user\": \"email@example.com\",\n" +
                    "  \"first_conversion\": {\n" +
                    "    \"content\": {\n" +
                    "      \"identificador\":\"ebook-abc\",\n" +
                    "      \"nome\":\"Bruno\",\n" +
                    "      \"email_lead\":\"suporte@resultadosdigitais.com.br\",\n" +
                    "      \"telefone\":\"99999999\",\n" +
                    "      \"empresa\":\"Resultados Digitais\",\n" +
                    "      \"cargo\":\"IT\"\n" +
                    "    },\n" +
                    "    \"created_at\":\"2012-06-04T15:31:35-03:00\",\n" +
                    "    \"cumulative_sum\":\"1\",\n" +
                    "    \"source\":\"source 1\",\n" +
                    "    \"conversion_origin\": {\n" +
                    "      \"source\": \"source 1\",\n" +
                    "      \"medium\": \"medium 1\",\n" +
                    "      \"value\": \"value 1\",\n" +
                    "      \"campaign\": \"campaign 1\",\n" +
                    "      \"channel\": \"channel 1\"\n" +
                    "    }\n" +
                    "  },\n" +
                    "  \"last_conversion\": {\n" +
                    "    \"content\": {\n" +
                    "      \"identificador\":\"webinar-abc\",\n" +
                    "      \"email_lead\":\"suporte@resultadosdigitais.com.br\"\n" +
                    "    },\n" +
                    "    \"created_at\":\"2012-06-04T15:31:35-03:00\",\n" +
                    "    \"cumulative_sum\":\"2\",\n" +
                    "    \"source\":\"source 2\"\n" +
                    "  },\n" +
                    "  \"custom_fields\": {\n" +
                    "    \"cf_phone_xp\": \"3241663\",\n" +
                    "    \"cf_last_name\": \"xpto\"\n" +
                    "  },\n" +
                    "  \"website\": \"http://www.resultadosdigitais.com.br\",\n" +
                    "  \"personal_phone\":\"48 30252598\",\n" +
                    "  \"mobile_phone\":\"48 30252598\",\n" +
                    "  \"city\":\"Florianópolis\",\n" +
                    "  \"state\": \"SC\",\n" +
                    "  \"lead_stage\": \"Lead\",\n" +
                    "  \"tags\": [\"tag 1\", \"tag 2\"],\n" +
                    "  \"fit_score\":\"d\",\n" +
                    "  \"interest\":0\n" +
                    "}]\n" +
                    "}";
        jsonParser.setUuid("c2f3d2b3-7250-4d27-97f4-eef38be32f7f");
        jsonParser.setEmail("suporte@resultadosdigitais.com.br");
        jsonParser.setName("Bruno Ghisi");
        jsonParser.setCompany("Resultados Digitais");
        jsonParser.setJob_title("IT");
        jsonParser.setBio("This is my bio");
        jsonParser.setCreated_at("2012-06-04T15:31:35-03:00");
        jsonParser.setOpportunity("false");
        jsonParser.setNumber_conversions("3");
        jsonParser.setWebsite("http://www.resultadosdigitais.com.br");
        jsonParser.setPersonal_phone("48 30252598");
        jsonParser.setMobile_phone("48 30252598");
        jsonParser.setLead_stage("Lead");
        jsonParser.setFit_score("d");
        jsonParser.setInterest("0");


        jsonParser.setTags("");
        jsonParser.setCustomFields("{cf_phone_xp=3241663, cf_last_name=xpto}");
        jsonParser.setFirstConversion("{content={identificador=ebook-abc, nome=Bruno, email_lead=suporte@resultadosdigitais.com.br, telefone=99999999, empresa=Resultados Digitais, cargo=IT}, created_at=2012-06-04T15:31:35-03:00, cumulative_sum=1, source=source 1, conversion_origin={source=source 1, medium=medium 1, value=value 1, campaign=campaign 1, channel=channel 1}}");
        jsonParser.setLastConversion("{content={identificador=webinar-abc, email_lead=suporte@resultadosdigitais.com.br}, created_at=2012-06-04T15:31:35-03:00, cumulative_sum=2, source=source 2}");


        app = new Ymlconfig();
        app.setRoot_leads("leads");
        app.setUuid("uuid");
        app.setEmail("email");
        app.setName("name");
        app.setCompany("company");
        app.setJob_title("job_title");
        app.setBio("bio");
        app.setCreated_at("created_at");
        app.setOpportunity("opportunity");
        app.setNumber_conversions("number_conversions");
        app.setWebsite("website");
        app.setPersonal_phone("personal_phone");
        app.setMobile_phone("mobile_phone");
        app.setLead_stage("lead_stage");
        app.setFit_score("fit_score");
        app.setInterest("interest");
        app.setTags("tags");
        app.setCustom_fields("custom_fields");
        app.setFirst_conversion("first_conversion");
        app.setLast_conversion("last_conversion");





        jsonParser.setApp(app);
        jsonParser.setJsonUtil(jsonUtil);
        lead = Leads.builder().uuid(jsonParser.getUuid())
                .email(jsonParser.getEmail())
                .name(jsonParser.getName())
                .company(jsonParser.getCompany())
                .jobTitle(jsonParser.getJob_title())
                .bio(jsonParser.getBio())
                .created_at(jsonParser.getCreated_at())
                .opportunity(jsonParser.getOpportunity())
                .number_conversions(jsonParser.getNumber_conversions())
                .website(jsonParser.getWebsite())
                .personal_phone(jsonParser.getPersonal_phone())
                .mobile_phone(jsonParser.getMobile_phone())
                .lead_stage(jsonParser.getLead_stage())
                .fit_score(jsonParser.getFit_score())
                .interest(jsonParser.getInterest())
                .tags(jsonParser.getTags())
                .customFields(jsonParser.getCustomFields())
                .firstConversion(jsonParser.getFirstConversion())
                .lastConversion(jsonParser.getLastConversion())
                .build();
        leadsList.add(lead);

    }

    @Test
    public void parseJsontoLeads() throws Exception {

        Assert.assertEquals(jsonParser.parseJsontoLeads(str), leadsList);
    }

    @Test
    public void getUuid() throws Exception {
        String uuid = "c2f3d2b3-7250-4d27-97f4-eef38be32f7f";
        Assert.assertEquals(jsonParser.getUuid(), uuid);
    }

    @Test
    public void getName() throws Exception {
        String name = "Bruno Ghisi";
        Assert.assertEquals(jsonParser.getName(), name);
    }

    @Test
    public void getEmail() throws Exception {
        String email = "suporte@resultadosdigitais.com.br";
        Assert.assertEquals(jsonParser.getEmail(), email);
    }

    @Test
    public void getCompany() throws Exception {
        String company = "Resultados Digitais";
        Assert.assertEquals(jsonParser.getCompany(), company);
    }

    @Test
    public void getJob_title() throws Exception {
        String job_title = "IT";
        Assert.assertEquals(jsonParser.getJob_title(), job_title);
    }

    @Test
    public void getCreated_at() throws Exception {
        String created_at = "2012-06-04T15:31:35-03:00";
        Assert.assertEquals(jsonParser.getCreated_at(), created_at);
    }

    @Test
    public void getOpportunity() throws Exception {
        String op = "false";
        Assert.assertEquals(jsonParser.getOpportunity(), op);
    }

    @Test
    public void getNumber_conversations() throws Exception {
        String nc = "3";
        Assert.assertEquals(jsonParser.getNumber_conversions(), nc);
    }

    @Test
    public void getCustom_fields() throws Exception {
        String cf = "{cf_phone_xp=3241663, cf_last_name=xpto}";
        Assert.assertEquals(jsonParser.getCustomFields(), cf);
    }

    @Test
    public void getPersonal_phone() throws Exception {
        String pf = "48 30252598";
        Assert.assertEquals(jsonParser.getPersonal_phone(), pf);
    }

    @Test
    public void getMobile_phone() throws Exception {
        String mf = "48 30252598";
        Assert.assertEquals(jsonParser.getMobile_phone(), mf);
    }

    @Test
    public void getLead_stage() throws Exception {
        String lead_stage = "Lead";
        Assert.assertEquals(jsonParser.getLead_stage(), lead_stage);
    }

    @Test
    public void getTags() throws Exception {
        //        Assert.assertEquals(jsonParser.getTags(), set);
    }

    @Test
    public void getFit_score() throws Exception {
        String fit_score = "d";
        Assert.assertEquals(jsonParser.getFit_score(), fit_score);
    }

    @Test
    public void getInterest() throws Exception {
        String interest = "0";
        Assert.assertEquals(jsonParser.getInterest(), interest);
    }

    @Test
    public void getFirstConversion() throws Exception {
        String fc = "{content={identificador=ebook-abc, nome=Bruno, email_lead=suporte@resultadosdigitais.com.br, telefone=99999999, empresa=Resultados Digitais, cargo=IT}, created_at=2012-06-04T15:31:35-03:00, cumulative_sum=1, source=source 1, conversion_origin={source=source 1, medium=medium 1, value=value 1, campaign=campaign 1, channel=channel 1}}";
        Assert.assertEquals(jsonParser.getFirstConversion(), fc);
    }

    @Test
    public void getLastConversion() throws Exception {
        String lc = "{content={identificador=webinar-abc, email_lead=suporte@resultadosdigitais.com.br}, created_at=2012-06-04T15:31:35-03:00, cumulative_sum=2, source=source 2}";
        Assert.assertEquals(jsonParser.getLastConversion(), lc);
    }

}