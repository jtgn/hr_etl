package pt.xpand.it.rdservice.controller;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import org.slf4j.Logger;
import pt.xpand.it.rdservice.audit.JsonAudit;
import pt.xpand.it.rdservice.entity.Leads;
import pt.xpand.it.rdservice.mapper.JsonParser;
import pt.xpand.it.rdservice.model.JsonAuditRepository;
import pt.xpand.it.rdservice.model.LeadsRepository;

import java.net.URISyntaxException;
import java.util.*;

@RestController
@RequestMapping("/api")
public class LeadsController {

    private final Logger log = LoggerFactory.getLogger(LeadsController.class);

    private LeadsRepository leadsRepository;
    private JsonAuditRepository jsonAuditRepository;

    public LeadsController(LeadsRepository leadsRepository, JsonAuditRepository jsonAuditRepository){
        this.leadsRepository=leadsRepository;
        this.jsonAuditRepository=jsonAuditRepository;
    }

    @Autowired
    private JsonParser jsonParser;

    /**
     * Rest endpoint to store the json file from the webhook of RD Station
     * @param json
     * @return
     * @throws URISyntaxException
     * @throws Exception
     */
    @PostMapping("/leads")
    ResponseEntity<Leads> addLead(@RequestBody String json) throws URISyntaxException, Exception {
        //move to a method
        jsonAuditRepository.save(new JsonAudit(new Date(), json.toString()));
        List<Leads> leadsList = new ArrayList<>();
        leadsList = jsonParser.parseJsontoLeads(json);

        leadsList.forEach((l) -> {
            leadsRepository.save(l);
        });
        return ResponseEntity.ok().build();
    }

    @GetMapping("/getLeads")
    Collection<Leads> leads(){
        return leadsRepository.findAll();
    }
}
