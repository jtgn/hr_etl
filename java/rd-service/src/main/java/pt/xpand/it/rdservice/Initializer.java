package pt.xpand.it.rdservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import pt.xpand.it.rdservice.config.Ymlconfig;
import pt.xpand.it.rdservice.entity.Leads;
import pt.xpand.it.rdservice.mapper.JsonParser;
import pt.xpand.it.rdservice.model.LeadsRepository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class Initializer implements CommandLineRunner {

    private final LeadsRepository leadsRepository;
    private List<Object> leads;

    public Initializer(LeadsRepository leadsRepository){
        this.leadsRepository=leadsRepository;
    }

    @Autowired
    private JsonParser jsonParser;

    @Autowired
    private Ymlconfig appConfig;


    @Override
    public void run(String... args) throws Exception {

        /**
         * Used for testing . TODO Create Unit testing
         */
        /*leadsRepository.deleteAllInBatch();
        File file = new ClassPathResource("static/rdstation_integration.json").getFile();
        List<String> list = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        leads = new ArrayList<>();

        list = bufferedReader.lines().collect(Collectors.toList());

        list.forEach(str -> {

            if (!str.equals("")) {
                leads.add(str);
            }

        });
        String test="";
        List<Leads> leadsList = new ArrayList<>();
        leads.forEach((v) -> {

            String json = v.toString();
            jsonParser.parseJsontoLeads(json).forEach(leads1 -> {
                leadsList.add(leads1);
            });
            String val= "";

        });

        leadsList.forEach(l ->{
            leadsRepository.save(l);
        });
        String value="";*/
        //leadsRepository.findAll().forEach(System.out::println);
        //String test="";
       //System.out.println(appConfig.getEmail());


    }
}
