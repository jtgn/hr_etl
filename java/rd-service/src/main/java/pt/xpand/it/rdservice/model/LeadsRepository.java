package pt.xpand.it.rdservice.model;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.xpand.it.rdservice.entity.Leads;

public interface LeadsRepository extends JpaRepository<Leads, Long> {
    Leads findByEmail(String email);
}
