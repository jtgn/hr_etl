package pt.xpand.it.rdservice.entity;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class CustomFields {
    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    @Column(columnDefinition = "text")
    private String cfName;
    @NonNull
    @Column(columnDefinition = "text")
    private String cfValue;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "leads_id")
    private Leads lead;
}
