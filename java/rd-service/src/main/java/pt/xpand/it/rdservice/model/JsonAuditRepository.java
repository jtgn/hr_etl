package pt.xpand.it.rdservice.model;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.xpand.it.rdservice.audit.JsonAudit;

public interface JsonAuditRepository extends JpaRepository<JsonAudit, Long> {
}
