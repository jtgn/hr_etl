package pt.xpand.it.rdservice.entity;


import lombok.*;
import org.aspectj.runtime.internal.Conversions;

import javax.persistence.*;
import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name= "Leads")
public class Leads {

    @Id
    @GeneratedValue
    private Long id;
    private String uuid;
    private String email;
    private String name;
    private String company;
    private String jobTitle;
    private String bio;
    private String created_at;
    private String opportunity;
    private String number_conversions;
    private String website;
    private String personal_phone;
    private String mobile_phone;
    private String lead_stage;
    private String fit_score;
    private String interest;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "leads_tag",
            joinColumns = @JoinColumn(name = "leads_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tags> tags = new ArrayList<>();

    @OneToMany(
            mappedBy = "lead",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CustomFields> cFields = new ArrayList<>();

    @OneToMany(
            mappedBy = "lead_conversion",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Conversion> conversions;

    public void addTag(Tags tag){
        tags.add(tag);
        tag.getLeads().add(this);
    }

    public void addCField(CustomFields customFields){
        cFields.add(customFields);
        customFields.setLead(this);
    }

    public void addConversions(Conversion conversion){
        conversions.add(conversion);
        conversion.setLead_conversion(this);
    }
}
