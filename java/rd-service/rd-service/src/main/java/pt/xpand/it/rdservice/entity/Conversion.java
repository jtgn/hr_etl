package pt.xpand.it.rdservice.entity;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Conversion {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String conversion;

    @NonNull
    private String source;

    @NonNull
    private String created_date;

    @NonNull
    private  String comulative_sum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "leads_id")
    private Leads lead_conversion;
}
