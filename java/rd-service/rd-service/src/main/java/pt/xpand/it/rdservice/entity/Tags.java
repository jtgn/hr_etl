package pt.xpand.it.rdservice.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Tags {

    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    private String tag;
    @ManyToMany(mappedBy = "tags")
    private List<Leads> leads = new ArrayList<>();
}
