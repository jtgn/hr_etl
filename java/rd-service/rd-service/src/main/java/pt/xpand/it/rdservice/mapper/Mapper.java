package pt.xpand.it.rdservice.mapper;


/***
 * Mapper interface to parse json objects
 */
public interface Mapper {
    <T> T jasonMapper(String s);
}
