package pt.xpand.it.rdservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RdServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RdServiceApplication.class, args);
	}

}

