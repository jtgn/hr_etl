package pt.xpand.it.rdservice.audit;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class JsonAudit {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date created_at;

    @NonNull
    @Column(columnDefinition = "text")
    private String json;
}
