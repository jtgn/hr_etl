package pt.xpand.it.rdservice.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app")
@NoArgsConstructor
@Data
public class Ymlconfig {

    private String root_leads;
    private String uuid;
    private String email;
    private String name;
    private String company;
    private String job_title;
    private String bio;
    private String created_at;
    private String opportunity;
    private String number_conversions;
    private String user;
    private String custom_fields;
    private List<String> cf_custom_fields = new ArrayList<>();
    private String website;
    private String personal_phone;
    private String mobile_phone;
    private String lead_stage;
    private String tags;
    private String fit_score;
    private String interest;
    private String conversion;

}
