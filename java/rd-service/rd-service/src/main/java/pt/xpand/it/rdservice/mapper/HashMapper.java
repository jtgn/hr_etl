package pt.xpand.it.rdservice.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;


public class HashMapper<T> implements Mapper{

    @Override
    public <T> T jasonMapper(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonMap = new HashMap<>();
        try {
            jsonMap = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {});

        }catch(Exception e){}
        return (T) jsonMap;
    }
}
