package pt.xpand.it.rdservice.util;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedHashMap;

@Component
public class JsonUtil {

    /**
     * Tests if the key exists in the map
     * @param map
     * @param v
     * @return boolean if thekey exists
     */
    public boolean haveKey(Object map, String v){

        if (map instanceof LinkedHashMap){
            return ((LinkedHashMap) map).containsKey(v);
        }
        return false;
    }

    public boolean haveKeywithPattern(Object map, String pattern){
        if (map instanceof LinkedHashMap){
            for (Object key : ((LinkedHashMap) map).keySet().toArray()){
                if(key.toString().matches(pattern)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param map
     * @param pattern
     * @return hasmap with the conversions
     */
    public HashMap<String, LinkedHashMap> buildHashMapConversions(Object map, String pattern){
        HashMap<String, LinkedHashMap> mapConversions = new HashMap<>();
        for (Object key : ((LinkedHashMap) map).keySet().toArray()){
            if(key.toString().matches(pattern)){
                mapConversions.put(key.toString(), (LinkedHashMap)((LinkedHashMap) map).get(key.toString()));
            }
        }
        return mapConversions;
    }

}
