package pt.xpand.it.rdservice.mapper;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.xpand.it.rdservice.config.Ymlconfig;
import pt.xpand.it.rdservice.entity.Conversion;
import pt.xpand.it.rdservice.entity.CustomFields;
import pt.xpand.it.rdservice.entity.Leads;
import pt.xpand.it.rdservice.entity.Tags;
import pt.xpand.it.rdservice.util.JsonUtil;

import java.util.*;

@Component
@Data
@NoArgsConstructor
public class JsonParser {

    @Autowired
    private Ymlconfig app;

    @Autowired
    private JsonUtil jsonUtil;

    private String uuid;
    private String name;
    private String email;
    private String company;
    private String job_title;
    private String bio;
    private String created_at;
    private String opportunity;
    private String number_conversions;
    private String user;
    private LinkedHashMap custom_fields;
    private String website;
    private String personal_phone;
    private String mobile_phone;
    private String lead_stage;
    private List<String> tags;
    private List<Tags> tagsList;
    private String fit_score;
    private String interest;
    private HashMap<String, LinkedHashMap> conversions_map;


    /**
     * Method that parses a string object to a Leads object
     * @param json
     * @return Leads object
     */
    public List<Leads> parseJsontoLeads(String json){

        List<Leads> leadsList = new ArrayList<>();
        List<Tags> tagsList = new ArrayList<>();
        Mapper mapper = Mapperfactory.createHashMapper();
        Map<String, Object> jsonMap = mapper.jasonMapper(json);

        List<Object> leads = (List<Object>) jsonMap.get(app.getRoot_leads());

        leads.forEach((v) -> {

            this.setAttributes(v);

            Leads lead = Leads.builder().uuid(getUuid())
                    .email(getEmail())
                    .name(getName())
                    .company(getCompany())
                    .jobTitle(getJob_title())
                    .bio(getBio())
                    .created_at(getCreated_at())
                    .opportunity(getOpportunity())
                    .number_conversions(getNumber_conversions())
                    .website(getWebsite())
                    .personal_phone(getPersonal_phone())
                    .mobile_phone(getMobile_phone())
                    .lead_stage(getLead_stage())
                    .fit_score(getFit_score())
                    .interest(getInterest())
                    .tags(new ArrayList<>())
                    .cFields(new ArrayList<>())
                    .conversions(new ArrayList<>()).build();

            if (getTags().size() > 0){
                buildTagsObj(getTags(), lead);
            }
            if (getCustom_fields().size() > 0){
                buildCustomFieldsObj(getCustom_fields(), lead);
            }

            if (getConversions_map().size() > 0){
                buildConversionsFiels(getConversions_map(), lead);
            }

            leadsList.add(lead);
        });
        return leadsList;
    }

    /**
     * Method that sets the attributes of the class
     * @param v
     */
    private void setAttributes(Object v){
        if (v instanceof LinkedHashMap){
            setUuid(jsonUtil.haveKey(v, app.getUuid()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getUuid())).orElse("") : "");
            setEmail(jsonUtil.haveKey(v, app.getEmail()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getEmail())).orElse("") : "");
            setName(jsonUtil.haveKey(v, app.getName()) ? (String)  Optional.ofNullable(((LinkedHashMap) v).get(app.getName())).orElse("") : "");
            setCompany(jsonUtil.haveKey(v, app.getCompany()) ? (String)  Optional.ofNullable(((LinkedHashMap) v).get(app.getCompany())).orElse("") : "");
            setJob_title(jsonUtil.haveKey(v, app.getJob_title()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getJob_title())).orElse("") : "");
            setBio(jsonUtil.haveKey(v,app.getBio()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getBio())).orElse("") : "");
            setCreated_at(jsonUtil.haveKey(v,app.getCreated_at()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getCreated_at())).orElse("") : "");
            setOpportunity(jsonUtil.haveKey(v,app.getOpportunity()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getOpportunity())).orElse("") : "");
            setNumber_conversions(jsonUtil.haveKey(v,app.getNumber_conversions()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getNumber_conversions())).orElse("") : "");
            setUser(jsonUtil.haveKey(v,app.getUser()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getUser())).orElse("") : "");
            setCustom_fields(jsonUtil.haveKey(v,app.getCustom_fields()) ? (LinkedHashMap) ((LinkedHashMap) v).get(app.getCustom_fields()) : new LinkedHashMap());
            setWebsite(jsonUtil.haveKey(v,app.getWebsite()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getWebsite())).orElse("") : "");
            setPersonal_phone(jsonUtil.haveKey(v,app.getPersonal_phone()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getPersonal_phone())).orElse("") : "");
            setMobile_phone(jsonUtil.haveKey(v,app.getMobile_phone()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getMobile_phone())).orElse("") : "");
            setLead_stage(jsonUtil.haveKey(v,app.getLead_stage()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getLead_stage())).orElse("") : "");
            setFit_score(jsonUtil.haveKey(v,app.getFit_score()) ? (String) Optional.ofNullable(((LinkedHashMap) v).get(app.getFit_score())).orElse("") : "");
            setInterest(jsonUtil.haveKey(v,app.getInterest()) ? Optional.ofNullable(((LinkedHashMap) v).get(app.getInterest())).orElse("").toString() : "");
            setTags(jsonUtil.haveKey(v, app.getTags()) ? (List<String>) Optional.ofNullable(((LinkedHashMap) v).get(app.getTags())).orElse(new ArrayList<>()) : new ArrayList<>());
            setConversions_map(jsonUtil.haveKeywithPattern(v,app.getConversion()) ? jsonUtil.buildHashMapConversions(v, app.getConversion()): new HashMap<>());
        }
    }

    /**
     * Method to build the tags set
     * @param tags
     */
    private void buildTagsObj(List<String> tags, Leads lead){
        tags.forEach(t -> {
            lead.addTag(new Tags(t));
        });
    }

    /**
     * Method to build the custom fields set
     * @param cf
     */
    private void buildCustomFieldsObj(LinkedHashMap cf, Leads lead){
        for (Object key : cf.keySet().toArray()){
            String cfkey = key.toString();
            String cfvalue = cf.get(cfkey).toString();
            lead.addCField(new CustomFields(cfkey,cfvalue));
        }
    }

    private void buildConversionsFiels(HashMap<String, LinkedHashMap> map, Leads lead){
        map.forEach( (k, v) ->{
            lead.addConversions(new Conversion(k.toString(), v.get("source").toString(), v.get("created_at").toString(), v.get("cumulative_sum").toString()));
        });
    }
}
