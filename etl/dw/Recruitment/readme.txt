root_job � o chain principal de execu�ao.

O stage_job e constituido por 2 transforma��es(lastsnapshot e changelog).
-A primeira transforma�ao carrega o ultimo estado de todas as issues applications e job opportunities para as tabelas correspondestes atrav�s da RestAPI do Jira.
-A segunda transforma��o ve o changelog de cada uma das issues applications e job opportunities e e carrega na tabela changelogapp e changelogjobopp.

A tranformacao Calendar e responsavel por calcular e enviar as datas necessarias para popular as tabelas de factos. 
Esta transforma�ao tamb�m insere a primeira linha nas tabelas de factos(o dia de execu�ao).

O gap_job e responsavel pelo carregamento do historico que ser� carregado na primeira execu��o e � responsavel por carregar os gaps(ou seja caso haja gaps ou
haja uma falha no sistema que impe�a a executa�ao do ETL por um ou mais dias. O conteudo deste job sao:
- pop_factjobopp e pop_factapp que sao responsaveis por popular as tabelas de factos com um dia anterior 
- Hist_jobopp e o hist_app sao transforma�oes que verificam se existiram alteracoes no changelog no dia que esta a ser inserido na tabela de factos
- Caso tenha havido alguma altera�ao no dia que esta a ser inserido na tabela de factos � executado o daybefore_app e o day_before_jobopp.

O script de tabela de controle insere o valor mais alto da tabela de factos no ultimo passo de execu��o, ou seja, 
caso haja algum problema durante a execu�ao do etl e a tenha havido algumas inser�oes, estas sao apagadas se a data nao 
for escrita nesta tabela na transforma�ao calendar.

As variaveis de ambiente que estao no kettle.properties s�o:

BaseURL
BaseURLClone
URLApp
URLCloneApp
URLClone_JobOpp
URLJonOpp
FirstDay

Database_Name
Host_Name
User_name
Password
Port_Number
firstday

Start:

Para correr o ETL ser� necessario alterar as credencias do posgres.
Apagar, caso haja algum valor a tabela de controle_table.
Clicar Run.


