-- Table: recruitment.controle_table

-- DROP TABLE recruitment.controle_table;

CREATE TABLE recruitment.controle_table
(
    lastupdateday timestamp without time zone
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE recruitment.controle_table
    OWNER to postgres;