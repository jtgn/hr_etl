-- Table: recruitment.changelogjobopp

-- DROP TABLE recruitment.changelogjobopp;

CREATE TABLE recruitment.changelogjobopp
(
    keyjobopp text COLLATE pg_catalog."default",
    field text COLLATE pg_catalog."default",
    "from" text COLLATE pg_catalog."default",
    fromstring text COLLATE pg_catalog."default",
    "to" text COLLATE pg_catalog."default",
    tostring text COLLATE pg_catalog."default",
    created timestamp without time zone
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE recruitment.changelogjobopp
    OWNER to postgres;