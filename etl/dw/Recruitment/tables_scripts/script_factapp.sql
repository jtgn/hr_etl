-- Table: recruitment.factapp

-- DROP TABLE recruitment.factapp;

CREATE TABLE recruitment.factapp
(
    calendar timestamp without time zone,
    parentid double precision,
    parentkey text COLLATE pg_catalog."default",
    parentissuetype text COLLATE pg_catalog."default",
    idapp double precision,
    keyapp text COLLATE pg_catalog."default",
    statusapp text COLLATE pg_catalog."default",
    issuetypeapp text COLLATE pg_catalog."default",
    createdapp timestamp without time zone,
    assignee text COLLATE pg_catalog."default",
    dateofapplication text COLLATE pg_catalog."default",
    resolutiondateapp timestamp without time zone,
    areaapp text COLLATE pg_catalog."default",
    fullname text COLLATE pg_catalog."default",
    nryearsofcarrer double precision,
    ownerapp text COLLATE pg_catalog."default",
    reasonsfornointerest text COLLATE pg_catalog."default",
    reasonsforrefusal text COLLATE pg_catalog."default",
    referencetype text COLLATE pg_catalog."default",
    referenceby text COLLATE pg_catalog."default",
    typeofcontract text COLLATE pg_catalog."default",
    dateofbirth text COLLATE pg_catalog."default",
    datetocontact text COLLATE pg_catalog."default",
    expectedgrossincome text COLLATE pg_catalog."default",
    expectednetincome text COLLATE pg_catalog."default",
    incomecurrency text COLLATE pg_catalog."default",
    isforrecruitmentiniciative text COLLATE pg_catalog."default",
    technicalinterviewdate text COLLATE pg_catalog."default",
    proposegrossincome text COLLATE pg_catalog."default",
    proposenetincome text COLLATE pg_catalog."default",
    startdate text COLLATE pg_catalog."default",
    duedate text COLLATE pg_catalog."default",
    summary text COLLATE pg_catalog."default",
    educations text COLLATE pg_catalog."default",
    nrofcareeryears double precision,
    locations text COLLATE pg_catalog."default",
    lastopenstate text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE recruitment.factapp
    OWNER to postgres;