-- Table: recruitment.factjobopp

-- DROP TABLE recruitment.factjobopp;

CREATE TABLE recruitment.factjobopp
(
    calendar timestamp without time zone,
    idjobopp double precision,
    keyjobopp text COLLATE pg_catalog."default",
    statusjobopp text COLLATE pg_catalog."default",
    assignee text COLLATE pg_catalog."default",
    issuetypejobopp text COLLATE pg_catalog."default",
    createdjobopp timestamp without time zone,
    resolutiondatejobopp timestamp without time zone,
    projectlocation text COLLATE pg_catalog."default",
    jobpublicationid text COLLATE pg_catalog."default",
    maxbudget double precision,
    minbudget double precision,
    nrofopenpositions double precision,
    experiencelevel text COLLATE pg_catalog."default",
    businessunit text COLLATE pg_catalog."default",
    summary text COLLATE pg_catalog."default",
    department text COLLATE pg_catalog."default",
    openpositions double precision,
    closepositions double precision
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE recruitment.factjobopp
    OWNER to postgres;