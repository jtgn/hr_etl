-- Table: recruitment.changelogapp

-- DROP TABLE recruitment.changelogapp;

CREATE TABLE recruitment.changelogapp
(
    field text COLLATE pg_catalog."default",
    "from" text COLLATE pg_catalog."default",
    fromstring text COLLATE pg_catalog."default",
    "to" text COLLATE pg_catalog."default",
    tostring text COLLATE pg_catalog."default",
    created timestamp without time zone,
    keyapp text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE recruitment.changelogapp
    OWNER to postgres;