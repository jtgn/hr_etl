TRUNCATE prospects.d_proposal;
ALTER TABLE prospects.d_proposal ADD COLUMN dt_reference_date DATE DEFAULT '2000-01-01';
INSERT INTO prospects.d_proposal (id_proposal) VALUES (0);

TRUNCATE prospects.d_prospect;
ALTER TABLE prospects.d_prospect ADD COLUMN dt_reference_date DATE DEFAULT '2000-01-01';
INSERT INTO prospects.d_prospect (id_prospect) VALUES (0);

TRUNCATE prospects.f_prospects;
ALTER TABLE prospects.f_prospects ADD COLUMN dt_reference_date DATE NOT NULL;

CREATE INDEX d_proposal_date_idx ON prospects.d_proposal(dt_reference_date,ref_proposal);

CREATE INDEX d_prospect_date_idx ON prospects.d_prospect(dt_reference_date,ref_prospect);

CREATE INDEX f_prospects_date_idx ON prospects.f_prospects(dt_reference_date,ref_prospect,ref_status);
