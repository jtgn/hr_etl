ALTER TABLE erp.d_document ADD COLUMN dsc_sales_representative VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_document ADD COLUMN dsc_lead_representative VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_document ADD COLUMN dsc_presales_representative VARCHAR(255) DEFAULT 'NA';

CREATE TABLE erp.d_enterprise (
	id_enterprise SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_enterprise VARCHAR(32) DEFAULT '-1',
	dsc_enterprise VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_enterprise (id_enterprise) VALUES (0);

ALTER TABLE erp.f_document_lines ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE erp.f_allocations ADD COLUMN id_enterprise INTEGER DEFAULT 0;
