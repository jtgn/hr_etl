CREATE TABLE sa.f_indirect_bu_allocations_0 (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA'
);

CREATE TABLE sa.f_indirect_bu_allocations_1 (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA'
);

CREATE TABLE sa.f_indirect_bu_allocations_2 (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA'
);

ALTER TABLE sa.f_document_lines ADD COLUMN ref_enterprise VARCHAR(32) DEFAULT '-1';

ALTER TABLE sa.f_allocations_0 ADD COLUMN ref_enterprise VARCHAR(32) DEFAULT '-1';

ALTER TABLE sa.f_allocations_1 ADD COLUMN ref_enterprise VARCHAR(32) DEFAULT '-1';

CREATE TABLE sa.f_indirect_bu_allocations_product_weights (
	dsc_product TEXT NOT NULL,
	nm_year INTEGER NOT NULL,
	nm_value NUMERIC(20,8)
);

INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Service',2014,1);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Service',2015,1);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Service',2016,1);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Resell',2014,0.4);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Resell',2015,0.4);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Resell',2016,0.4);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Product',2014,1);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Product',2015,1);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Product',2016,1);

CREATE TABLE sa.f_indirect_bu_allocations_investment_weights (
	ref_project TEXT NOT NULL,
	nm_year INTEGER NOT NULL,
	nm_ms_value NUMERIC(20,8),
	nm_bi_value NUMERIC(20,8),
	nm_es_value NUMERIC(20,8),
	nm_dmi_value NUMERIC(20,8)
);

INSERT INTO sa.f_indirect_bu_allocations_investment_weights VALUES ('*',2014,0.25,0.25,0.25,0.25);
INSERT INTO sa.f_indirect_bu_allocations_investment_weights VALUES ('*',2015,0.25,0.25,0.25,0.25);
INSERT INTO sa.f_indirect_bu_allocations_investment_weights VALUES ('*',2016,0.25,0.25,0.25,0.25);
