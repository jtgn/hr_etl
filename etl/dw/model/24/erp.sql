CREATE TABLE erp.d_physical_office (
	id_physical_office SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_physical_office VARCHAR(32) DEFAULT '-1',
	dsc_physical_office VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	nm_area INTEGER DEFAULT 0
);

INSERT INTO erp.d_physical_office (id_physical_office) VALUES (0);
