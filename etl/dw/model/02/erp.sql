
CREATE TABLE erp.d_contract (
	id_contract SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_contract VARCHAR(32) DEFAULT '-1',
	dsc_contract VARCHAR(255) DEFAULT 'NA',
	flg_is_active CHAR(1) DEFAULT 'N',
	dt_contract_start_date DATE DEFAULT '2000-01-01',
	dt_contract_end_date DATE DEFAULT '2999-12-31',
	dsc_contract_type VARCHAR(255) DEFAULT 'NA',
	dsc_contract_start_reason VARCHAR(255) DEFAULT 'NA',
	dsc_contract_end_reason VARCHAR(255) DEFAULT 'NA',
	dsc_irct VARCHAR(255) DEFAULT 'NA',
	dsc_irct_application VARCHAR(255) DEFAULT 'NA',
	dsc_working_regime VARCHAR(255) DEFAULT 'NA',
	nm_pnt_time INTEGER DEFAULT 0,
	dsc_work_time_duration VARCHAR(255) DEFAULT 'NA',
	dsc_work_time_organization VARCHAR(255) DEFAULT 'NA',
	dsc_fixed_term_contract_end_reason VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_contract (id_contract) VALUES (0);

CREATE TABLE erp.d_career (
	id_career SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_career VARCHAR(32) DEFAULT '-1',
	dsc_career VARCHAR(255) DEFAULT 'NA',
	dt_career_start_date DATE DEFAULT '2000-01-01',
	dt_career_end_date DATE DEFAULT '2999-12-31'
);

INSERT INTO erp.d_career (id_career) VALUES (0);

CREATE TABLE erp.d_salary_category (
	id_salary_category SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',	
	ref_salary_category VARCHAR(32) DEFAULT '-1',
	dsc_salary_category VARCHAR(255) DEFAULT 'NA',
	dt_category_start_date DATE DEFAULT '2000-01-01',
	dt_category_end_date DATE DEFAULT '2999-12-31',
	nm_base_income NUMERIC DEFAULT 0,
	nm_work_exemption NUMERIC DEFAULT 0,
	nm_food_allowance NUMERIC DEFAULT 0,
	nm_other_expenses NUMERIC DEFAULT 0,
	nm_social_security NUMERIC DEFAULT 0,
	dt_exemption_start_date DATE,
	dt_exemption_end_date DATE,
	nm_social_security_exeption NUMERIC DEFAULT 0,
	flg_is_active CHAR(1) DEFAULT 'N'
);

INSERT INTO erp.d_salary_category (id_salary_category,dt_exemption_start_date,dt_exemption_end_date) VALUES (0,NULL,NULL);

CREATE TABLE erp.d_health (
	id_health SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_health VARCHAR(32) DEFAULT '-1',
	dsc_health VARCHAR(255) DEFAULT 'NA',
	dt_date DATE DEFAULT '2000-01-01',
	dsc_place VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_health (id_health) VALUES (0);

CREATE TABLE erp.d_employee_document (
	id_employee_document SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_employee_document VARCHAR(32) DEFAULT '-1',
	dsc_employee_document VARCHAR(255) DEFAULT 'NA',
	dt_date DATE DEFAULT '2000-01-01'
);

INSERT INTO erp.d_employee_document (id_employee_document) VALUES (0);

CREATE TABLE erp.d_benefit (
	id_benefit SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_benefit VARCHAR(32) DEFAULT '-1',
	dsc_benefit VARCHAR(255) DEFAULT 'NA',
	flg_is_active CHAR(1) DEFAULT 'N',
	dsc_benefit_number VARCHAR(255) DEFAULT 'NA',
	flg_is_payslip_item CHAR(1) DEFAULT 'N',
	flg_is_paid_by_company CHAR(1) DEFAULT 'N',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	dsc_benefit_category VARCHAR(255) DEFAULT 'NA',
	nm_amount NUMERIC DEFAULT 0,
	nm_supported_percentage NUMERIC DEFAULT 0,
	nm_supported_amount NUMERIC DEFAULT 0,
	flg_is_receivable CHAR(1) DEFAULT 'N',
	dsc_benefit_description VARCHAR(255) DEFAULT 'NA'	
);


INSERT INTO erp.d_benefit (id_benefit) VALUES (0);

CREATE TABLE erp.d_contractor (
	id_contractor SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_contractor_type VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	nm_number_of_workers NUMERIC DEFAULT 0,
	ref_contractor VARCHAR(32) DEFAULT '-1'
);

INSERT INTO erp.d_contractor (id_contractor) VALUES (0);

CREATE TABLE erp.d_qualification (
	id_qualification SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_qualification VARCHAR(32) DEFAULT '-1',
	dsc_course VARCHAR(255) DEFAULT 'NA',
	dsc_qualification VARCHAR(255) DEFAULT 'NA',
	dsc_place VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	dsc_qualification_description VARCHAR(255) DEFAULT 'NA',
	flg_is_pre_bologna CHAR(1) DEFAULT 'N',
	nm_cost NUMERIC DEFAULT 0,
	dsc_qualification_status VARCHAR(255) DEFAULT 'NA',
	nm_hours NUMERIC DEFAULT 0,
	dsc_period VARCHAR(255) DEFAULT 'NA',
	dsc_formation_field VARCHAR(255) DEFAULT 'NA',
	dsc_formation_modality VARCHAR(255) DEFAULT 'NA',
	dsc_initiative VARCHAR(255) DEFAULT 'NA',
	dsc_schedule VARCHAR(255) DEFAULT 'NA',
	dsc_formation_place VARCHAR(255) DEFAULT 'NA',
	dsc_certification_type VARCHAR(255) DEFAULT 'NA',
	dsc_education_code VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_qualification (id_qualification) VALUES (0);

CREATE TABLE erp.f_human_resources (
	id_f_human_resources SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,
	id_contract INTEGER NOT NULL,
	id_career INTEGER NOT NULL,
	id_salary_category INTEGER NOT NULL,
	id_health INTEGER NOT NULL,
	id_employee_document INTEGER NOT NULL,
	id_benefit INTEGER NOT NULL,
	id_contractor INTEGER NOT NULL,
	id_qualification INTEGER NOT NULL,
	id_resource INTEGER NOT NULL,
	ref_resource VARCHAR(32) NOT NULL
);

CREATE INDEX f_human_resources_ref_resource_idx ON erp.f_human_resources(ref_resource);

ALTER TABLE erp.d_vendor ADD COLUMN dsc_vendor_code VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_customer ADD COLUMN dsc_customer_code VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_resource_bu VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_resource_department VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_resource_office VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN flg_is_active CHAR(1) DEFAULT 'N';

ALTER TABLE erp.d_resource ADD COLUMN dsc_resource_category VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dt_collaboration_start_date DATE DEFAULT '2000-01-01';

ALTER TABLE erp.d_resource ADD COLUMN dt_collaboration_end_date DATE;
