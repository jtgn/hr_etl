
CREATE TABLE rd.f_human_resources (
	ref_resource VARCHAR(32) NOT NULL,
	ts_contract TIMESTAMP NOT NULL,
	ts_career TIMESTAMP NOT NULL,
	ts_salary_category TIMESTAMP NOT NULL,
	ts_health TIMESTAMP NOT NULL,
	ts_employee_document TIMESTAMP NOT NULL,
	ts_benefit TIMESTAMP NOT NULL,
	ts_contractor TIMESTAMP NOT NULL,
	ts_qualification TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_human_resources_backup (
	ref_resource VARCHAR(32) NOT NULL,
	ts_contract TIMESTAMP NOT NULL,
	ts_career TIMESTAMP NOT NULL,
	ts_salary_category TIMESTAMP NOT NULL,
	ts_health TIMESTAMP NOT NULL,
	ts_employee_document TIMESTAMP NOT NULL,
	ts_benefit TIMESTAMP NOT NULL,
	ts_contractor TIMESTAMP NOT NULL,
	ts_qualification TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE INDEX rd_f_human_resources_ref_resource_idx ON rd.f_human_resources(ref_resource);

TRUNCATE rd.f_atlassian_sales;

TRUNCATE rd.f_atlassian_sales_backup;
