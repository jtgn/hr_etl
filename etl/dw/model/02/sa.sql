
CREATE TABLE sa.f_human_resources (
	ref_contract VARCHAR(32) NOT NULL,
	ref_career VARCHAR(32) NOT NULL,
	ref_salary_category VARCHAR(32) NOT NULL,
	ref_health VARCHAR(32) NOT NULL,
	ref_employee_document VARCHAR(32) NOT NULL,
	ref_benefit VARCHAR(32) NOT NULL,
	ref_contractor VARCHAR(32) NOT NULL,
	ref_qualification VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL
);

ALTER TABLE sa.f_atlassian_sales ADD COLUMN ref_jira_server VARCHAR(50) NOT NULL DEFAULT '-1';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_jira_server_license VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_jira_server_limit INTEGER NOT NULL DEFAULT 0;

TRUNCATE sa.f_atlassian_sales;
