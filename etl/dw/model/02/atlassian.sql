
CREATE TABLE atlassian.d_server_license (
	id_server_license SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_server_license VARCHAR(50) DEFAULT '-1',
	dsc_server_license VARCHAR(255) DEFAULT 'NA',
	nm_server_user_limit INTEGER DEFAULT 0
);

INSERT INTO atlassian.d_server_license (id_server_license) VALUES (0);

TRUNCATE atlassian.f_sales;

ALTER TABLE atlassian.f_sales ADD COLUMN id_server_license INTEGER NOT NULL;
