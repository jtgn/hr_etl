

CREATE TABLE atlassian.d_marketplace_plugin (
	id_marketplace_plugin SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_plugin_key VARCHAR(255) DEFAULT 'NA',
	dsc_plugin VARCHAR(255) DEFAULT 'NA',
	dsc_category VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_marketplace_plugin (id_marketplace_plugin) VALUES (0);

CREATE TABLE atlassian.f_atlassian_marketplace (
	id_marketplace_plugin INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	nm_installs INTEGER NOT NULL,
	nm_downloads INTEGER NOT NULL,
	nm_stars NUMERIC(20,8) NOT NULL,
	nm_ratings INTEGER NOT NULL,
	nm_score NUMERIC(20,8) NOT NULL
);

CREATE INDEX f_atlassian_marketplace_idx ON atlassian.f_atlassian_marketplace (id_analytical_date,id_marketplace_plugin);
