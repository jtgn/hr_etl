
CREATE TABLE rd.f_erp_expenses (
	ref_expense VARCHAR(32) NOT NULL,
	ts_update_expense TIMESTAMP NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ts_update_expense_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_erp_expenses_backup (
	ref_expense VARCHAR(32) NOT NULL,
	ts_update_expense TIMESTAMP NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ts_update_expense_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_timesheets_expenses (
	ref_expense VARCHAR(32) NOT NULL,
	ts_update_expense TIMESTAMP NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ts_update_expense_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_timesheets_expenses_backup (
	ref_expense VARCHAR(32) NOT NULL,
	ts_update_expense TIMESTAMP NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ts_update_expense_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);
