
CREATE TABLE sa.f_atlassian_marketplace (
	dsc_plugin_key VARCHAR(255) NOT NULL,
	dsc_plugin VARCHAR(255) NOT NULL,
	dsc_category VARCHAR(255) NOT NULL,
	nm_installs INTEGER NOT NULL,
	nm_downloads INTEGER NOT NULL,
	nm_stars NUMERIC(20,8) NOT NULL,
	nm_ratings INTEGER NOT NULL,
	nm_score NUMERIC(20,8) NOT NULL,
	dt_date DATE NOT NULL
);

CREATE INDEX f_atlassian_marketplace_idx ON sa.f_atlassian_marketplace (dsc_plugin_key,dt_date);

CREATE TABLE sa.f_erp_expenses (
	ref_expense VARCHAR(32) NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	dt_report_date DATE NOT NULL,
	dsc_report_description VARCHAR(255) NOT NULL,
	flg_is_processed CHAR(1) NOT NULL,
	flg_is_esr_processed CHAR(1) NOT NULL,
	dt_expense_date DATE NOT NULL,
	nm_quantity NUMERIC(20,8) NOT NULL,
	nm_amount NUMERIC(20,8) NOT NULL,
	dsc_expense_description VARCHAR(255) NOT NULL,
	dsc_expense_type VARCHAR(255) NOT NULL,
	ref_project VARCHAR(32) NOT NULL,
	nm_unit_price NUMERIC(20,8) NOT NULL
);

CREATE TABLE sa.f_timesheets_expenses (
	ref_expense VARCHAR(32) NOT NULL,
	ref_expense_line VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	dt_report_date DATE NOT NULL,
	dsc_report_description VARCHAR(255) NOT NULL,
	dt_expense_date DATE NOT NULL,
	nm_quantity NUMERIC(20,8) NOT NULL,
	nm_amount NUMERIC(20,8) NOT NULL,
	dsc_expense_description VARCHAR(255) NOT NULL,
	dsc_expense_type VARCHAR(255) NOT NULL,
	ref_project VARCHAR(255) NOT NULL,
	nm_unit_price NUMERIC(20,8) NOT NULL,
	dsc_report_status VARCHAR(255) NOT NULL,
	dsc_expense_status VARCHAR(255) NOT NULL,
	flg_is_reimburse CHAR(1) NOT NULL,
	flg_is_billed CHAR(1) NOT NULL
);
