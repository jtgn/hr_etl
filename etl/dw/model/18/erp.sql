ALTER TABLE erp.d_project ADD COLUMN dsc_meta_project VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.f_indirect_project_allocations_des ADD COLUMN dsc_meta_project VARCHAR(255) DEFAULT 'NA';
