CREATE TABLE rd.f_budget (
	ref_budget VARCHAR(32) NOT NULL,
	ts_budget TIMESTAMP NOT NULL,
	ref_task VARCHAR(32) NOT NULL,
	ts_task TIMESTAMP NOT NULL,
	ref_task_resource VARCHAR(32) NOT NULL,
	ts_task_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_budget_backup (
	ref_budget VARCHAR(32) NOT NULL,
	ts_budget TIMESTAMP NOT NULL,
	ref_task VARCHAR(32) NOT NULL,
	ts_task TIMESTAMP NOT NULL,
	ref_task_resource VARCHAR(32) NOT NULL,
	ts_task_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);
