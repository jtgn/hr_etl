ALTER TABLE erp.d_activity ADD COLUMN flg_is_billable CHAR(1) DEFAULT 'N';

ALTER TABLE erp.d_resource ADD COLUMN dt_id_expiration_date DATE DEFAULT '2000-01-01';

ALTER TABLE erp.d_resource ADD COLUMN dt_pro_start_date DATE DEFAULT '2000-01-01';

CREATE TABLE erp.f_budget (
	ref_budget VARCHAR(32) NOT NULL,
	id_enterprise INTEGER NOT NULL,
	flg_is_budget_active CHAR(1) NOT NULL,
	dsc_budget VARCHAR(255) NOT NULL,
	dt_budget_start_date DATE NOT NULL,
	dt_budget_end_date DATE NOT NULL,
	nm_budget_version INTEGER NOT NULL,
	nm_budgeted_value NUMERIC(20,8) NOT NULL,
	nm_remaining_value NUMERIC(20,8) NOT NULL,
	id_project INTEGER NOT NULL,
	flg_is_monthly CHAR(1) NOT NULL,
	flg_is_budget_validated CHAR(1) NOT NULL,
	ref_task VARCHAR(32) NOT NULL,
	flg_is_task_active CHAR(1) NOT NULL,
	dsc_task VARCHAR(255) NOT NULL,
	nm_task_version INTEGER NOT NULL,
	nm_task_value NUMERIC(20,8) NOT NULL,
	nm_non_resource_value NUMERIC(20,8) NOT NULL,
	flg_is_task_validated CHAR(1) NOT NULL,
	ref_task_resource VARCHAR(32) NOT NULL,
	flg_is_task_resource_active CHAR(1) NOT NULL,
	id_resource INTEGER NOT NULL,
	nm_resource_value NUMERIC(20,8) NOT NULL,
	nm_time_percentage NUMERIC(20,8) NOT NULL,
	flg_is_task_resource_validated CHAR(1) NOT NULL,
	nm_task_resource_version INTEGER NOT NULL
);
