CREATE SCHEMA cp;

CREATE TABLE cp.f_resource_allocation (
	ref_resource_allocation VARCHAR(255) NOT NULL,
	id_project INTEGER NOT NULL,
	dsc_label VARCHAR(255) NOT NULL,
	id_resource INTEGER NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	dsc_bar_color VARCHAR(255) NOT NULL,
	nm_allocation_percentage NUMERIC(20,8) NOT NULL,
	id_reference_date INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	id_enterprise INTEGER NOT NULL
);
