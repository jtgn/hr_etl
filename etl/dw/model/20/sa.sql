CREATE TABLE sa.f_budget (
	ref_budget VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL,
	flg_is_budget_active CHAR(1) NOT NULL,
	dsc_budget VARCHAR(255) NOT NULL,
	dt_budget_start_date DATE NOT NULL,
	dt_budget_end_date DATE NOT NULL,
	nm_budget_version INTEGER NOT NULL,
	nm_budgeted_value NUMERIC(20,8) NOT NULL,
	nm_remaining_value NUMERIC(20,8) NOT NULL,
	ref_project VARCHAR(32) NOT NULL,
	flg_is_monthly CHAR(1) NOT NULL,
	flg_is_budget_validated CHAR(1) NOT NULL,
	ref_task VARCHAR(32) NOT NULL,
	flg_is_task_active CHAR(1) NOT NULL,
	dsc_task VARCHAR(255) NOT NULL,
	nm_task_version INTEGER NOT NULL,
	nm_task_value NUMERIC(20,8) NOT NULL,
	nm_non_resource_value NUMERIC(20,8) NOT NULL,
	flg_is_task_validated CHAR(1) NOT NULL,
	ref_task_resource VARCHAR(32) NOT NULL,
	flg_is_task_resource_active CHAR(1) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	nm_resource_value NUMERIC(20,8) NOT NULL,
	nm_time_percentage NUMERIC(20,8) NOT NULL,
	flg_is_task_resource_validated CHAR(1) NOT NULL,
	nm_task_resource_version INTEGER NOT NULL
);

CREATE TABLE sa.f_resource_allocation (
	ref_resource_allocation VARCHAR(255) NOT NULL,
	ref_project VARCHAR(255) NOT NULL,
	dsc_label VARCHAR(255) NOT NULL,
	ref_resource VARCHAR(255) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	dsc_bar_color VARCHAR(255) NOT NULL,
	nm_allocation_percentage NUMERIC(20,8) NOT NULL,
	dt_reference_date DATE NOT NULL
);

CREATE TABLE sa.f_resource_allocation_backup (
	ref_resource_allocation VARCHAR(255) NOT NULL,
	ref_project VARCHAR(255) NOT NULL,
	dsc_label VARCHAR(255) NOT NULL,
	ref_resource VARCHAR(255) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	dsc_bar_color VARCHAR(255) NOT NULL,
	nm_allocation_percentage NUMERIC(20,8) NOT NULL,
	dt_reference_date DATE NOT NULL
);
