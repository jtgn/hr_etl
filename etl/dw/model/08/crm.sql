CREATE SCHEMA crm;

CREATE TABLE crm.d_account (
	id_account SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_account VARCHAR(36) DEFAULT '-1',
	dsc_account_name VARCHAR(255) DEFAULT 'NA',
	dsc_account_type VARCHAR(255) DEFAULT 'NA',
	dsc_account_industry VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_account (id_account) VALUES (0);

CREATE TABLE crm.d_campaign (
	id_campaign SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_campaign VARCHAR(36) DEFAULT '-1',
	dsc_campaign_name VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	dsc_campaign_status VARCHAR(255) DEFAULT 'NA',
	dsc_campaign_type VARCHAR(255) DEFAULT 'NA',
	dsc_campaign_content TEXT DEFAULT 'NA'
);

INSERT INTO crm.d_campaign (id_campaign) VALUES (0);

CREATE TABLE crm.d_lead_source (
	id_lead_source SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_lead_source VARCHAR(255) DEFAULT 'NA',
	dsc_lead_source_details TEXT DEFAULT 'NA'
);

INSERT INTO crm.d_lead_source (id_lead_source) VALUES (0);

CREATE TABLE crm.d_line_of_business (
	id_line_of_business SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_line_of_business_code VARCHAR(255) DEFAULT 'NA',
	dsc_line_of_business_name VARCHAR(255) DEFAULT 'NA',
	dsc_business_unit VARCHAR(255) DEFAULT 'NA',
	dsc_business_unit_code VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_line_of_business (id_line_of_business) VALUES (0);

CREATE TABLE crm.d_project_type (
	id_project_type SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_project_type VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_project_type (id_project_type) VALUES (0);

CREATE TABLE crm.d_opportunity (
	id_opportunity SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_opportunity_name VARCHAR(255) DEFAULT 'NA',
	dsc_opportunity_type VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_opportunity (id_opportunity) VALUES (0);

CREATE TABLE crm.d_stage (
	id_stage SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_stage VARCHAR(255) DEFAULT 'NA',
	nm_stage_order INTEGER DEFAULT 0
);

INSERT INTO crm.d_stage (id_stage) VALUES (0);

CREATE TABLE crm.d_business_partner (
	id_business_partner SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_business_partner VARCHAR(36) DEFAULT '-1',
	dsc_business_partner_name VARCHAR(255) DEFAULT 'NA',
	dsc_business_partner_title VARCHAR(255) DEFAULT 'NA',
	dsc_business_partner_status VARCHAR(255) DEFAULT 'NA',
	dsc_employee_code VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_business_partner (id_business_partner) VALUES (0);

CREATE TABLE crm.d_contact (
	id_contact SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_contact VARCHAR(36) DEFAULT '-1',
	dsc_contact_department VARCHAR(255) DEFAULT 'NA',
	dsc_contact_title VARCHAR(255) DEFAULT 'NA',
	dsc_contact_name VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_contact (id_contact) VALUES (0);

CREATE TABLE crm.d_lead_contact (
	id_lead_contact SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_contact_name VARCHAR(255) DEFAULT 'NA',
	dsc_contact_title VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_lead_contact (id_lead_contact) VALUES (0);

CREATE TABLE crm.d_status (
	id_status SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_status VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO crm.d_status (id_status) VALUES (0);

CREATE TABLE crm.d_lead_attendant (
	id_lead_attendant SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_attendant_status VARCHAR(255) DEFAULT 'NA',
	dsc_industry VARCHAR(255) DEFAULT 'NA',
	dsc_tax_code VARCHAR(255) DEFAULT 'NA',
	dsc_language VARCHAR(255) DEFAULT 'NA',
	flg_is_competitor CHAR(1) DEFAULT 'N'
);

INSERT INTO crm.d_lead_attendant (id_lead_attendant) VALUES (0);

CREATE TABLE crm.d_lead_account (
	id_lead_account SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_account_name VARCHAR(255) DEFAULT 'NA',
	dsc_account_description TEXT DEFAULT 'NA'
);

INSERT INTO crm.d_lead_account (id_lead_account) VALUES (0);

CREATE TABLE crm.f_opportunities (
	id_f_opportunities SERIAL PRIMARY KEY,
	id_create_date INTEGER NOT NULL,-- FK dim_date
	id_expected_close_date INTEGER NOT NULL,-- FK dim_date
	id_account INTEGER NOT NULL,-- FK crm.dim_account
	id_campaign INTEGER NOT NULL,-- FK crm.dim_campaign
	id_lead_source INTEGER NOT NULL,-- FK crm.dim_lead_source
	id_line_of_business INTEGER NOT NULL,-- FK crm.dim_line_of_business
	id_project_type INTEGER NOT NULL,-- FK crm.dim_project_type
	id_opportunity INTEGER NOT NULL,-- FK crm.dim_opportunity
	id_stage INTEGER NOT NULL,-- crm.dim_stage
	id_business_partner INTEGER NOT NULL,-- FK crm.dim_business_partner
	ref_opportunity VARCHAR(36) NOT NULL,
	dsc_opportunity_description TEXT NOT NULL,
	nm_amount NUMERIC(20,8) NOT NULL,
	nm_probability NUMERIC(20,8) NOT NULL
);

CREATE TABLE crm.f_opportunities_backup (
	id_f_opportunities INTEGER NOT NULL,
	id_create_date INTEGER NOT NULL,
	id_expected_close_date INTEGER NOT NULL,
	id_dim_account INTEGER NOT NULL,
	id_dim_campaign INTEGER NOT NULL,
	id_dim_lead_source INTEGER NOT NULL,
	id_dim_line_of_business INTEGER NOT NULL,
	id_dim_project_type INTEGER NOT NULL,
	id_dim_opportunity INTEGER NOT NULL,
	id_dim_stage INTEGER NOT NULL,
	id_dim_business_partner INTEGER NOT NULL,
	ref_opportunity VARCHAR(36),
	dsc_opportunity_description TEXT,
	nm_amount NUMERIC(20,8),
	nm_probability NUMERIC(20,8)
);

CREATE TABLE crm.f_leads (
	id_f_leads SERIAL PRIMARY KEY,
	id_create_date INTEGER NOT NULL, -- FK dim_date
	id_business_partner INTEGER NOT NULL,-- FK crm.dim_business_partner
	id_lead_contact INTEGER NOT NULL, -- FK crm.dim_lead_contact
	id_lead_source INTEGER NOT NULL,-- FK crm.dim_lead_source
	id_status INTEGER NOT NULL, -- FK crm.dim_status
	id_lead_account INTEGER NOT NULL, -- FK crm.dim_lead_account
	id_account INTEGER NOT NULL,-- FK crm.dim_account
	id_contact INTEGER NOT NULL, -- FK crm.dim_contact
	id_opportunity INTEGER NOT NULL,-- FK crm.dim_opportunity
	id_campaign INTEGER NOT NULL,-- FK crm.dim_campaign
	id_lead_attendant INTEGER NOT NULL, -- FK crm.dim_lead_attendant
	ref_lead VARCHAR(36),
	dsc_lead_description TEXT
);

CREATE TABLE crm.f_leads_backup (
	id_f_leads INTEGER NOT NULL,
	id_create_date INTEGER NOT NULL,
	id_business_partner INTEGER NOT NULL,
	id_lead_contact INTEGER NOT NULL,
	id_lead_source INTEGER NOT NULL,
	id_status INTEGER NOT NULL,
	id_lead_account INTEGER NOT NULL,
	id_account INTEGER NOT NULL,
	id_contact INTEGER NOT NULL,
	id_opportunity INTEGER NOT NULL,
	id_campaign INTEGER NOT NULL,
	id_lead_attendant INTEGER NOT NULL,
	ref_lead VARCHAR(36),
	dsc_lead_description TEXT
);
