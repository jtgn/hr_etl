TRUNCATE atlassian.d_country;

INSERT INTO atlassian.d_country (id_country,dsc_iso_code_two,dsc_iso_code_three, dsc_country_name) VALUES (0,'N0','NA0','NA0');

TRUNCATE atlassian.d_tech_contact;

INSERT INTO atlassian.d_tech_contact (id_tech_contact,dsc_tech_contact_country) VALUES (0,'NA0');

TRUNCATE sa.f_atlassian_sales;
TRUNCATE rd.f_atlassian_sales;
TRUNCATE atlassian.f_sales;
