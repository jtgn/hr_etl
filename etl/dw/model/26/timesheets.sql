
ALTER TABLE timesheets.f_timesheets ADD COLUMN id_enterprise INTEGER DEFAULT 0;

CREATE INDEX f_vacations_id_resource_id_analytical_date_idx ON timesheets.f_vacations(id_resource, id_analytical_date);
