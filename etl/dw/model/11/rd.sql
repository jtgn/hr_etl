CREATE TABLE rd.f_amortizations (
	ref_amortizationline VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_amortizations_backup (
	ref_amortizationline VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_asset_history (
	ref_asset_history VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_asset_history_backup (
	ref_asset_history VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);
