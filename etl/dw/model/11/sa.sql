CREATE TABLE sa.f_amortizations (
	ref_amortizationline VARCHAR(32) NOT NULL,
	ref_asset VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_department VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_amortization_amount NUMERIC(20,8) NOT NULL
);

CREATE TABLE sa.f_asset_history (
	ref_asset_history VARCHAR(32) NOT NULL,
	ref_asset VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	ref_department VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	dt_purchase_date DATE NOT NULL,
	dt_cancelled_date DATE NOT NULL
);

ALTER TABLE sa.f_payments ADD COLUMN ref_enterprise VARCHAR(32) DEFAULT '-1';

ALTER TABLE sa.f_human_resources ADD COLUMN ref_enterprise VARCHAR(32) DEFAULT '-1';

ALTER TABLE sa.f_indirect_bu_allocations_0 ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE sa.f_indirect_bu_allocations_1 ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE sa.f_indirect_bu_allocations_2 ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE sa.f_indirect_bu_allocations_3 ADD COLUMN id_enterprise INTEGER DEFAULT 0;
