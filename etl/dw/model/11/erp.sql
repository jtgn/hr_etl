ALTER TABLE erp.d_salary_category ADD COLUMN nm_annual_prize NUMERIC DEFAULT 0;

ALTER TABLE erp.d_career ADD COLUMN nm_minimum_rate NUMERIC DEFAULT 0;

ALTER TABLE erp.d_career ADD COLUMN nm_maximum_rate NUMERIC DEFAULT 0;

ALTER TABLE erp.d_career ADD COLUMN nm_minimum_cost NUMERIC DEFAULT 0;

ALTER TABLE erp.d_career ADD COLUMN nm_maximum_cost NUMERIC DEFAULT 0;

CREATE TABLE erp.d_asset (
	id_asset SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_asset VARCHAR(32) DEFAULT '-1',
	dsc_asset VARCHAR(255) DEFAULT 'NA',
	dsc_asset_key VARCHAR(255) DEFAULT 'NA',
	ref_asset_group VARCHAR(32) DEFAULT '-1',
	dsc_asset_group VARCHAR(255) DEFAULT 'NA',
	dsc_asset_group_key VARCHAR(255) DEFAULT 'NA',
	dsc_asset_group_description VARCHAR(255) DEFAULT 'NA',
	nm_asset_group_depreciation_rate NUMERIC DEFAULT 0,
	nm_usable_life_years NUMERIC DEFAULT 0,
	flg_is_full_depreciated CHAR(1) DEFAULT 'N',
	dt_amortization_start_date DATE DEFAULT '2000-01-01',
	dt_amortization_end_date DATE DEFAULT '2999-12-31',
	nm_annual_depreciation_rate NUMERIC DEFAULT 0,
	nm_asset_value NUMERIC DEFAULT 0,
	dt_cancelled_date DATE DEFAULT '2000-01-01',
	dsc_asset_description VARCHAR(255) DEFAULT 'NA',
	dsc_serial_number VARCHAR(255) DEFAULT 'NA',
	dsc_condition VARCHAR(255) DEFAULT 'NA',
	flg_is_depreciated CHAR(1) DEFAULT 'N',
	nm_quantity NUMERIC DEFAULT 0,
	dsc_depreciation_type VARCHAR(255) DEFAULT 'NA',
	flg_is_low_cost CHAR(1) DEFAULT 'N',
	nm_asset_fiscal_value NUMERIC DEFAULT 0,
	nm_alienation_amount NUMERIC DEFAULT 0,
	nm_depreciation_amount NUMERIC DEFAULT 0,
	nm_residual_asset_value NUMERIC DEFAULT 0,
	nm_previosly_depreciated_amount NUMERIC DEFAULT 0,
	nm_depreciated_value NUMERIC DEFAULT 0,
	nm_depreciated_plan NUMERIC DEFAULT 0,
	flg_is_cancelled CHAR(1) DEFAULT 'N',
	flg_is_alienated CHAR(1) DEFAULT 'N'
);

INSERT INTO erp.d_asset (id_asset) VALUES (0);

CREATE TABLE erp.f_amortizations (
	id_f_amortizations SERIAL PRIMARY KEY,
	ref_amortizationline VARCHAR(32),
	id_asset INTEGER NOT NULL,
	id_document_line INTEGER NOT NULL,
	id_department INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	id_enterprise INTEGER NOT NULL,
	nm_amortization_amount NUMERIC(20,8) NOT NULL
);

CREATE TABLE erp.f_asset_history (
	id_f_asset_history SERIAL PRIMARY KEY,
	ref_asset_history VARCHAR(32),
	id_asset INTEGER NOT NULL,
	id_resource INTEGER NOT NULL,
	id_department INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	id_enterprise INTEGER NOT NULL
);

ALTER TABLE erp.f_payments ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE erp.f_human_resources ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE erp.f_indirect_bu_allocations ADD COLUMN id_enterprise INTEGER DEFAULT 0;

ALTER TABLE erp.f_indirect_project_allocations ADD COLUMN id_enterprise INTEGER DEFAULT 0;
