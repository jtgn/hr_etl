ALTER TABLE erp.d_salary_category ADD COLUMN nm_national_allowance_days NUMERIC DEFAULT 0;

ALTER TABLE erp.d_salary_category ADD COLUMN nm_national_allowance NUMERIC DEFAULT 0;

ALTER TABLE erp.d_salary_category ADD COLUMN nm_international_allowance_days NUMERIC DEFAULT 0;

ALTER TABLE erp.d_salary_category ADD COLUMN nm_international_allowance NUMERIC DEFAULT 0;

ALTER TABLE erp.f_asset_history ADD COLUMN id_document_line INTEGER DEFAULT 0;

CREATE INDEX d_project_ref_line_of_business_idx ON erp.d_project(ref_line_of_business);

CREATE INDEX d_department_dsc_business_unit_idx ON erp.d_department(dsc_business_unit);

CREATE INDEX d_customer_ref_customer_idx ON erp.d_customer(ref_customer);

CREATE INDEX f_allocations_id_customer_idx ON erp.f_allocations(id_customer);

CREATE INDEX d_document_ref_subbase_type_idx ON erp.d_document(ref_subbase_type);

CREATE INDEX d_project_dsc_project_code_idx ON erp.d_project(dsc_project_code);

CREATE INDEX d_activity_dsc_activity_idx ON erp.d_activity(dsc_activity);

CREATE INDEX d_project_id_department_idx ON erp.d_project(id_department);

CREATE INDEX d_project_dsc_project_cost_type_idx ON erp.d_project(dsc_project_cost_type);

CREATE INDEX d_department_ref_department_idx ON erp.d_department(ref_department);

CREATE INDEX d_department_dsc_department_dsc_business_unit_description_idx ON erp.d_department(dsc_department, dsc_business_unit_description);

ALTER TABLE erp.d_document ADD COLUMN dsc_order_reference VARCHAR(255) DEFAULT 'NA';
