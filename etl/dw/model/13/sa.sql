CREATE TABLE sa.f_indirect_bu_allocations_4 (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA',
	id_enterprise INTEGER NOT NULL
);

ALTER TABLE sa.f_asset_history ADD COLUMN ref_document_line VARCHAR(32) DEFAULT '-1';

DROP INDEX sa.sa_f_atlassian_sales_refs_idx;

CREATE INDEX f_atlassian_sales_ref_license_idx ON sa.f_atlassian_sales (ref_license);

CREATE INDEX f_atlassian_sales_ref_license_ref_invoice_idx ON sa.f_atlassian_sales (ref_license, ref_invoice);

CREATE INDEX f_indirect_bu_allocations_investment_we_nm_year_ref_project_idx ON sa.f_indirect_bu_allocations_investment_weights (nm_year, ref_project);

CREATE TABLE sa.f_indirect_bu_allocations_5 (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA',
	id_enterprise INTEGER NOT NULL
);
