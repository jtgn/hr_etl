CREATE INDEX f_atlassian_sales_flagfield_idx ON rd.f_atlassian_sales (flagfield);

CREATE INDEX f_atlassian_sales_ref_license_ref_invoice_idx ON rd.f_atlassian_sales (ref_license, ref_invoice);

