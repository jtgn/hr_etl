CREATE INDEX d_bill_contact_dsc_bill_contact_email_idx ON atlassian.d_bill_contact (dsc_bill_contact_email);

CREATE INDEX d_country_dsc_iso_code_two_idx ON atlassian.d_country (dsc_iso_code_two);

CREATE INDEX d_expert_dsc_expert_name_idx ON atlassian.d_expert (dsc_expert_name);

CREATE INDEX d_license_type_dsc_lincese_type_dsc_license_size_dsc_sale_t_idx ON atlassian.d_license_type (dsc_lincese_type, dsc_license_size, dsc_sale_type);

CREATE INDEX d_plugin_dsc_plugin_key_idx ON atlassian.d_plugin (dsc_plugin_key);

CREATE INDEX d_tech_contact_dsc_tech_contact_email_idx ON atlassian.d_tech_contact (dsc_tech_contact_email);

CREATE INDEX d_server_license_ref_server_license_idx ON atlassian.d_server_license (ref_server_license);

CREATE INDEX f_sales_ref_license_ref_invoice_idx ON atlassian.f_sales (ref_license, ref_invoice);

CREATE INDEX d_country_dsc_country_name_idx ON atlassian.d_country (dsc_country_name);

