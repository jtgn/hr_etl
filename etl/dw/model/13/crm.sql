CREATE INDEX d_account_ref_account_idx ON crm.d_account(ref_account);

CREATE INDEX d_campaign_ref_campaign_idx ON crm.d_campaign(ref_campaign);

CREATE INDEX d_lead_source_dsc_lead_source_md5_idx ON crm.d_lead_source(dsc_lead_source, md5(dsc_lead_source_details));

CREATE INDEX d_line_of_business_dsc_line_of_business_code_idx ON crm.d_line_of_business(dsc_line_of_business_code);

CREATE INDEX d_lead_account_dsc_account_name_dsc_account_description_idx ON crm.d_lead_account(dsc_account_name, dsc_account_description);

CREATE INDEX d_status_dsc_status_idx ON crm.d_status(dsc_status);

CREATE INDEX d_lead_contact_dsc_contact_name_dsc_contact_title_idx ON crm.d_lead_contact(dsc_contact_name, dsc_contact_title);

CREATE INDEX d_lead_attendant_dsc_attendant_status_dsc_industry_dsc_tax__idx ON crm.d_lead_attendant(dsc_attendant_status, dsc_industry, dsc_tax_code, dsc_language, flg_is_competitor);

CREATE INDEX d_contact_ref_contact_idx ON crm.d_contact(ref_contact);

CREATE INDEX d_project_type_dsc_project_type_idx ON crm.d_project_type(dsc_project_type);

CREATE INDEX d_opportunity_dsc_opportunity_name_dsc_opportunity_type_idx ON crm.d_opportunity(dsc_opportunity_name, dsc_opportunity_type);

CREATE INDEX d_stage_dsc_stage_idx ON crm.d_stage(dsc_stage);

CREATE INDEX d_business_partner_ref_business_partner_idx ON crm.d_business_partner(ref_business_partner);

CREATE INDEX f_opportunities_ref_opportunity_idx ON crm.f_opportunities(ref_opportunity);

CREATE INDEX f_leads_ref_lead_idx ON crm.f_leads(ref_lead);

