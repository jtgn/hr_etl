-- SPRENPLAN ONLY
CREATE VIEW erp.f_indirect_allocations AS
(WITH timesheets AS (
	SELECT
		timesheets.f_timesheets.id_project AS new_id_project,
		(timesheets.f_timesheets.id_analytical_date / 100) AS year_month, 
		SUM(timesheets.f_timesheets.nm_total_time) AS time
	FROM
		timesheets.f_timesheets
		INNER JOIN erp.d_project ON (timesheets.f_timesheets.id_project = erp.d_project.id_project)
	WHERE
		erp.d_project.dsc_line_of_business NOT IN ('Interna')
	GROUP BY
		timesheets.f_timesheets.id_project,
		(timesheets.f_timesheets.id_analytical_date / 100)
	HAVING
		SUM(timesheets.f_timesheets.nm_total_time) > 0
)
SELECT
	erp.f_allocations.*,
	timesheets.new_id_project,
	(CASE 
		WHEN 
			(SELECT SUM(timesheets_total.time) FROM timesheets AS timesheets_total WHERE timesheets_total.year_month = timesheets.year_month) = 0
		THEN 
			0
		ELSE 
			timesheets.time / (SELECT SUM(timesheets_total.time) FROM timesheets AS timesheets_total WHERE timesheets_total.year_month = timesheets.year_month) 
		END
	* erp.f_allocations.nm_cost) AS new_nm_cost
FROM
	erp.f_allocations
	INNER JOIN erp.d_project ON (erp.f_allocations.id_project = erp.d_project.id_project)
	INNER JOIN erp.d_product ON (erp.f_allocations.id_product = erp.d_product.id_product)
	INNER JOIN timesheets ON ((erp.f_allocations.id_analytical_date / 100) = timesheets.year_month)
WHERE
	erp.f_allocations.nm_cost <> 0
	AND erp.d_project.dsc_line_of_business IN ('Interna')
	AND erp.d_product.dsc_subcategory NOT IN ('Impostos','Ativos - Imobilizado')
	AND erp.d_product.dsc_product NOT IN ('Renda mensal contrato de leasing','Seguro Multiplic +','Seguro Saúde','Sobretaxa em sede de IRS','Utilização viatura uso pessoal',
	'Seguro Auto - Volkswagens','Seguro Auto - Peugeot','IUC - Volkswagen','IUC - Peugeot','Automóveis - Não Dedutível','Combustível - Alocado','Portagens - Alocado')
) UNION (
SELECT
	erp.f_allocations.*,
	erp.f_allocations.id_project AS new_id_project,
	erp.f_allocations.nm_cost AS new_nm_cost
FROM
	erp.f_allocations
	INNER JOIN erp.d_project ON (erp.f_allocations.id_project = erp.d_project.id_project)
	INNER JOIN erp.d_product ON (erp.f_allocations.id_product = erp.d_product.id_product)
WHERE
	NOT (
		erp.f_allocations.nm_cost <> 0
		AND erp.d_project.dsc_line_of_business IN ('Interna')
		AND erp.d_product.dsc_subcategory NOT IN ('Impostos','Ativos - Imobilizado')
		AND erp.d_product.dsc_product NOT IN ('Renda mensal contrato de leasing','Seguro Multiplic +','Seguro Saúde','Sobretaxa em sede de IRS','Utilização viatura uso pessoal',
		'Seguro Auto - Volkswagens','Seguro Auto - Peugeot','IUC - Volkswagen','IUC - Peugeot','Automóveis - Não Dedutível','Combustível - Alocado','Portagens - Alocado')
	)
)
