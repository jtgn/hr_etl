CREATE SCHEMA xpagile_addons;

CREATE TABLE xpagile_addons.d_profile
(
  id_profile serial NOT NULL,
  version integer DEFAULT 1,
  date_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  date_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
  ref_profile character varying(32) DEFAULT '-1'::character varying,
  dsc_name character varying(255) DEFAULT 'NA'::character varying,
  dt_start_date date DEFAULT '1900-01-01'::date,
  dt_end_date date DEFAULT '1900-01-01'::date,
  nm_units_cost numeric(20,8) DEFAULT 0,
  CONSTRAINT d_profile_pkey PRIMARY KEY (id_profile)
);

INSERT INTO xpagile_addons.d_profile (id_profile) VALUES (0);

CREATE TABLE xpagile_addons.d_project_version
(
  id_project_version serial NOT NULL,
  version integer DEFAULT 1,
  date_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  date_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
  ref_project character varying(32) DEFAULT '-1'::character varying,
  dsc_project character varying(255) DEFAULT 'NA'::character varying,
  ref_release character varying(32) DEFAULT '-1'::character varying,
  dsc_release character varying(255) DEFAULT 'NA'::character varying,
  dt_release_end_date date DEFAULT '1900-01-01'::date,
  ref_sprint character varying(32) DEFAULT '-1'::character varying,
  dsc_sprint character varying(255) DEFAULT 'NA'::character varying,
  dt_sprint_end_date date DEFAULT '1900-01-01'::date,
  nm_current_sprint integer DEFAULT (-1),
  nm_number_of_sprints integer DEFAULT (-1),
  flg_is_backlog character(1) DEFAULT 'N'::bpchar,
  CONSTRAINT d_project_version_pkey PRIMARY KEY (id_project_version)
);

INSERT INTO xpagile_addons.d_project_version (id_project_version) VALUES (0);

CREATE TABLE xpagile_addons.f_xpagile
(
  id_f_xpagile serial NOT NULL,
  id_analytical_date integer NOT NULL,
  id_project integer NOT NULL,
  id_project_version integer NOT NULL,
  id_profile integer NOT NULL,
  nm_bac numeric(20,8) NOT NULL,
  nm_bac_aggregate_project numeric(20,8) NOT NULL,
  nm_ac numeric(20,8) NOT NULL,
  nm_ac_cumulative_sprint numeric(20,8) NOT NULL,
  nm_spc numeric(20,8) NOT NULL,
  nm_spc_cumulative_sprint numeric(20,8) NOT NULL,
  nm_prsp numeric(20,8) NOT NULL,
  nm_psp numeric(20,8) NOT NULL,
  nm_current_sprint integer,
  nm_number_of_sprints integer,
  id_resource integer DEFAULT 0,
  nm_day_ac numeric(20,8) DEFAULT 0,
  nm_day_bac numeric(20,8) DEFAULT 0,
  nm_day_bac_aggregate_project numeric(20,8) DEFAULT 0,
  nm_bugs integer NOT NULL,
  nm_sub_tasks integer NOT NULL,
  CONSTRAINT f_xpagile_pkey PRIMARY KEY (id_f_xpagile)
);
