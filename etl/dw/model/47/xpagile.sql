CREATE VIEW xpagile.d_profile_v AS (
SELECT
	*,
	'JIRA_' || id_profile AS id_profile_v
FROM
	xpagile.d_profile
UNION
SELECT
	*,
	'JIRA_ADDONS_' || id_profile AS id_profile_v
FROM
	xpagile_addons.d_profile
);

CREATE VIEW xpagile.d_project_version_v AS (
SELECT
	*,
	'JIRA_' || id_project_version AS id_project_version_v
FROM
	xpagile.d_project_version
UNION
SELECT
	*,
	'JIRA_ADDONS_' || id_project_version AS id_project_version_v
FROM
	xpagile_addons.d_project_version
);

CREATE VIEW xpagile.f_xpagile_v AS (
SELECT
	*,
	'JIRA_' || id_project_version AS id_project_version_v,
	'JIRA_' || id_profile AS id_profile_v
FROM
	xpagile.f_xpagile
UNION
SELECT
	*,
	'JIRA_ADDONS_' || id_project_version AS id_project_version_v,
	'JIRA_ADDONS_' || id_profile AS id_profile_v
FROM
	xpagile_addons.f_xpagile
);

CREATE TABLE xpagile.f_governance (
	dsc_issue_type VARCHAR(255),
	ref_issue VARCHAR(255),
	dsc_summary VARCHAR(255),
	dsc_environment VARCHAR(255),
	dsc_resolution VARCHAR(255),
	dsc_issue_status VARCHAR(255),
	id_reporter INTEGER,
	id_assignee INTEGER,
	id_creation_date INTEGER,
	id_due_date INTEGER,
	id_resolution_date INTEGER
);
