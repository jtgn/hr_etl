CREATE TABLE sa.f_employee_skill (
	ref_resource VARCHAR(32) NOT NULL,
	ref_skill VARCHAR(32) NOT NULL,
	dsc_status VARCHAR(255) DEFAULT 'NA',
	dsc_knowledge_level VARCHAR(255) DEFAULT 'NA',
	dsc_experience_level VARCHAR(255) DEFAULT 'NA'
);

CREATE TABLE sa.f_employee_skill_backup (
	ref_resource VARCHAR(32) NOT NULL,
	ref_skill VARCHAR(32) NOT NULL,
	dsc_status VARCHAR(255) DEFAULT 'NA',
	dsc_knowledge_level VARCHAR(255) DEFAULT 'NA',
	dsc_experience_level VARCHAR(255) DEFAULT 'NA'
);
