
CREATE TABLE cp.d_skill (
	id_skill SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_skill VARCHAR(32) DEFAULT '-1',
	dsc_skill VARCHAR(255) DEFAULT 'NA',
	dsc_skill_group VARCHAR(255) DEFAULT 'NA',
	dsc_skill_area VARCHAR(255) DEFAULT 'NA',
	dsc_parent_group VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO cp.d_skill (id_skill) VALUES (0);

CREATE TABLE cp.f_employee_skill (
	id_resource INTEGER NOT NULL,
	id_skill INTEGER NOT NULL,
	dsc_status VARCHAR(255) DEFAULT 'NA',
	dsc_knowledge_level VARCHAR(255) DEFAULT 'NA',
	dsc_experience_level VARCHAR(255) DEFAULT 'NA'
);
