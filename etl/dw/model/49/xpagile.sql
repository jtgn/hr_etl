
TRUNCATE xpagile.f_xpagile;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_risks INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_blockeds INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_closed_stories_without_story_points INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_stories_without_description INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_tasks_without_estimate INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_unscheduled_version INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_duplicates_closed_without_link INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_removed_user_stories_resolved_wont_fix INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_closed_without_worklog INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_closed_stories_without_tests INTEGER NOT NULL;

ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_closed_stories_without_tests_executions INTEGER NOT NULL;

DROP VIEW xpagile.f_xpagile_v;
CREATE VIEW xpagile.f_xpagile_v AS (
SELECT
	*,
	'JIRA_' || id_project_version AS id_project_version_v,
	'JIRA_' || id_profile AS id_profile_v
FROM
	xpagile.f_xpagile
UNION
SELECT
	*,
	'JIRA_ADDONS_' || id_project_version AS id_project_version_v,
	'JIRA_ADDONS_' || id_profile AS id_profile_v
FROM
	xpagile_addons.f_xpagile
);
