
TRUNCATE xpagile_addons.f_xpagile;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_risks INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_blockeds INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_closed_stories_without_story_points INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_stories_without_description INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_tasks_without_estimate INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_unscheduled_version INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_duplicates_closed_without_link INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_removed_user_stories_resolved_wont_fix INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_closed_without_worklog INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_closed_stories_without_tests INTEGER NOT NULL;

ALTER TABLE xpagile_addons.f_xpagile ADD COLUMN nm_closed_stories_without_tests_executions INTEGER NOT NULL;

