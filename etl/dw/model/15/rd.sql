CREATE TABLE rd.f_qualifications (
	ref_resource VARCHAR(32) NOT NULL,
	ts_qualification TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_qualifications_backup (
	ref_resource VARCHAR(32) NOT NULL,
	ts_qualification TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_benefits (
	ref_resource VARCHAR(32) NOT NULL,
	ts_benefit TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_benefits_backup (
	ref_resource VARCHAR(32) NOT NULL,
	ts_benefit TIMESTAMP NOT NULL,
	ts_resource TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);
