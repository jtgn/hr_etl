CREATE TABLE sa.f_qualifications (
	ref_qualification VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL
);

CREATE TABLE sa.f_benefits (
	ref_benefit VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	ref_enterprise VARCHAR(32) NOT NULL
);
