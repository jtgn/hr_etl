CREATE SCHEMA socialcontent;

CREATE TABLE socialcontent.f_social
(
  ref_id character varying(100), -- Twitter page, tweet, facebook page/post ID
  nm_date_created integer, --Creation date Ex 20160403
  nm_date_updated integer, --Update date Ex 20160404
  dsc_hour_created character varying(10),--Creation hour  Ex 16:04:32
  dsc_hour_updated character varying(10),--Update hour Ex 17:04:32
  dsc_content_type character varying(10),-- Post/page/tweet
  dsc_social_media character varying(10),--Facebook/Tweeter
  dsc_description text, -- page name, post/tweet text 
  dsc_main_hashtag text, -- main hashtag
  dsc_other_hashtag text, -- other hashtag
  dsc_shorturls text, -- short urls
  nm_fans integer, -- number of fans
  nm_followers integer, -- number of followers
  nm_number_of_posts integer, --number of posts 
  nm_number_of_views integer, --number of views
  nm_impressions_organic integer, -- number of non-paid impressions
  nm_impressions_paid integer,-- number of paid impressions
  nm_fan_impressions integer, --number of fan impressions
  nm_fan_impressions_paid integer, --number of fan paid impressions
  nm_negative_feedback integer, --number of negative feedback
  nm_engaged_fans integer, --number of engaged fans
  nm_engaged_users integer,  --number of engaged users
  nm_fan_reach integer,  --number of fan reach
  nm_likes integer,  --number of likes
  nm_other_reactions integer,  --number of other reactions
  nm_shares integer,  --number of shares
  nm_comments integer,  --number of comments
  PRIMARY KEY (ref_id,nm_date_updated)
);

CREATE TABLE socialcontent.f_url
(
  ref_short_url character varying(100), --short url
  nm_date_created integer, --Creation date Ex 20160403
  nm_date_updated integer, --Update date Ex 20160404
  dsc_hour_created character varying(10),--Creation hour  Ex 16:04:32
  dsc_hour_updated character varying(10),--Update hour Ex 17:04:32
  dsc_original_link text,--Original url
  nm_total_clicks integer,--Total clicks
  nm_facebook_clicks integer,--Referrers: Facebook clicks
  nm_linkdin_clicks integer,--Referrers: Linkdin clicks
  nm_twitter_clicks integer,--Referrers: Twitter clicks
  nm_google_clicks integer,--Referrers: Google clicks
  nm_unknown_ref_clicks integer,--Referrers: Unknown clicks
  nm_portugal_clicks integer,--Country: Portugal clicks
  nm_united_kingdom_clicks integer, --Country: UK clicks
  nm_other_country_clicks integer, --Country: unknown clicks
  PRIMARY KEY (ref_short_url,nm_date_updated)
);

CREATE TABLE socialcontent.f_url_social
(
  ref_id character varying(100), -- Twitter page, tweet, facebook page/post ID
  ref_short_url character varying(100), --short url
  PRIMARY KEY (ref_id,ref_short_url)
);

CREATE TABLE socialcontent.f_social_backup
(
  ref_id character varying(100), -- Twitter page, tweet, facebook page/post ID
  nm_date_created integer, --Creation date Ex 20160403
  nm_date_updated integer, --Update date Ex 20160404
  dsc_hour_created character varying(10),--Creation hour  Ex 16:04:32
  dsc_hour_updated character varying(10),--Update hour Ex 17:04:32
  dsc_content_type character varying(10),-- Post/page/tweet
  dsc_social_media character varying(10),--Facebook/Tweeter
  dsc_description text, -- page name, post/tweet text 
  dsc_main_hashtag text, -- main hashtag
  dsc_other_hashtag text, -- other hashtag
  dsc_shorturls text, -- short urls
  nm_fans integer, -- number of fans
  nm_followers integer, -- number of followers
  nm_number_of_posts integer, --number of posts 
  nm_number_of_views integer, --number of views
  nm_impressions_organic integer, -- number of non-paid impressions
  nm_impressions_paid integer,-- number of paid impressions
  nm_fan_impressions integer, --number of fan impressions
  nm_fan_impressions_paid integer, --number of fan paid impressions
  nm_negative_feedback integer, --number of negative feedback
  nm_engaged_fans integer, --number of engaged fans
  nm_engaged_users integer,  --number of engaged users
  nm_fan_reach integer,  --number of fan reach
  nm_likes integer,  --number of likes
  nm_other_reactions integer,  --number of other reactions
  nm_shares integer,  --number of shares
  nm_comments integer,  --number of comments
  PRIMARY KEY (ref_id,nm_date_updated)
);

CREATE TABLE socialcontent.f_url_backup
(
  ref_short_url character varying(100), --short url
  nm_date_created integer, --Creation date Ex 20160403
  nm_date_updated integer, --Update date Ex 20160404
  dsc_hour_created character varying(10),--Creation hour  Ex 16:04:32
  dsc_hour_updated character varying(10),--Update hour Ex 17:04:32
  dsc_original_link text,--Original url
  nm_total_clicks integer,--Total clicks
  nm_facebook_clicks integer,--Referrers: Facebook clicks
  nm_linkdin_clicks integer,--Referrers: Linkdin clicks
  nm_twitter_clicks integer,--Referrers: Twitter clicks
  nm_google_clicks integer,--Referrers: Google clicks
  nm_unknown_ref_clicks integer,--Referrers: Unknown clicks
  nm_portugal_clicks integer,--Country: Portugal clicks
  nm_united_kingdom_clicks integer, --Country: UK clicks
  nm_other_country_clicks integer, --Country: unknown clicks
  PRIMARY KEY (ref_short_url,nm_date_updated)
);

CREATE TABLE socialcontent.f_url_social_backup
(
  ref_id character varying(100), -- Twitter page, tweet, facebook page/post ID
  ref_short_url character varying(100), --short url
  PRIMARY KEY (ref_id,ref_short_url)
);