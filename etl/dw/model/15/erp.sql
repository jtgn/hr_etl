ALTER TABLE erp.d_project ADD COLUMN dsc_sales_type VARCHAR(255) DEFAULT 'NA';

CREATE TABLE erp.f_qualifications (
	id_f_qualifications SERIAL NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	id_resource INTEGER NOT NULL,
  	ref_resource VARCHAR(32) NOT NULL,
	id_academic_qualification INTEGER NOT NULL,
	id_qualification INTEGER NOT NULL,
	id_enterprise INTEGER NOT NULL
);


CREATE TABLE erp.f_benefits (
	id_f_benefits SERIAL NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	id_enterprise INTEGER NOT NULL,
	id_benefit INTEGER NOT NULL,
	id_resource INTEGER NOT NULL,
	nm_amount NUMERIC(20,8) NOT NULL,
	nm_supported_amount NUMERIC(20,8) NOT NULL
);
