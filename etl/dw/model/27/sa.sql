
ALTER TABLE sa.f_indirect_bu_allocations_investment_weights ADD COLUMN nm_cds_value numeric(20,8) DEFAULT 0;

INSERT INTO sa.f_indirect_bu_allocations_investment_weights VALUES ('*',2017,0.2,0.2,0.2,0.2,0.2);

INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Service',2017,1.0);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Resell',2017,0.4);
INSERT INTO sa.f_indirect_bu_allocations_product_weights VALUES ('Product',2017,1.0);

CREATE TABLE sa.f_atlassian_reviews (
	dsc_plugin_key VARCHAR(255) NOT NULL,
	dsc_author VARCHAR(255) NOT NULL,
	dsc_review TEXT NOT NULL,
	dsc_response TEXT NOT NULL,
	nm_stars INTEGER NOT NULL,
	dt_date DATE NOT NULL,
	nm_total_votes INTEGER NOT NULL,
	nm_helpful_votes INTEGER NOT NULL
);

CREATE TABLE sa.f_atlassian_recommendations (
	dsc_plugin_key VARCHAR(255) NOT NULL,
	nm_average_stars NUMERIC(20,8) NOT NULL,
	nm_count INTEGER NOT NULL,
	dsc_other_plugin_key VARCHAR(255) NOT NULL,
	dsc_other_plugin_name VARCHAR(255) NOT NULL
);

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_date TEXT DEFAULT 'NA';
ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_type TEXT DEFAULT 'NA';
ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_reason TEXT DEFAULT 'NA';
ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_message TEXT DEFAULT 'NA';
ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_author_email TEXT DEFAULT 'NA';
ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_feedback_author TEXT DEFAULT 'NA';
