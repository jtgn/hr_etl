
CREATE TABLE atlassian.f_reviews (
	id_plugin INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	id_tech_contact INTEGER NOT NULL,
	dsc_author VARCHAR(255) NOT NULL,
	dsc_review TEXT NOT NULL,
	dsc_response TEXT NOT NULL,
	nm_stars INTEGER NOT NULL,
	nm_total_votes INTEGER NOT NULL,
	nm_helpful_votes INTEGER NOT NULL
);

CREATE TABLE atlassian.f_recommendations (
	id_plugin INTEGER NOT NULL,
	nm_average_stars NUMERIC(20,8) NOT NULL,
	nm_count INTEGER NOT NULL,
	dsc_other_plugin_key VARCHAR(255) NOT NULL,
	dsc_other_plugin_name VARCHAR(255) NOT NULL
);

CREATE TABLE atlassian.d_feedback (
	id_feedback SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_feedback_date TEXT DEFAULT 'NA',
	dsc_feedback_type TEXT DEFAULT 'NA',
	dsc_feedback_reason TEXT DEFAULT 'NA',
	dsc_feedback_message TEXT DEFAULT 'NA',
	dsc_feedback_author_email TEXT DEFAULT 'NA',
	dsc_feedback_author TEXT DEFAULT 'NA',
	ref_license VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_feedback (id_feedback) VALUES (0);

ALTER TABLE atlassian.f_sales ADD COLUMN id_feedback INTEGER DEFAULT 0;
