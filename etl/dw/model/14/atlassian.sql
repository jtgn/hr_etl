ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_domain VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_tech_contact ADD COLUMN dsc_domain VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_tech_contact ADD COLUMN dsc_customer_code VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.f_sales ADD COLUMN flg_is_conversion CHAR(1) DEFAULT 'N';

ALTER TABLE atlassian.f_sales ADD COLUMN dt_conversion_date DATE;

ALTER TABLE atlassian.f_sales ADD COLUMN flg_is_dropout CHAR(1) DEFAULT 'N';
