
CREATE TABLE atlassian.d_project (
	id_project SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_project VARCHAR(255) DEFAULT 'NA',
	dsc_project_key VARCHAR(255) DEFAULT '-1'
);

INSERT INTO atlassian.d_project (id_project) VALUES (0);

CREATE TABLE atlassian.d_jira_user (
	id_jira_user SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_user VARCHAR(255) DEFAULT 'NA',
	dsc_email VARCHAR(255) DEFAULT '-1'
);

INSERT INTO atlassian.d_jira_user (id_jira_user) VALUES (0);

CREATE TABLE atlassian.f_addons_service_desk (
	dsc_issue_key VARCHAR(255) NOT NULL,
	dsc_status VARCHAR(255) NOT NULL,
	dsc_sen VARCHAR(255) NOT NULL,
	ts_creation_date TIMESTAMP NOT NULL,
	ts_resolution_date TIMESTAMP NOT NULL,
	dsc_priority VARCHAR(255) NOT NULL,
	dsc_description TEXT NOT NULL,
	id_project INTEGER NOT NULL,
	id_reporter INTEGER NOT NULL,
	id_assignee INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL
);
