
ALTER TABLE erp.d_account ADD COLUMN dsc_taxonomy TEXT DEFAULT 'NA';

ALTER TABLE erp.d_account ADD COLUMN dsc_taxonomy_code TEXT DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_referral_id VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_referral_type VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN dsc_referenced_by VARCHAR(255) DEFAULT 'NA';
