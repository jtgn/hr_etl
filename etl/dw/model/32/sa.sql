
CREATE TABLE sa.f_addons_service_desk (
	dsc_project_key VARCHAR(255) NOT NULL,
	dsc_project VARCHAR(255) NOT NULL,
	dsc_issue_key VARCHAR(255) NOT NULL,
	dsc_status VARCHAR(255) NOT NULL,
	dsc_reporter_email VARCHAR(255) NOT NULL,
	dsc_reporter VARCHAR(255) NOT NULL,
	dsc_assignee_email VARCHAR(255) NOT NULL,
	dsc_assignee VARCHAR(255) NOT NULL,
	dsc_sen VARCHAR(255) NOT NULL,
	ts_creation_date TIMESTAMP NOT NULL,
	ts_resolution_date TIMESTAMP NOT NULL,
	dsc_priority VARCHAR(255) NOT NULL,
	dsc_description TEXT NOT NULL
);

CREATE INDEX sa_f_addons_service_desk_key ON sa.f_addons_service_desk(dsc_issue_key);
