CREATE INDEX atlassian_f_sales_nm_vendor_idx ON atlassian.f_sales(nm_vendor);

CREATE INDEX atlassian_f_sales_id_licanse_type_idx ON atlassian.f_sales(id_licanse_type);

CREATE INDEX atlassian_f_sales_id_tech_contact_idx ON atlassian.f_sales(id_tech_contact);

CREATE INDEX atlassian_f_sales_id_plugin_idx ON atlassian.f_sales(id_plugin);

CREATE INDEX atlassian_f_sales_id_acquisition_date_idx ON atlassian.f_sales(id_acquisition_date);
