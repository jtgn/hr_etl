DROP TABLE prospects.f_prospects;

CREATE TABLE prospects.d_proposal (
	id_proposal SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_proposal VARCHAR(255) DEFAULT '-1',
	dsc_summary VARCHAR(255) DEFAULT 'NA',
	dsc_business_unit VARCHAR(255) DEFAULT 'NA',
	dsc_role VARCHAR(255) DEFAULT 'NA',
	dsc_experience_level VARCHAR(255) DEFAULT 'NA',
	dsc_opportunity_description TEXT DEFAULT 'NA',
	dsc_detailed_description TEXT DEFAULT 'NA',
	dsc_project_title VARCHAR(255) DEFAULT 'NA',
	dsc_project_location VARCHAR(255) DEFAULT 'NA',
	flg_is_remote_work_possible CHAR(1) DEFAULT 'N',
	dsc_deadline VARCHAR(255) DEFAULT 'NA',
	nm_min_budget INTEGER DEFAULT 0,
	nm_max_budget INTEGER DEFAULT 0,
	dsc_job_publication_id VARCHAR(255) DEFAULT 'NA',
	dsc_owner VARCHAR(255) DEFAULT 'NA',
	nm_number_of_opportunities INTEGER DEFAULT 0,
	dt_creation_date DATE DEFAULT '2000-01-01',
	dt_resolution_date DATE DEFAULT '2999-12-31',
	dsc_status VARCHAR(255) DEFAULT 'NA',
	dsc_description TEXT DEFAULT 'NA'
);

INSERT INTO prospects.d_proposal (id_proposal) VALUES (0);

CREATE TABLE prospects.d_prospect (
	id_prospect SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_area VARCHAR(255) DEFAULT 'NA',
	dt_available_at DATE DEFAULT '2999-12-31',
	dsc_comment  VARCHAR(255) DEFAULT 'NA',
	dt_application_date DATE DEFAULT '2000-01-01',
	dt_birth_date DATE DEFAULT '1900-01-01',
	dt_contact_date DATE DEFAULT '2999-12-31',
	dsc_description TEXT DEFAULT 'NA',
	dsc_education VARCHAR(255) DEFAULT 'NA',
	nm_expected_gross_income INTEGER DEFAULT 0,
	nm_expected_net_income INTEGER DEFAULT 0,
	dsc_full_name VARCHAR(255) DEFAULT 'NA',
	dsc_income_currency VARCHAR(255) DEFAULT 'NA',
	flg_is_for_recruitment_initiative CHAR(1) DEFAULT 'N',
	nm_years_of_career INTEGER DEFAULT 0,
	dsc_owner VARCHAR(255) DEFAULT 'NA',
	nm_proposed_gross_income INTEGER DEFAULT 0,
	nm_proposed_net_income INTEGER DEFAULT 0,
	dsc_qualification_level VARCHAR(255) DEFAULT 'NA',
	dsc_reason_for_no_interest VARCHAR(255) DEFAULT 'NA',
	dsc_reason_for_refusal VARCHAR(255) DEFAULT 'NA',
	dsc_reference_type VARCHAR(255) DEFAULT 'NA',
	dsc_referenced_by VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '2999-12-31',
	dsc_summary VARCHAR(255) DEFAULT 'NA',
	dsc_which_initiative VARCHAR(255) DEFAULT 'NA',
	dt_creation_date DATE DEFAULT '2000-01-01',
	dt_resolution_date DATE DEFAULT '2999-12-31',
	dsc_status VARCHAR(255) DEFAULT 'NA',
	ref_prospect VARCHAR(255) DEFAULT '-1'
);

INSERT INTO prospects.d_prospect (id_prospect) VALUES (0);

CREATE TABLE prospects.f_prospects (
	ref_prospect VARCHAR(255) NOT NULL,
	ref_status VARCHAR(255) NOT NULL,
	id_prospect INTEGER NOT NULL,
	id_proposal INTEGER NOT NULL,
	dsc_analysed_status VARCHAR(255) NOT NULL,
	nm_time_in_status BIGINT NOT NULL
);
