TRUNCATE rd.f_atlassian_sales;

ALTER TABLE rd.f_atlassian_sales ADD COLUMN dsc_plugin VARCHAR(255) NOT NULL;

TRUNCATE rd.f_atlassian_sales_backup;

ALTER TABLE rd.f_atlassian_sales_backup ADD COLUMN dsc_plugin VARCHAR(255) NOT NULL;
