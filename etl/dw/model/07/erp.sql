
ALTER TABLE erp.d_resource ADD COLUMN dsc_resource_working_office VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_resource ADD COLUMN nm_number_of_documents INTEGER DEFAULT 0;

ALTER TABLE erp.d_contract ADD COLUMN nm_number_of_attachments INTEGER DEFAULT 0;
