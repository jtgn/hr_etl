
CREATE TABLE timesheets.f_vacations (
	id_f_vacations SERIAL PRIMARY KEY,		
	id_resource INTEGER NOT NULL, -- FK d_resource
	id_analytical_date INTEGER NOT NULL, -- FK d_date
	dsc_vacation_type VARCHAR(255) DEFAULT 'NA'
);

CREATE VIEW timesheets.f_vacation_timesheets AS 
SELECT
	v.id_f_vacations,
	v.id_resource,
	v.id_analytical_date,
	v.dsc_vacation_type,
	COALESCE(t.id_f_timesheets,'-1') AS id_f_timesheets,
	COALESCE(t.id_activity,0) AS id_activity,
	COALESCE(t.dsc_status,'NA') AS dsc_status
FROM
	timesheets.f_vacations v
	LEFT JOIN timesheets.f_timesheets t ON (v.id_analytical_date = t.id_analytical_date AND v.id_resource = t.id_employee)
ORDER BY
	v.id_analytical_date ASC,
	v.id_resource ASC
