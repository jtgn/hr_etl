
ALTER TABLE erp.d_project ADD COLUMN dsc_gross_margin_status VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_project ADD COLUMN dsc_bu_margin_status VARCHAR(255) DEFAULT 'NA';

ALTER TABLE erp.d_project ADD COLUMN dsc_margin_status VARCHAR(255) DEFAULT 'NA';
