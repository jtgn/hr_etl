CREATE SCHEMA prospects;

CREATE TABLE prospects.f_prospects (
	ref_issue VARCHAR(255) NOT NULL,
	id_reporter INTEGER NOT NULL,
	id_assignee INTEGER NOT NULL,
	dsc_summary VARCHAR(255) NOT NULL,
	dsc_priority VARCHAR(255) NOT NULL,
	dsc_status VARCHAR(255) NOT NULL,
	id_created_date INTEGER NOT NULL,
	id_due_date INTEGER NOT NULL,
	id_resolution_date INTEGER NOT NULL,
	nm_auto_evaluation INTEGER NOT NULL,
	id_available_at INTEGER NOT NULL,
	dsc_business_unit VARCHAR(255) NOT NULL,
	nm_critical_tough INTEGER NOT NULL,
	nm_expected_gross_income VARCHAR(255) NOT NULL,
	nm_expected_net_income VARCHAR(255) NOT NULL,
	nm_formal_presentation INTEGER NOT NULL,
	dsc_full_name VARCHAR(255) NOT NULL,
	nm_interpersonal_communication INTEGER NOT NULL,
	dsc_language_english VARCHAR(255) NOT NULL,
	dsc_language_french VARCHAR(255) NOT NULL,
	dsc_language_german VARCHAR(255) NOT NULL,
	dsc_language_other VARCHAR(255) NOT NULL,
	dsc_location VARCHAR(255) NOT NULL,
	nm_oral_presentation_speech INTEGER NOT NULL,
	dsc_other_language VARCHAR(255) NOT NULL,
	dsc_profile VARCHAR(255) NOT NULL,
	nm_proposed_gross_income VARCHAR(255) NOT NULL,
	dsc_reason_for_no_interest VARCHAR(255) NOT NULL,
	dsc_reason_for_refusal VARCHAR(255) NOT NULL,
	id_referenced_by INTEGER NOT NULL,
	nm_responsibility_autonomy INTEGER NOT NULL,
	id_start_date INTEGER NOT NULL,
	nm_technical_background INTEGER NOT NULL,
	dsc_time_to_entry VARCHAR(255) NOT NULL,
	nm_career_entry_year INTEGER NOT NULL,
	nm_year_of_birth INTEGER NOT NULL,
	dsc_analysed_status VARCHAR(255) NOT NULL,
	nm_time_in_status INTEGER NOT NULL
);
