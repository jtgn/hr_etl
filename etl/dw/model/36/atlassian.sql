
CREATE TABLE atlassian.f_addons_versions (
	id_plugin INTEGER NOT NULL,
	id_analytical_date INTEGER NOT NULL,
	dsc_name VARCHAR(255) NOT NULL,
	flg_cloud CHAR(1) NOT NULL,
	flg_server CHAR(1) NOT NULL 
);
