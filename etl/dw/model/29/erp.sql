
CREATE INDEX f_asset_history_id_asset_idx ON erp.f_asset_history(id_asset);

CREATE INDEX f_asset_history_id_date_idx ON erp.f_asset_history(id_analytical_date);
