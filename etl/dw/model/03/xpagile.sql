ALTER TABLE xpagile.f_xpagile ADD COLUMN id_resource INTEGER DEFAULT 0;
ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_day_ac NUMERIC(20,8) DEFAULT 0;
ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_day_bac NUMERIC(20,8) DEFAULT 0;
ALTER TABLE xpagile.f_xpagile ADD COLUMN nm_day_bac_aggregate_project NUMERIC(20,8) DEFAULT 0;
