CREATE SCHEMA sa;

CREATE TABLE sa.f_document_lines (
	-- Primary Key
	ref_document VARCHAR(32) NOT NULL,
	ref_tax_line VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	--
	dt_document_date DATE NOT NULL,
	ref_order VARCHAR(32) NOT NULL,
	ref_product VARCHAR(32) NOT NULL,
	ref_customer VARCHAR(32) NOT NULL,
	ref_vendor VARCHAR(32) NOT NULL,
    	ref_price_list VARCHAR(32) NOT NULL,
	ref_payment_term VARCHAR(32) NOT NULL,
	ref_sales_representative VARCHAR(32) NOT NULL,
	ref_presales_representative VARCHAR(32) NOT NULL,
	ref_lead_representative VARCHAR(32) NOT NULL,
	ref_company_agent VARCHAR(32) NOT NULL,
	ref_tax VARCHAR(32) NOT NULL,
	nm_quantity NUMERIC(20,8) NOT NULL,
	nm_price_list NUMERIC(20,8) NOT NULL,
	nm_price_limit NUMERIC(20,8) NOT NULL,
	nm_net_amount NUMERIC(20,8) NOT NULL,
	nm_total_allocated NUMERIC(20,8) NOT NULL,
	nm_tax_amount NUMERIC(20,8) NOT NULL,
	nm_tax_amount_deductable NUMERIC(20,8) NOT NULL,
	nm_tax_amount_withholding NUMERIC(20,8) NOT NULL,
	nm_taxable_amount NUMERIC(20,8) NOT NULL
);

CREATE INDEX sa_f_document_lines_ref_document_idx ON sa.f_document_lines (ref_document);

CREATE TABLE sa.f_allocations_0 (
	id_f_allocations_0 SERIAL NOT NULL,
	ref_allocation VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_activity VARCHAR(32) NOT NULL,
	ref_product VARCHAR(32) NOT NULL,
	ref_office VARCHAR(32) NOT NULL,
	ref_department VARCHAR(32) NOT NULL,
	ref_project VARCHAR(32) NOT NULL,
	ref_customer VARCHAR(32) NOT NULL,
	ref_vendor VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	ts_start_date TIMESTAMP NOT NULL,
	ts_end_date TIMESTAMP NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8)  NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_allocation_year INTEGER NOT NULL,
	dsc_allocation_status VARCHAR(255) NOT NULL,
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL
);

CREATE INDEX sa_f_allocations_0_ref_document_idx ON sa.f_allocations_0 (ref_document);

CREATE TABLE sa.f_allocations_1 (
	ref_allocation VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_activity VARCHAR(32) NOT NULL,
	ref_product VARCHAR(32) NOT NULL,
	ref_office VARCHAR(32) NOT NULL,
	ref_department VARCHAR(32) NOT NULL,
	ref_project VARCHAR(32) NOT NULL,
	ref_customer VARCHAR(32) NOT NULL,
	ref_vendor VARCHAR(32) NOT NULL,
	ref_resource VARCHAR(32) NOT NULL,
	ts_start_date TIMESTAMP NOT NULL,
	ts_end_date TIMESTAMP NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8)  NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	dt_analytical_date DATE NOT NULL,
	nm_allocation_year INTEGER NOT NULL,
	dsc_allocation_status VARCHAR(255) NOT NULL,
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL
);

CREATE INDEX sa_f_allocations_1_ref_document_line_idx ON sa.f_allocations_1 (ref_document_line);

CREATE TABLE sa.f_timesheets (
	ref_timesheet VARCHAR(32) NOT NULL,
	ts_update_timesheet TIMESTAMP NOT NULL,
	ref_employee VARCHAR(255) NOT NULL,
	ref_project_leader VARCHAR(255) NOT NULL,
	ref_project_manager VARCHAR(255) NOT NULL,
	dt_analytical_date DATE NOT NULL,
	ref_project VARCHAR(255) NOT NULL,
	ref_activity VARCHAR(255) NOT NULL,
	ref_work_type VARCHAR(255) NOT NULL,
	ref_office VARCHAR(255) NOT NULL,
	dsc_description TEXT,
	dsc_status VARCHAR(255) NOT NULL,
	nm_total_time NUMERIC(20,8) NOT NULL
);

CREATE INDEX sa_f_timesheets_ref_timesheet_idx ON sa.f_timesheets (ref_timesheet);

CREATE TABLE sa.f_atlassian_sales (
	dsc_bill_contact_email VARCHAR(255) NOT NULL,
	dsc_bill_contact_name VARCHAR(255) NOT NULL,
	dsc_bill_contact_phone VARCHAR(255) NOT NULL,
	dsc_organization VARCHAR(255) NOT NULL,
	dsc_country_name VARCHAR(255) NOT NULL,
	dsc_expert_name VARCHAR(255) NOT NULL,
	dsc_lincese_type VARCHAR(255) NOT NULL,
	dsc_license_size VARCHAR(255) NOT NULL,
	dsc_sale_type VARCHAR(255) NOT NULL,
	dsc_plugin_key VARCHAR(255) NOT NULL,
	dsc_plugin_name VARCHAR(255) NOT NULL,
	dsc_tech_contact_email VARCHAR(255) NOT NULL,
	dsc_tech_contact_name VARCHAR(255) NOT NULL,
	dsc_tech_contact_phone VARCHAR(255) NOT NULL,
	dsc_tech_contact_address VARCHAR(255) NOT NULL,
	dsc_tech_contact_postal_code VARCHAR(255) NOT NULL,
	dsc_tech_contact_city VARCHAR(255) NOT NULL,
	dsc_tech_contact_state VARCHAR(255) NOT NULL,
	dsc_tech_contact_country VARCHAR(255) NOT NULL,
	ref_license VARCHAR(255) NOT NULL,
	ref_invoice VARCHAR(255) NOT NULL,
	dt_acquisition_date DATE NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_price NUMERIC(20,8) NOT NULL,
	nm_vendor NUMERIC(20,8) NOT NULL,
	flg_has_discount CHAR(1) NOT NULL,
	dsc_renewal_action VARCHAR(255) NOT NULL
);

CREATE INDEX sa_f_atlassian_sales_refs_idx ON sa.f_atlassian_sales(ref_license,ref_invoice);

CREATE TABLE sa.f_xpagile_0 (
	ref_entity VARCHAR(32),
	ref_parent VARCHAR(32),
	ref_son VARCHAR(32)
);

CREATE TABLE sa.f_xpagile_1 (
	ref_xpbu_project_budget VARCHAR(32),
	ref_ad_client VARCHAR(32),
	ref_ad_org VARCHAR(32),
	flg_is_active CHAR(1),
	ts_created TIMESTAMP,
	dsc_created_by VARCHAR(32),
	ts_updated TIMESTAMP,
	dsc_updated_by VARCHAR(32),
	ref_c_project VARCHAR(32),
	ref_xphr_profile VARCHAR(32),
	nm_days NUMERIC(20,8),
	ts_start_date TIMESTAMP,
	dsc_description VARCHAR(255)
);
