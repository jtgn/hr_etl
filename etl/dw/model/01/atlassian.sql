CREATE SCHEMA atlassian;

CREATE TABLE atlassian.d_bill_contact (
	id_bill_contact SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',	
	dsc_bill_contact_email VARCHAR(255) DEFAULT 'NA',
	dsc_bill_contact_name VARCHAR(255) DEFAULT 'NA',
	dsc_bill_contact_phone VARCHAR(255) DEFAULT 'NA',
	dsc_organization VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_bill_contact (id_bill_contact) VALUES (0);

CREATE TABLE atlassian.d_country (
	id_country SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_iso_code_two VARCHAR(2) DEFAULT 'NA',
	dsc_iso_code_three VARCHAR(3) DEFAULT 'NA',
	dsc_country_name VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_country (id_country) VALUES (0);

CREATE TABLE atlassian.d_expert (
	id_expert SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_expert_name VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_expert (id_expert) VALUES (0);

CREATE TABLE atlassian.d_license_type (
	id_license_type SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_lincese_type VARCHAR(255) DEFAULT 'NA',
	dsc_license_size VARCHAR(255) DEFAULT 'NA',
	dsc_sale_type VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_license_type (id_license_type) VALUES (0);

CREATE TABLE atlassian.d_plugin (
	id_plugin SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_plugin_key VARCHAR(255) DEFAULT 'NA',
	dsc_plugin_name VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_plugin (id_plugin) VALUES (0);

CREATE TABLE atlassian.d_tech_contact (
	id_tech_contact SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_tech_contact_email VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_name VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_phone VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_address VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_postal_code VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_city VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_state VARCHAR(255) DEFAULT 'NA',
	dsc_tech_contact_country VARCHAR(255) DEFAULT 'NA',
	dsc_organization VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_tech_contact (id_tech_contact) VALUES (0);

CREATE TABLE atlassian.f_sales (
	id_f_sales SERIAL PRIMARY KEY,
	ref_license VARCHAR(255) NOT NULL,
	ref_invoice VARCHAR(255) NOT NULL,
	id_acquisition_date INTEGER NOT NULL,
	id_start_date INTEGER NOT NULL,
	id_end_date INTEGER NOT NULL,
	id_plugin INTEGER NOT NULL,
	id_country INTEGER NOT NULL,
	id_tech_contact INTEGER NOT NULL,
	id_bill_contact INTEGER NOT NULL,
	id_licanse_type INTEGER NOT NULL,
	id_expert INTEGER NOT NULL,
	nm_price NUMERIC(20,8) NOT NULL,
	nm_vendor NUMERIC(20,8) NOT NULL,
	flg_has_discount CHAR(1) NOT NULL,
	dsc_renewal_action VARCHAR(255) NOT NULL
);
