CREATE SCHEMA rd;

CREATE TABLE rd.f_document_lines (
	ref_document VARCHAR(32) NOT NULL,
	ts_update_document TIMESTAMP NOT NULL,
	ref_tax_line VARCHAR(32) NOT NULL,
	ts_update_tax TIMESTAMP NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ts_update_document_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE INDEX rd_f_document_lines_flagfield_idx ON rd.f_document_lines (flagfield);
CREATE INDEX rd_f_document_lines_refs_idx ON rd.f_document_lines (ref_document,ref_document_line,ref_tax_line);

CREATE TABLE rd.f_document_lines_backup (
	ref_document VARCHAR(32) NOT NULL,
	ts_update_document TIMESTAMP NOT NULL,
	ref_tax_line VARCHAR(32) NOT NULL,
	ts_update_tax TIMESTAMP NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ts_update_document_line TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_allocations (
	ref_allocation VARCHAR(32) NOT NULL,
	ts_update_allocation TIMESTAMP NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,	
	ts_update_line_allocation TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ts_update_document TIMESTAMP NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ts_update_document_line TIMESTAMP NOT NULL
);

CREATE INDEX rd_f_allocations_flagfield_idx ON rd.f_allocations (flagfield);
CREATE INDEX rd_f_allocations_refs_idx ON rd.f_allocations (ref_document,ref_document_line,ref_line_allocation,ref_allocation);

CREATE TABLE rd.f_allocations_backup (
	ref_allocation VARCHAR(32) NOT NULL,
	ts_update_allocation TIMESTAMP NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,	
	ts_update_line_allocation TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ts_update_document TIMESTAMP NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ts_update_document_line TIMESTAMP NOT NULL
);

CREATE TABLE rd.f_timesheets (
	ref_timesheet VARCHAR(32) NOT NULL,
	ts_update_timesheet TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE INDEX rd_f_timesheets_flagfield_idx ON rd.f_timesheets (flagfield);
CREATE INDEX rd_f_timesheets_ref_timesheet_idx ON rd.f_timesheets(ref_timesheet);

CREATE TABLE rd.f_timesheets_backup (
	ref_timesheet VARCHAR(32) NOT NULL,
	ts_update_timesheet TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_atlassian_sales (
	ref_license VARCHAR(255) NOT NULL,
	ref_invoice VARCHAR(255) NOT NULL,
	dt_acquisition_date DATE NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_atlassian_sales_backup (
	ref_license VARCHAR(255) NOT NULL,
	ref_invoice VARCHAR(255) NOT NULL,
	dt_acquisition_date DATE NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

