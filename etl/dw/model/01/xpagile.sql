CREATE SCHEMA xpagile;

CREATE TABLE xpagile.d_profile (
	id_profile SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_profile VARCHAR(32) DEFAULT '-1',
	dsc_name VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE DEFAULT '1900-01-01',
	dt_end_date DATE DEFAULT '1900-01-01',
	nm_units_cost NUMERIC(20,8) DEFAULT 0
);

INSERT INTO xpagile.d_profile (id_profile) VALUES (0);

CREATE TABLE xpagile.d_project_version (
	id_project_version SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_project VARCHAR(32) DEFAULT '-1',
	dsc_project VARCHAR(255) DEFAULT 'NA',
	ref_release VARCHAR(32) DEFAULT '-1',
	dsc_release VARCHAR(255) DEFAULT 'NA',
	dt_release_end_date DATE DEFAULT '1900-01-01',
	ref_sprint VARCHAR(32) DEFAULT '-1',
	dsc_sprint VARCHAR(255) DEFAULT 'NA',
	dt_sprint_end_date DATE DEFAULT '1900-01-01',
	nm_current_sprint INTEGER DEFAULT -1,
	nm_number_of_sprints INTEGER DEFAULT -1,
	flg_is_backlog CHAR(1) DEFAULT 'N'
);

INSERT INTO xpagile.d_project_version(id_project_version) VALUES (0);

CREATE TABLE xpagile.f_xpagile (
	id_f_xpagile SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,
	id_project INTEGER NOT NULL,
	id_project_version INTEGER NOT NULL,
	id_profile INTEGER NOT NULL,
	nm_bac NUMERIC(20,8) NOT NULL,
	nm_bac_aggregate_project NUMERIC(20,8) NOT NULL,
	nm_ac NUMERIC(20,8) NOT NULL,
	nm_ac_cumulative_sprint NUMERIC(20,8) NOT NULL,
	nm_spc NUMERIC(20,8) NOT NULL,
	nm_spc_cumulative_sprint NUMERIC(20,8) NOT NULL,
	nm_prsp NUMERIC(20,8) NOT NULL,
	nm_psp NUMERIC(20,8) NOT NULL,
	nm_current_sprint INTEGER,
	nm_number_of_sprints INTEGER
);

