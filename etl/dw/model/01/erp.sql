CREATE SCHEMA erp;

CREATE TABLE erp.d_activity (
	id_activity SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_activity VARCHAR(32) DEFAULT '-1',
	dsc_activity VARCHAR(255) DEFAULT 'NA',
	ref_subcategory VARCHAR(32) DEFAULT '-1',
	dsc_subcategory VARCHAR(255) DEFAULT 'NA',
	ref_category VARCHAR(32) DEFAULT '-1',
	dsc_category VARCHAR(255) DEFAULT 'NA',
	flg_is_productive CHAR(1) DEFAULT 'N'
);

INSERT INTO erp.d_activity (id_activity) VALUES (0);

CREATE TABLE erp.d_customer (
	id_customer SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_customer VARCHAR(32) DEFAULT '-1',
	dsc_customer VARCHAR(255) DEFAULT 'NA',
	ref_group VARCHAR(32) DEFAULT '-1',
	dsc_group VARCHAR(255) DEFAULT 'NA',
	dsc_country VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_customer (id_customer) VALUES (0);

CREATE TABLE erp.d_department (
	id_department SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_department VARCHAR(32) DEFAULT '-1',
	dsc_department VARCHAR(255) DEFAULT 'NA',
	dsc_department_description TEXT DEFAULT 'NA',
	ref_division VARCHAR(32) DEFAULT '-1',
	dsc_division VARCHAR(255) DEFAULT 'NA',
	dsc_division_description TEXT DEFAULT 'NA',
	ref_business_unit VARCHAR(32) DEFAULT '-1',
	dsc_business_unit VARCHAR(255) DEFAULT 'NA',
	dsc_business_unit_description TEXT DEFAULT 'NA',
	ref_enterprise VARCHAR(32) DEFAULT '-1',
	dsc_enterprise VARCHAR(255) DEFAULT 'NA',
	ref_holding VARCHAR(32) DEFAULT '-1',
	dsc_holding VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_department (id_department) VALUES (0);

CREATE TABLE erp.d_office (
	id_office SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_office VARCHAR(32) DEFAULT '-1',
	dsc_office VARCHAR(255) DEFAULT 'NA',
	ref_country VARCHAR(32) DEFAULT '-1',
	dsc_country VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_office (id_office) VALUES (0);

CREATE TABLE erp.d_product (
	id_product SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_product VARCHAR(32) DEFAULT '-1',
	dsc_product VARCHAR(255) DEFAULT 'NA',
	dsc_product_description TEXT DEFAULT 'NA',
	ref_subcategory VARCHAR(32) DEFAULT '-1',
	dsc_subcategory VARCHAR(255) DEFAULT 'NA',
	ref_category VARCHAR(32) DEFAULT '-1',
	dsc_category VARCHAR(255) DEFAULT 'NA',
	ref_type VARCHAR(32) DEFAULT '-1',
	dsc_type VARCHAR(255) DEFAULT 'NA',
	dsc_product_type VARCHAR(255) DEFAULT 'NA',
	dsc_tax_category VARCHAR(255) DEFAULT 'NA',
	flg_is_sales CHAR(1) DEFAULT 'N'
);

INSERT INTO erp.d_product (id_product) VALUES (0);

CREATE TABLE erp.d_project (
	id_project SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_project VARCHAR(32) DEFAULT '-1',
	dsc_project VARCHAR(255) DEFAULT 'NA',
	dsc_project_code VARCHAR(255) DEFAULT 'NA',
	ref_line_of_business VARCHAR(32) DEFAULT '-1',
	dsc_line_of_business VARCHAR(255) DEFAULT 'NA',
	dsc_project_cost_type VARCHAR(255) DEFAULT 'NA',
	dsc_project_type VARCHAR(255) DEFAULT 'NA',
	dsc_project_status VARCHAR(255) DEFAULT 'NA',
	dt_start_date DATE,
	dt_end_date DATE,
	dt_planned_end_date DATE,
	id_department INTEGER NOT NULL
);

INSERT INTO erp.d_project (id_project,dt_start_date,dt_end_date,dt_planned_end_date,id_department) VALUES (0,'1900-01-01','1900-01-01','1900-01-01',0);

CREATE TABLE erp.d_resource (
	id_resource SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_resource VARCHAR(32) DEFAULT '-1',
	dsc_resource VARCHAR(255) DEFAULT 'NA',
	dsc_resource_code VARCHAR(255) DEFAULT 'NA',
	nm_reference_number INTEGER DEFAULT -1,
	dsc_tax_id VARCHAR(255) NOT NULL,
	dsc_social_security_id VARCHAR(255) NOT NULL,
	ref_group VARCHAR(32) DEFAULT '-1',
	dsc_group VARCHAR(255) DEFAULT 'NA',
	flg_is_sales_representative CHAR(1) DEFAULT 'N',
	ref_sales_representative VARCHAR(32) DEFAULT '-1',
	dsc_gender VARCHAR(255) NOT NULL,
	dsc_nationality VARCHAR(255) NOT NULL,
	flg_is_productive CHAR(1) DEFAULT 'N',
	dsc_id_number VARCHAR(255) NOT NULL,
	dt_birth_date DATE NOT NULL,
	dsc_marital_status VARCHAR(255) NOT NULL,
	nm_number_of_children INTEGER DEFAULT -1,
	nm_number_of_dependents INTEGER DEFAULT -1,
	flg_is_disabled CHAR(1) DEFAULT 'N',
	flg_is_student_worker CHAR(1) DEFAULT 'N',
	flg_is_employee CHAR(1) DEFAULT 'N',
	flg_is_contractor CHAR(1) DEFAULT 'N',
	dsc_contractor_type VARCHAR(255) NOT NULL
);

INSERT INTO erp.d_resource (id_resource,dsc_tax_id,dsc_social_security_id,dsc_gender,dsc_nationality,dsc_id_number,dt_birth_date,dsc_marital_status,dsc_contractor_type) VALUES (0,'NA','NA','NA','NA','NA','1900-01-01','NA','NA');

CREATE TABLE erp.d_vendor (
	id_vendor SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_vendor VARCHAR(32) DEFAULT '-1',
	dsc_vendor VARCHAR(255) DEFAULT 'NA',
	ref_group VARCHAR(32) DEFAULT '-1',
	dsc_group VARCHAR(255) DEFAULT 'NA',
	dsc_country VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_vendor (id_vendor) VALUES (0);

CREATE TABLE erp.d_document (
	id_document SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_document VARCHAR(32) DEFAULT '-1',
	dsc_document VARCHAR(255) DEFAULT 'NA',
	dsc_document_description TEXT DEFAULT 'NA',
	ref_subbase_type VARCHAR(32) DEFAULT '-1',
	dsc_subbase_type VARCHAR(255) DEFAULT 'NA',
	ref_base_type VARCHAR(32) DEFAULT '-1',
	dsc_base_type VARCHAR(255) DEFAULT 'NA',
	dsc_transaction_type VARCHAR(255) DEFAULT 'NA',
	dsc_category VARCHAR(255) DEFAULT 'NA',
	nm_allocation_status INTEGER DEFAULT -1,
	dsc_allocation_status VARCHAR(255) DEFAULT 'NA',
	flg_is_posted CHAR(1) DEFAULT 'N',
	dsc_paid_by VARCHAR(255) DEFAULT 'NA',
	flg_has_invoice CHAR(1) DEFAULT 'N',
	flg_class_validated CHAR(1) DEFAULT 'N',
	flg_is_allocation_completed CHAR(1) DEFAULT 'N',
	flg_is_allocation_validated CHAR(1) DEFAULT 'N',
	dt_due_date DATE,
	ref_document_status VARCHAR(32) DEFAULT '-1',
	dsc_document_status VARCHAR(255) DEFAULT 'NA',
	flg_is_paid CHAR(1) DEFAULT 'N',
	flg_is_sales_document CHAR(1) DEFAULT 'N'
);

INSERT INTO erp.d_document (id_document) VALUES (0);

CREATE INDEX erp_d_document_ref_document_idx ON erp.d_document (ref_document);

CREATE TABLE erp.d_document_line (
	id_document_line SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_document_line VARCHAR(32) DEFAULT '-1',
	nm_document_line INTEGER DEFAULT -1,
	dsc_document_line TEXT DEFAULT 'NA',
	dsc_paid_by_type VARCHAR(255) DEFAULT 'NA',
	dsc_paid_for VARCHAR(255) DEFAULT 'NA',
	nm_allocation_status INTEGER DEFAULT -1,
	dsc_allocation_status VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_document_line (id_document_line) VALUES (0);

CREATE INDEX erp_d_document_line_ref_document_line_idx ON erp.d_document_line (ref_document_line);

CREATE TABLE erp.d_tax (
	id_tax SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_tax VARCHAR(32) DEFAULT '-1',
	dsc_tax VARCHAR(255) DEFAULT 'NA',
	ref_category VARCHAR(32) DEFAULT '-1',
	dsc_category VARCHAR(255) DEFAULT 'NA',
	dsc_tax_rate VARCHAR(255) DEFAULT 'NA',
	flg_is_sales_tax CHAR(1) DEFAULT 'N',
	dsc_country VARCHAR(255) DEFAULT 'NA',
	dsc_tax_description TEXT DEFAULT 'NA',
	flg_is_withholding CHAR(1) DEFAULT 'N',
	flg_is_not_taxable CHAR(1) DEFAULT 'N',
	flg_is_not_deductible CHAR(1) DEFAULT 'N',
	flg_is_deductible CHAR(1) DEFAULT 'N',
	flg_is_exempt CHAR(1) DEFAULT 'N',
	dsc_tax_code VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_tax (id_tax) VALUES (0);

CREATE TABLE erp.d_price_list (
	id_price_list SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_price_list VARCHAR(32) DEFAULT '-1',
	dsc_price_list VARCHAR(255) DEFAULT 'NA',
	dsc_price_list_description TEXT DEFAULT 'NA'
);

INSERT INTO erp.d_price_list (id_price_list) VALUES (0);

CREATE TABLE erp.d_payment_term (
	id_payment_term SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_payment_term VARCHAR(32) DEFAULT '-1',
	dsc_payment_term VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_payment_term (id_payment_term) VALUES (0);


CREATE TABLE erp.d_allocation_period (
	id_allocation_period SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	nm_year INTEGER NOT NULL,
	dsc_status VARCHAR(255) DEFAULT 'NORMAL'
);

INSERT INTO erp.d_allocation_period (id_allocation_period,nm_year) VALUES (0,1900);

CREATE TABLE erp.d_period_view (
	id_period_view SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	id_allocation_period INTEGER NOT NULL,
	id_allocation_view INTEGER NOT NULL
);

INSERT INTO erp.d_period_view (id_period_view,id_allocation_period,id_allocation_view) VALUES (0,0,0);

CREATE TABLE erp.d_allocation_view (
	id_allocation_view SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_allocation_view VARCHAR(255) DEFAULT 'Current'
);

INSERT INTO erp.d_allocation_view (id_allocation_view) VALUES (0);

CREATE TABLE erp.f_document_lines (
	id_f_document_lines SERIAL PRIMARY KEY,
	-- Primary Key
	ref_document VARCHAR(32) NOT NULL,
	ref_tax_line VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	--
	id_document_date INTEGER NOT NULL, -- FK d_date
	id_document INTEGER NOT NULL, -- FK d_document
	id_order INTEGER NOT NULL, -- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_product INTEGER NOT NULL, -- FK d_product
	id_customer INTEGER NOT NULL, -- FK d_customer
	id_vendor INTEGER NOT NULL, -- FK d_vendor
	id_price_list INTEGER NOT NULL, -- FK d_price_list
	id_payment_term INTEGER NOT NULL, -- FK d_payment_term
	id_sales_representative INTEGER NOT NULL, -- FK d_resource
	id_presales_representative INTEGER NOT NULL, -- FK d_resource
	id_lead_representative INTEGER NOT NULL, -- FK d_resource
	id_company_agent INTEGER NOT NULL, -- FK d_resource
	id_tax INTEGER NOT NULL, -- FK d_tax
	nm_quantity NUMERIC(20,8) NOT NULL,
	nm_price_list NUMERIC(20,8) NOT NULL,
	nm_price_limit NUMERIC(20,8) NOT NULL,
	nm_net_amount NUMERIC(20,8) NOT NULL,
	nm_total_allocated NUMERIC(20,8) NOT NULL,
	nm_tax_amount NUMERIC(20,8) NOT NULL,
	nm_tax_amount_deductable NUMERIC(20,8) NOT NULL,
	nm_tax_amount_withholding NUMERIC(20,8) NOT NULL,
	nm_gross_amount NUMERIC(20,8) NOT NULL,
	nm_real_costs NUMERIC(20,8) NOT NULL,
	nm_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_taxable_amount NUMERIC(20,8) NOT NULL
);

CREATE INDEX erp_f_document_lines_ref_document_idx ON erp.f_document_lines (ref_document);
CREATE INDEX erp_f_document_lines_ref_document_line_idx ON erp.f_document_lines (ref_document_line);

CREATE TABLE erp.f_allocations (
	id_f_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL
);

CREATE INDEX  erp_f_allocations_ref_document_idx ON  erp.f_allocations (ref_document);

CREATE TABLE erp.f_indirect_bu_allocations (
	id_f_indirect_bu_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_document_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_product_allocation INTEGER NOT NULL,-- FK d_product
	id_customer_allocation INTEGER NOT NULL,-- FK d_customer
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_document INTEGER NOT NULL,-- FK d_document
	id_company_agent INTEGER NOT NULL,-- FK d_resource
	id_lead_representative INTEGER NOT NULL,-- FK d_resource
	id_sales_representative INTEGER NOT NULL,-- FK d_resource
	id_presales_representative INTEGER NOT NULL,-- FK d_resource
	id_price_list INTEGER NOT NULL,-- FK d_price_list
	id_payment_term INTEGER NOT NULL,-- FK d_payment_term
	id_product INTEGER NOT NULL,-- FK d_product
	id_order INTEGER NOT NULL,-- FK d_document
	id_document_line INTEGER NOT NULL, -- FK d_document_line
	id_vendor_allocation INTEGER NOT NULL, -- FK d_vendor,
	ref_allocation  VARCHAR(32) NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	ref_document_line VARCHAR(32) NOT NULL,
	ref_line_allocation VARCHAR(32) NOT NULL,
	dt_start_date DATE NOT NULL,
	dt_end_date DATE NOT NULL,
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_total_amount NUMERIC(20,8) NOT NULL,
	id_allocation_period INTEGER NOT NULL, -- FK d_allocation_period
	dsc_updated_by VARCHAR(255) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	allocation_origin VARCHAR(32) DEFAULT 'NA'
);
