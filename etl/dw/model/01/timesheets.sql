CREATE SCHEMA timesheets;

CREATE TABLE timesheets.d_work_type (
	id_work_type SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_work_type VARCHAR(32)DEFAULT '-1',
	dsc_work_type VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO timesheets.d_work_type (id_work_type) VALUES (0);

CREATE TABLE timesheets.f_timesheets (
	id_f_timesheets SERIAL PRIMARY KEY,
	ref_timesheet VARCHAR(32) NOT NULL,		
	id_employee INTEGER NOT NULL, -- FK d_resource
	id_project_leader INTEGER NOT NULL, -- FK d_resource
	id_project_manager INTEGER NOT NULL, -- FK d_resource
	id_analytical_date INTEGER NOT NULL, -- FK d_date
	id_project INTEGER NOT NULL, -- FK d_project
	id_activity INTEGER NOT NULL, -- FK d_project_task
	id_work_type INTEGER NOT NULL, -- FK d_work_type
	id_office INTEGER NOT NULL, -- FK d_office
	dsc_description TEXT,
	dsc_status VARCHAR(255) NOT NULL,
	nm_total_time NUMERIC(20,8) NOT NULL
);

CREATE INDEX timesheets_f_timesheets_ref_timesheet_idx ON timesheets.f_timesheets (ref_timesheet);
