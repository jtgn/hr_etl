CREATE SCHEMA budget;

CREATE TABLE budget.dim_budget(
id_budget INT PRIMARY KEY,
ref_budget_id INT,
budget_name VARCHAR (255),
version INT,
date_from timestamp,
date_to timestamp
);

CREATE TABLE budget.dim_budget_status(
id_budget_status INT PRIMARY KEY,
ref_budget_status_id INT,
budget_status VARCHAR (255),
version INT,
date_from timestamp,
date_to timestamp
);

CREATE TABLE budget.fact_budget(
id_budget INT references budget.dim_budget(id_budget),
id_budget_status INT references budget.dim_budget_status(id_budget_status),
id_analytical_date INT,
id_approval_date INT,
id_creation_date INT,
id_department INT,
ref_enterprise VARCHAR(255),
ref_business_unit VARCHAR(255),
ref_division VARCHAR(255),
ref_department VARCHAR(255),
flg_is_department BOOLEAN,
fls_is_budget_approved BOOLEAN,
flg_is_employee BOOLEAN,
Income NUMERIC,
nm_cost NUMERIC
);
