CREATE TABLE erp.d_user_contact
(
  id_user_contact BIGSERIAL
, version INTEGER
, date_from TIMESTAMP
, date_to TIMESTAMP
, ref_user_contact TEXT
, id_document INTEGER
, ref_document VARCHAR(32)
, dsc_document VARCHAR(255)
, user_name VARCHAR(60)
, user_firstname VARCHAR(60)
, user_lastname VARCHAR(60)
, user_description VARCHAR(255)
, user_mail VARCHAR(255)
, c_bpartner_id VARCHAR(32)
, user_phone VARCHAR(40)
, user_phone2 VARCHAR(40)
)
;CREATE INDEX idx_d_user_contact_lookup ON erp.d_user_contact(ref_user_contact, id_document)
;
CREATE INDEX idx_d_user_contact_tk ON erp.d_user_contact(id_user_contact)
;