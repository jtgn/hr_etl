﻿-- Schema: doc_audit

-- DROP SCHEMA doc_audit;

CREATE SCHEMA doc_audit
  AUTHORIZATION pentahoprd;


-- Table: doc_audit.d_document

-- DROP TABLE doc_audit.d_document;

CREATE TABLE doc_audit.d_document
(
  id_document serial NOT NULL,
  version integer DEFAULT 1,
  date_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  date_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
  ref_document character varying(32) DEFAULT '-1'::character varying,
  dsc_document character varying(255) DEFAULT 'NA'::character varying,
  dsc_document_description text DEFAULT 'NA'::text,
  ref_subbase_type character varying(32) DEFAULT '-1'::character varying,
  dsc_subbase_type character varying(255) DEFAULT 'NA'::character varying,
  ref_base_type character varying(32) DEFAULT '-1'::character varying,
  dsc_base_type character varying(255) DEFAULT 'NA'::character varying,
  dsc_transaction_type character varying(255) DEFAULT 'NA'::character varying,
  dsc_category character varying(255) DEFAULT 'NA'::character varying,
  nm_allocation_status integer DEFAULT (-1),
  dsc_allocation_status character varying(255) DEFAULT 'NA'::character varying,
  flg_is_posted character(1) DEFAULT 'N'::bpchar,
  dsc_paid_by character varying(255) DEFAULT 'NA'::character varying,
  flg_has_invoice character(1) DEFAULT 'N'::bpchar,
  flg_class_validated character(1) DEFAULT 'N'::bpchar,
  flg_is_allocation_completed character(1) DEFAULT 'N'::bpchar,
  flg_is_allocation_validated character(1) DEFAULT 'N'::bpchar,
  flg_is_deleted character(1) DEFAULT 'N'::bpchar,
  dt_due_date date,
  dsc_document_status character varying(255) DEFAULT 'NA'::character varying,
  flg_is_paid character(1) DEFAULT 'N'::bpchar,
  flg_is_sales_document character(1) DEFAULT 'N'::bpchar,
  dsc_sales_representative character varying(255) DEFAULT 'NA'::character varying,
  dsc_lead_representative character varying(255) DEFAULT 'NA'::character varying,
  dsc_order_reference character varying(255) DEFAULT 'NA'::character varying,
  dsc_payment_method character varying(255) DEFAULT 'NA'::character varying,
  ts_update_document timestamp without time zone,
  flg_is_last_version character(1) DEFAULT 'N'::bpchar,
  CONSTRAINT d_document_pkey PRIMARY KEY (id_document)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE doc_audit.d_document
  OWNER TO pentahoprd;

-- Index: doc_audit.d_document_ref_subbase_type_idx

-- DROP INDEX doc_audit.d_document_ref_subbase_type_idx;

CREATE INDEX d_document_ref_subbase_type_idx
  ON doc_audit.d_document
  USING btree
  (ref_subbase_type COLLATE pg_catalog."default");

-- Index: doc_audit.doc_audit_d_document_ref_document_idx

-- DROP INDEX doc_audit.doc_audit_d_document_ref_document_idx;

CREATE INDEX doc_audit_d_document_ref_document_idx
  ON doc_audit.d_document
  USING btree
  (ref_document COLLATE pg_catalog."default");


ALTER TABLE doc_audit.d_document
   ADD COLUMN id_document_erp_bi integer DEFAULT 0;

-- Table: doc_audit.d_document_line

-- DROP TABLE doc_audit.d_document_line;

CREATE TABLE doc_audit.d_document_line
(
  id_document_line serial NOT NULL,
  version integer DEFAULT 1,
  date_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  date_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
  ref_document_line character varying(32) DEFAULT '-1'::character varying,
  nm_document_line integer DEFAULT (-1),
  nm_net_amount numeric(20,8),
  nm_quantity numeric(20,8),
  ref_document character varying(32),
  dsc_document character varying(30),
  dsc_document_description character varying(255),
  ts_update_document timestamp without time zone,
  id_document integer,
  dsc_document_line character varying(2000),
  dsc_paid_by_type text,
  dsc_paid_for character varying(75),
  nm_allocation_status integer,
  dsc_allocation_status text,
  ts_update_document_line timestamp without time zone,
  flg_is_deleted character(1) DEFAULT 'N'::bpchar,
  flg_is_last_version character(1) DEFAULT 'N'::bpchar,
  CONSTRAINT d_document_line_pkey PRIMARY KEY (id_document_line)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE doc_audit.d_document_line
  OWNER TO pentahoprd;

-- Index: doc_audit.doc_audit_d_document_line_ref_document_line_idx

-- DROP INDEX doc_audit.doc_audit_d_document_line_ref_document_line_idx;

CREATE INDEX doc_audit_d_document_line_ref_document_line_idx
  ON doc_audit.d_document_line
  USING btree
  (ref_document_line COLLATE pg_catalog."default");

-- Index: doc_audit.idx_d_document_line_lookup

-- DROP INDEX doc_audit.idx_d_document_line_lookup;

CREATE INDEX idx_d_document_line_lookup
  ON doc_audit.d_document_line
  USING btree
  (ref_document_line COLLATE pg_catalog."default");

-- Index: doc_audit.idx_d_document_line_tk

-- DROP INDEX doc_audit.idx_d_document_line_tk;

CREATE INDEX idx_d_document_line_tk
  ON doc_audit.d_document_line
  USING btree
  (id_document_line);


-- Table: doc_audit.f_audit_allocations

-- DROP TABLE doc_audit.f_audit_allocations;

CREATE TABLE doc_audit.f_audit_allocations
(
  id_f_allocations serial NOT NULL,
  id_analytical_date integer NOT NULL,
  id_document_date integer NOT NULL,
  id_office integer NOT NULL,
  id_project integer NOT NULL,
  id_department integer NOT NULL,
  id_activity integer NOT NULL,
  id_resource integer NOT NULL,
  id_product_allocation integer NOT NULL,
  id_customer_allocation integer NOT NULL,
  id_customer integer NOT NULL,
  id_vendor integer NOT NULL,
  id_document integer NOT NULL,
  id_product integer NOT NULL,
  id_document_line integer NOT NULL,
  id_vendor_allocation integer NOT NULL,
  ref_allocation character varying(32) NOT NULL,
  ref_document character varying(32) NOT NULL,
  ref_document_line character varying(32) NOT NULL,
  ref_line_allocation character varying(32) NOT NULL,
  dt_start_date date NOT NULL,
  dt_end_date date NOT NULL,
  nm_dimension_allocation_amount numeric(20,8) NOT NULL,
  nm_income numeric(20,8) NOT NULL,
  nm_cost numeric(20,8) NOT NULL,
  nm_period_allocation_amount numeric(20,8) NOT NULL,
  nm_total_amount numeric(20,8) NOT NULL,
  id_allocation_period integer NOT NULL,
  dsc_updated_by character varying(255) NOT NULL,
  ts_update timestamp without time zone NOT NULL,
  id_enterprise integer DEFAULT 0,
  CONSTRAINT f_audit_allocations_pkey PRIMARY KEY (id_f_allocations)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE doc_audit.f_audit_allocations
  OWNER TO pentahoprd;

-- Index: doc_audit.doc_audit_f_audit_allocations_id_document_idx

-- DROP INDEX doc_audit.doc_audit_f_audit_allocations_id_document_idx;

CREATE INDEX doc_audit_f_audit_allocations_id_document_idx
  ON doc_audit.f_audit_allocations
  USING btree
  (id_document);

-- Index: doc_audit.doc_audit_f_audit_allocations_ref_document_idx

-- DROP INDEX doc_audit.doc_audit_f_audit_allocations_ref_document_idx;

CREATE INDEX doc_audit_f_audit_allocations_ref_document_idx
  ON doc_audit.f_audit_allocations
  USING btree
  (ref_document COLLATE pg_catalog."default");

-- Index: doc_audit.f_audit_allocations_id_customer_idx

-- DROP INDEX doc_audit.f_audit_allocations_id_customer_idx;

CREATE INDEX f_audit_allocations_id_customer_idx
  ON doc_audit.f_audit_allocations
  USING btree
  (id_customer);


-- staging area


-- Table: sa.audit_allocations_0

-- DROP TABLE sa.audit_allocations_0;

CREATE TABLE sa.audit_allocations_0
(
  id_f_allocations_0 serial NOT NULL,
  ref_allocation character varying(32) NOT NULL,
  ref_line_allocation character varying(32) NOT NULL,
  ref_document_line character varying(32) NOT NULL,
  ref_document character varying(32) NOT NULL,
  ref_activity character varying(32) NOT NULL,
  ref_product character varying(32) NOT NULL,
  ref_office character varying(32) NOT NULL,
  ref_department character varying(32) NOT NULL,
  ref_project character varying(32) NOT NULL,
  ref_customer character varying(32) NOT NULL,
  ref_vendor character varying(32) NOT NULL,
  ref_resource character varying(32) NOT NULL,
  ts_start_date timestamp without time zone NOT NULL,
  ts_end_date timestamp without time zone NOT NULL,
  nm_dimension_allocation_amount numeric(20,8) NOT NULL,
  nm_total_amount numeric(20,8) NOT NULL,
  nm_period_allocation_amount numeric(20,8) NOT NULL,
  nm_allocation_year integer NOT NULL,
  dsc_allocation_status character varying(255) NOT NULL,
  dsc_updated_by character varying(255) NOT NULL,
  ts_update timestamp without time zone NOT NULL,
  ref_enterprise character varying(32) DEFAULT '-1'::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sa.audit_allocations_0
  OWNER TO pentahoprd;

-- Index: sa.sa_audit_allocations_0_ref_document_idx

-- DROP INDEX sa.sa_audit_allocations_0_ref_document_idx;

CREATE INDEX sa_audit_allocations_0_ref_document_idx
  ON sa.audit_allocations_0
  USING btree
  (ref_document COLLATE pg_catalog."default");



-- Table: sa.audit_allocations_1

-- DROP TABLE sa.audit_allocations_1;

CREATE TABLE sa.audit_allocations_1
(
  ref_allocation character varying(32) NOT NULL,
  ref_line_allocation character varying(32) NOT NULL,
  ref_document_line character varying(32) NOT NULL,
  ref_document character varying(32) NOT NULL,
  ref_activity character varying(32) NOT NULL,
  ref_product character varying(32) NOT NULL,
  ref_office character varying(32) NOT NULL,
  ref_department character varying(32) NOT NULL,
  ref_project character varying(32) NOT NULL,
  ref_customer character varying(32) NOT NULL,
  ref_vendor character varying(32) NOT NULL,
  ref_resource character varying(32) NOT NULL,
  ts_start_date timestamp without time zone NOT NULL,
  ts_end_date timestamp without time zone NOT NULL,
  nm_dimension_allocation_amount numeric(20,8) NOT NULL,
  nm_total_amount numeric(20,8) NOT NULL,
  nm_period_allocation_amount numeric(20,8) NOT NULL,
  dt_analytical_date date NOT NULL,
  nm_allocation_year integer NOT NULL,
  dsc_allocation_status character varying(255) NOT NULL,
  dsc_updated_by character varying(255) NOT NULL,
  ts_update timestamp without time zone NOT NULL,
  ref_enterprise character varying(32) DEFAULT '-1'::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sa.audit_allocations_1
  OWNER TO pentahoprd;

-- Index: sa.sa_audit_allocations_1_ref_document_line_idx

-- DROP INDEX sa.sa_audit_allocations_1_ref_document_line_idx;

CREATE INDEX sa_audit_allocations_1_ref_document_line_idx
  ON sa.audit_allocations_1
  USING btree
  (ref_document_line COLLATE pg_catalog."default");

-- reference data

-- Table: rd.audit_allocations

-- DROP TABLE rd.audit_allocations;

CREATE TABLE rd.audit_allocations
(
  ref_allocation character varying(32) NOT NULL,
  ts_update_allocation timestamp without time zone NOT NULL,
  ref_line_allocation character varying(32) NOT NULL,
  ts_update_line_allocation timestamp without time zone NOT NULL,
  flagfield character varying(32) NOT NULL,
  ref_document character varying(32) NOT NULL,
  ts_update_document timestamp without time zone NOT NULL,
  ref_document_line character varying(32) NOT NULL,
  ts_update_document_line timestamp without time zone NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rd.audit_allocations
  OWNER TO pentahoprd;

-- Index: rd.rd_audit_allocations_flagfield_idx

-- DROP INDEX rd.rd_audit_allocations_flagfield_idx;

CREATE INDEX rd_audit_allocations_flagfield_idx
  ON rd.audit_allocations
  USING btree
  (flagfield COLLATE pg_catalog."default");

-- Index: rd.rd_audit_allocations_refs_idx

-- DROP INDEX rd.rd_audit_allocations_refs_idx;

CREATE INDEX rd_audit_allocations_refs_idx
  ON rd.audit_allocations
  USING btree
  (ref_document COLLATE pg_catalog."default", ref_document_line COLLATE pg_catalog."default", ref_line_allocation COLLATE pg_catalog."default", ref_allocation COLLATE pg_catalog."default");

-- Table: rd.audit_allocations_backup

-- DROP TABLE rd.audit_allocations_backup;

CREATE TABLE rd.audit_allocations_backup
(
  ref_allocation character varying(32) NOT NULL,
  ts_update_allocation timestamp without time zone NOT NULL,
  ref_line_allocation character varying(32) NOT NULL,
  ts_update_line_allocation timestamp without time zone NOT NULL,
  flagfield character varying(32) NOT NULL,
  ref_document character varying(32) NOT NULL,
  ts_update_document timestamp without time zone NOT NULL,
  ref_document_line character varying(32) NOT NULL,
  ts_update_document_line timestamp without time zone NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rd.audit_allocations_backup
  OWNER TO postgres;

-- Index: rd.rd_audit_allocations_backup_flagfield_idx

-- DROP INDEX rd.rd_audit_allocations_backup_flagfield_idx;

CREATE INDEX rd_audit_allocations_backup_flagfield_idx
  ON rd.audit_allocations_backup
  USING btree
  (flagfield COLLATE pg_catalog."default");

-- Index: rd.rd_audit_allocations_backup_refs_idx

-- DROP INDEX rd.rd_audit_allocations_backup_refs_idx;

CREATE INDEX rd_audit_allocations_backup_refs_idx
  ON rd.audit_allocations_backup
  USING btree
  (ref_document COLLATE pg_catalog."default", ref_document_line COLLATE pg_catalog."default", ref_line_allocation COLLATE pg_catalog."default", ref_allocation COLLATE pg_catalog."default");

