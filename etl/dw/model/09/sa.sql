CREATE TABLE sa.f_fingerprint (
	ref_fingerprint VARCHAR(32) NOT NULL,
	ts_update_fingerprint TIMESTAMP NOT NULL,
	ref_employee VARCHAR(255) NOT NULL,
	ref_device VARCHAR(255) NOT NULL,
	ref_time VARCHAR(255) NOT NULL,
	ref_date VARCHAR(255) NOT NULL,
	flg_first_entrance VARCHAR(255) NOT NULL,
	flg_last_exit VARCHAR(255) NOT NULL
);

CREATE INDEX sa_f_fingerprint_ref_fingerprint_in_idx ON sa.f_fingerprint(ref_fingerprint);