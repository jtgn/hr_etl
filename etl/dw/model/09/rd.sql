CREATE TABLE rd.f_fingerprint (
	ref_fingerprint VARCHAR(32) NOT NULL,
	ts_update_fingerprint TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE INDEX rd_f_fingerprint_flagfield_idx ON rd.f_fingerprint (flagfield);
CREATE INDEX rd_f_fingerprint_ref_fingerprint_idx ON rd.f_fingerprint(ref_fingerprint);

CREATE TABLE rd.f_fingerprint_backup (
	ref_fingerprint VARCHAR(32) NOT NULL,
	ts_update_fingerprint TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);
