CREATE SCHEMA fingerprint;

CREATE TABLE fingerprint.d_time (
	id_time int NOT NULL DEFAULT '0', -- Ex:2355
	dsc_hour varchar DEFAULT NULL, -- Ex:23
	dsc_meridiem_type varchar,-- Ex:PM
	dsc_twelve_hour varchar,-- Ex:11
	dsc_minutes varchar,-- Ex:55
	dsc_hour_minute varchar,-- Ex:23:55
	PRIMARY KEY (id_time)
);

CREATE TABLE fingerprint.d_device (
	id_device SERIAL PRIMARY KEY, 
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_device VARCHAR(32) DEFAULT '-1',
	dsc_ip_address VARCHAR(32) DEFAULT 'NA', 
	dsc_device_name VARCHAR(255) DEFAULT 'NA',
	nm_device_number INTEGER DEFAULT '-1',
	nm_door_number INTEGER DEFAULT '-1',
	dsc_direction VARCHAR(3) DEFAULT 'NA',
	nm_commport VARCHAR(32) DEFAULT '-1',
	nm_machine_type INTEGER DEFAULT '-1'
);

INSERT INTO fingerprint.d_device (id_device) VALUES (0);

CREATE TABLE fingerprint.f_fingerprint (
	id_f_fingerprint SERIAL PRIMARY KEY,
	ref_fingerprint VARCHAR(32) NOT NULL, -- NK Logid
	id_employee INTEGER NOT NULL, -- FK d_resource
	id_device INTEGER NOT NULL, -- FK d_device
	id_time INTEGER NOT NULL, -- FK d_time
	id_date INTEGER NOT NULL, -- FK d_date
	flg_first_entrance BOOLEAN DEFAULT FALSE, -- Flag for first entrance of user of the day
	flg_last_exit BOOLEAN DEFAULT FALSE -- Flag for last exit of user of the day
);