CREATE TABLE erp.d_account (
	id_account SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_account VARCHAR(32) DEFAULT '-1',
	dsc_account VARCHAR(255) DEFAULT 'NA',
	dsc_account_description VARCHAR(255) DEFAULT 'NA',
	dsc_account_type VARCHAR(255) DEFAULT 'NA',
	dsc_account_sign VARCHAR(255) DEFAULT 'NA',
	dsc_account_level VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_account (id_account) VALUES (0);

CREATE TABLE erp.f_accounting (
	ref_accounting VARCHAR(32) NOT NULL,
	dsc_accounting_description VARCHAR(255) NOT NULL,
	nm_credit NUMERIC(28, 8) NOT NULL,
	nm_debit NUMERIC(28, 8) NOT NULL,
	id_accounting_date INTEGER NOT NULL,
	id_product INTEGER NOT NULL,
	id_document INTEGER NOT NULL,
	id_tax INTEGER NOT NULL,
	id_enterprise INTEGER NOT NULL,
	id_customer INTEGER NOT NULL,
	id_vendor INTEGER NOT NULL,
	id_account INTEGER NOT NULL
);
