 CREATE TABLE rd.f_accounting (
	ref_accounting VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE INDEX rd_f_accounting_flagfield_idx ON rd.f_accounting (flagfield);
CREATE INDEX rd_f_accounting_refs_idx ON rd.f_accounting (ref_accounting);

CREATE TABLE rd.f_accounting_backup (
	ref_accounting VARCHAR(32) NOT NULL,
	ts_update TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

