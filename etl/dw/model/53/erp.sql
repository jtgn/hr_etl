ALTER TABLE erp.d_salary_category ADD COLUMN 
	flg_create_account CHAR(1) DEFAULT 'N'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_contractor_prod VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_net_amt NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_gross_amt NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	flg_local_bp_group VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN  
	flg_is_monthly_value CHAR(1) DEFAULT 'N'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_contractor_tax VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN  
	dsc_end_reason VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	flg_create_prize CHAR(1) DEFAULT 'N'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_work_exempion_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_net_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_net_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_gross_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_gross_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_company_goal_1 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_1_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_company_goal_2 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_2_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_company_goal_3 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_3_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_project_goal_1 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_1_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_project_goal_2 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_2_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_project_goal_3 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_3_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_bu_goal_1 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_1_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_bu_goal_2 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_2_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_bu_goal_3 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_3_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_ind_goal_1 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_1_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_ind_goal_2 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_2_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_ind_goal_3 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_3_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_holiday_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_xmas_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_avg_food_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_national_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_int_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_trans_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_monthly_other_ben_1 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_other_ben_1_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_monthly_other_ben_2 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_other_ben_2_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	dsc_monthly_other_ben_3 VARCHAR(255) DEFAULT 'NA'; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_other_ben_3_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_health_in_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_ad_h_in_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_comm_pl_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_child_allo_value NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_laptop_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_work_in_e_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_1_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_2_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_company_goal_3_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_1_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_2_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_project_goal_3_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_1_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_2_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_bu_goal_3_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_1_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_2_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_ind_goal_3_percent NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_autonomous_tax_penalty NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_st_com_plafond NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_st_hea_in_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_st_laptop_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_st_work_ins NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_daily_cost_max NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_daily_cost_with_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_net_with_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_gross_val NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_gross_val_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_gross_cost NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_gross_cost_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_net_val_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_gross_val_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_annual_gross_cost_prize NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_monthly_st_food_allow NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_food_allowance_days NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_taxable_base_amount NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_base_amount_twelfths NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_total_benefits NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_prize_irs_tax NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_employee_soc_secu_tax NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_employee_irs_tax NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_holders_number NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_dependents_number NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_an_gross_cost_p_no_ben NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_fct_percentage NUMERIC DEFAULT 0; 
 ALTER TABLE erp.d_salary_category ADD COLUMN 
	nm_fgct_percentage NUMERIC DEFAULT 0;

	
ALTER TABLE erp.d_period ADD COLUMN 
	dsc_role VARCHAR(255) DEFAULT 'NA'; 