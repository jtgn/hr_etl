CREATE TABLE erp.f_indirect_project_allocations (
	id_f_indirect_project_allocations SERIAL PRIMARY KEY,
	id_analytical_date INTEGER NOT NULL,-- FK d_date
	id_office INTEGER NOT NULL,-- FK d_office
	id_project INTEGER NOT NULL,-- FK d_project
	id_department INTEGER NOT NULL,-- FK d_department
	id_activity INTEGER NOT NULL,-- FK d_activity
	id_resource INTEGER NOT NULL,-- FK d_resource
	id_customer INTEGER NOT NULL,-- FK d_customer
	id_vendor INTEGER NOT NULL,-- FK d_vendor
	id_product INTEGER NOT NULL,-- FK d_product
	nm_dimension_allocation_amount NUMERIC(20,8) NOT NULL,
	nm_income NUMERIC(20,8) NOT NULL,
	nm_cost NUMERIC(20,8) NOT NULL,
	nm_period_allocation_amount NUMERIC(20,8) NOT NULL,
	id_old_project INTEGER NOT NULL
);

CREATE TABLE erp.d_payment_method (
	id_payment_method SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_payment_method VARCHAR(32) DEFAULT '-1',
	dsc_payment_method VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_payment_method (id_payment_method) VALUES (0);

CREATE TABLE erp.d_financial_account (
	id_financial_account SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_financial_account VARCHAR(32) DEFAULT '-1',
	dsc_financial_account VARCHAR(255) DEFAULT 'NA',
	dsc_financial_account_type VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO erp.d_financial_account (id_financial_account) VALUES (0);

CREATE TABLE erp.f_payments (
	id_f_payment_id SERIAL PRIMARY KEY,
	id_payment_date INTEGER NOT NULL,
	id_vendor INTEGER NOT NULL,
	id_customer INTEGER NOT NULL,
	id_department INTEGER NOT NULL,
	id_project INTEGER NOT NULL,
	dsc_payment_status VARCHAR(255) NOT NULL,
	dsc_payment VARCHAR(255) NOT NULL,
	id_payment_method INTEGER NOT NULL,
	id_financial_account INTEGER NOT NULL,
	nm_receivable NUMERIC(20,8) NOT NULL,
	dsc_payment_description VARCHAR(255) NOT NULL,
	nm_payable NUMERIC(20,8) NOT NULL,
	ref_payment VARCHAR(32) NOT NULL
);

CREATE INDEX erp_f_allocations_id_document_idx ON erp.f_allocations(id_document);

CREATE TABLE erp.d_period (
	id_period SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	ref_period VARCHAR(32) DEFAULT '-1',
	dt_start_date DATE DEFAULT '2000-01-01',
	dt_end_date DATE DEFAULT '2999-12-31',
	nm_budget NUMERIC(20,8) DEFAULT 0
);

INSERT INTO erp.d_period (id_period) VALUES (0);

ALTER TABLE erp.f_human_resources ADD COLUMN id_period INTEGER DEFAULT 0;

ALTER TABLE erp.f_human_resources ADD COLUMN nm_daily_headcount NUMERIC(20,8) DEFAULT 0.0;
