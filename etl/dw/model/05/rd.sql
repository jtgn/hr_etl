CREATE TABLE rd.f_payments (
	ref_payment VARCHAR(32) NOT NULL,
	ts_payment TIMESTAMP NOT NULL,
	ts_payment_line TIMESTAMP NOT NULL,
	ts_allocation TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

CREATE TABLE rd.f_payments_backup (
	ref_payment VARCHAR(32) NOT NULL,
	ts_payment TIMESTAMP NOT NULL,
	ts_payment_line TIMESTAMP NOT NULL,
	ts_allocation TIMESTAMP NOT NULL,
	flagfield VARCHAR(32) NOT NULL
);

ALTER TABLE rd.f_human_resources ADD COLUMN ts_period TIMESTAMP DEFAULT '2000-01-01 00:00:00.000';

ALTER TABLE rd.f_human_resources_backup ADD COLUMN ts_period TIMESTAMP DEFAULT '2000-01-01 00:00:00.000';
