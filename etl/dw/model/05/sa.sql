
CREATE TABLE sa.f_payments (
	ref_payment VARCHAR(32) NOT NULL,
	ref_vendor VARCHAR(32) NOT NULL,
	ref_customer VARCHAR(32) NOT NULL,
	dt_payment_date DATE NOT NULL,
	ref_document VARCHAR(32) NOT NULL,
	dsc_payment_status VARCHAR(255) NOT NULL,
	dsc_payment VARCHAR(255) NOT NULL,
	ref_payment_method VARCHAR(32) NOT NULL,
	ref_financial_account VARCHAR(255) NOT NULL,
	dsc_payment_description VARCHAR(255) NOT NULL,
	nm_receivable NUMERIC(20,8) NOT NULL,
	nm_payable NUMERIC(20,8) NOT NULL
);

ALTER TABLE sa.f_human_resources ADD COLUMN ref_period VARCHAR(32) DEFAULT '-1';
