ALTER TABLE erp.d_project ADD COLUMN flg_is_xpagile_compliant CHAR(1) DEFAULT 'N';

CREATE TABLE erp.d_currency (
	id_currency SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_currency VARCHAR(3) DEFAULT 'NA',
	nm_year INTEGER DEFAULT 0,
	nm_rate NUMERIC(20,8) DEFAULT 1
);

ALTER TABLE erp.d_enterprise ADD COLUMN dsc_currency VARCHAR(3) DEFAULT 'NA';
