ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_addon_license VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_host_license VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_status VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_hosting VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_region VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_tech_contact_address_other VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_bill_contact_address VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_bill_contact_address_other VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_bill_contact_city VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_bill_contact_state VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_bill_contact_postal_code VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_partner_type VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_partner VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_partner_contact VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_partner_email VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE sa.f_atlassian_sales ADD COLUMN dsc_billing_period VARCHAR(255) NOT NULL DEFAULT 'NA';
