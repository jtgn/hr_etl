
ALTER TABLE atlassian.d_tech_contact ADD COLUMN dsc_tech_contact_address_other VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_bill_contact_address VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_bill_contact_address_other VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_bill_contact_city VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_bill_contact_state VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_bill_contact ADD COLUMN dsc_bill_contact_postal_code VARCHAR(255) DEFAULT 'NA';

ALTER TABLE atlassian.d_country ADD COLUMN dsc_region VARCHAR(255) DEFAULT 'NA';

CREATE TABLE atlassian.d_partner (
	id_partner SERIAL PRIMARY KEY,
	version INTEGER DEFAULT 1,
	date_from TIMESTAMP DEFAULT '1900-01-01 00:00:00.000',
	date_to TIMESTAMP DEFAULT '2199-12-31 23:59:59.999',
	dsc_partner_type VARCHAR(255) DEFAULT 'NA',
	dsc_partner VARCHAR(255) DEFAULT 'NA',
	dsc_partner_contact VARCHAR(255) DEFAULT 'NA',
	dsc_partner_email VARCHAR(255) DEFAULT 'NA'
);

INSERT INTO atlassian.d_partner (id_partner) VALUES (0);

ALTER TABLE atlassian.f_sales ADD COLUMN id_partner INTEGER NOT NULL DEFAULT 0;

ALTER TABLE atlassian.f_sales ADD COLUMN dsc_addon_license VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE atlassian.f_sales ADD COLUMN dsc_host_license VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE atlassian.f_sales ADD COLUMN dsc_status VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE atlassian.f_sales ADD COLUMN dsc_hosting VARCHAR(255) NOT NULL DEFAULT 'NA';

ALTER TABLE atlassian.f_sales ADD COLUMN dsc_billing_period VARCHAR(255) NOT NULL DEFAULT 'NA';

