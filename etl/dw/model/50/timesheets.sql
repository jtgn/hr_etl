ALTER TABLE timesheets.f_expenses ADD COLUMN dsc_payment_status VARCHAR(255) DEFAULT 'NA';

ALTER TABLE timesheets.f_expenses ADD COLUMN ts_payment_status_updated TIMESTAMP DEFAULT '2000-01-01 00:00:00';

ALTER TABLE timesheets.f_expenses ADD COLUMN dsc_payment_status_updated_by VARCHAR(255) DEFAULT 'NA';
