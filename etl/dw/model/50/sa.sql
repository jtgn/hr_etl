ALTER TABLE sa.f_timesheets_expenses ADD COLUMN dsc_payment_status VARCHAR(255) NOT NULL;

ALTER TABLE sa.f_timesheets_expenses ADD COLUMN ts_payment_status_updated TIMESTAMP NOT NULL;

ALTER TABLE sa.f_timesheets_expenses ADD COLUMN dsc_payment_status_updated_by VARCHAR(255) NOT NULL;
