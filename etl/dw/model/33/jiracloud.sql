CREATE SCHEMA jiracloud;


CREATE TABLE jiracloud.d_date (
	ID_DATE int NOT NULL DEFAULT '0',
	DATE_VALUE date DEFAULT NULL,
	DATE_FULL varchar(32) DEFAULT NULL,
	DATE_LONG varchar(24) DEFAULT NULL,
	DATE_MEDIUM varchar(16) DEFAULT NULL,
	DATE_SHORT varchar(12) DEFAULT NULL,
	YEAR4 char(4) DEFAULT NULL,
	YEAR2 char(2) DEFAULT NULL,
	DAY_IN_YEAR int DEFAULT NULL,
	DAY_IN_MONTH smallint DEFAULT NULL,
	DAY_IN_WEEK smallint DEFAULT NULL,
	DAY_NAME varchar(12) DEFAULT NULL,
	DAY_ABBRV char(3) DEFAULT NULL,
	WEEK_IN_YEAR int DEFAULT NULL,
	WEEK_IN_MONTH smallint DEFAULT NULL,
	MONTH_NUMBER smallint DEFAULT NULL,
	MONTH_NAME varchar(12) DEFAULT NULL,
	MONTH_ABBRV char(3) DEFAULT NULL,
	QUARTER_NAME char(2) DEFAULT NULL,
	QUARTER_NUMBER smallint DEFAULT NULL,
	SEMESTER_NAME char(3) DEFAULT NULL,
	SEMESTER_NUMBER smallint DEFAULT NULL,
	IS_FIRST_DAY_IN_WEEK char(1) DEFAULT NULL,
	IS_LAST_DAY_IN_WEEK char(1) DEFAULT NULL,
	IS_FIRST_DAY_IN_MONTH char(1) DEFAULT NULL,
	IS_LAST_DAY_IN_MONTH char(1) DEFAULT NULL,
	IS_FIRST_DAY_IN_YEAR char(1) DEFAULT NULL,
	IS_LAST_DAY_IN_YEAR char(1) DEFAULT NULL,
	IS_WEEKEND char(1) DEFAULT NULL,
	YEAR_SEMESTER_NAME char(7) DEFAULT NULL,
	YEAR_QUARTER_NAME char(7) DEFAULT NULL,
	YEAR_MONTH_NUMBER char(8) DEFAULT NULL,
	YEAR_MONTH_ABBRV char(8) DEFAULT NULL,
	YEAR_WEEK_NUMBER char(8) DEFAULT NULL,
	PRIMARY KEY (ID_DATE)
);

CREATE TABLE jiracloud.d_jira_costumer (
	id_jiracostumer SERIAL PRIMARY KEY,
	version integer,
	dsc_clientkey text COLLATE pg_catalog."default",
	dsc_username text COLLATE pg_catalog."default",
	dsc_baseurl text COLLATE pg_catalog."default",
	clientname text COLLATE pg_catalog."default",
	clientnameusername text COLLATE pg_catalog."default",
	date_from timestamp without time zone,
	date_to timestamp without time zone
);

CREATE TABLE jiracloud.d_time (
	"hour" TEXT, 
	ID_TIME DOUBLE PRECISION, 
	MERIDIEM_TYPE TEXT, 
	HOUR_12 TEXT
);

CREATE TABLE jiracloud.etl_executions (
	id_execution SERIAL PRIMARY KEY,
	execution_start_date timestamp without time zone,
	execution_end_date timestamp without time zone,
	etl_start_date timestamp without time zone,
	etl_end_date timestamp without time zone,
	id_job integer,
	status varchar
);

INSERT INTO jiracloud.etl_executions VALUES (0,NULL,NULL,'2016-10-31 00:00:00.000','2016-10-31 00:00:00.000',-1,'OK');

CREATE TABLE jiracloud.f_facts (
	id_execution integer,
	date_tk integer,
	id_jiracostumer integer,
	time_tk integer,
	context varchar,
	baseformat varchar,
	expoutputformat varchar,
	genoutputformat varchar,
	issuescount integer,
	nmrecords integer,
	avgRequestTimeProcessing integer,
	sumRequestTimeProcessing integer,
	maxRequestTimeProcessing integer,
	minRequestTimeProcessing integer,
	avgRequestLatency integer,
	maxRequestLatency integer,
	minRequestLatency integer,
	sumRequestLatency integer,
	avgRequestTimeSpent integer,
	maxRequestTimeSpent integer,
	minRequestTimeSpent integer,
	sumRequestTimeSpent integer,
	PRIMARY KEY(id_execution,date_tk,id_jiracostumer,time_tk,context,baseformat,expoutputformat,genoutputformat)
);
