#!/bin/bash

. ~/.profile
. /etc/profile

export LC_CTYPE="en_US.UTF-8"

BASEDIR=$(dirname $0)

cd $BASEDIR

cd scripts

./doc_audit_etl.sh
