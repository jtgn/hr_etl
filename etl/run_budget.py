#!/usr/bin/python3

'''
install pip -  sudo apt-get install -y python-pip
export LC_ALL=C when have the error unsupported locale settings
install mysql connector python3 -m pip install mysql-connector
'''

from __future__ import print_function
import subprocess
import mysql.connector
from datetime import datetime, date, time, timedelta

mydb = mysql.connector.connect(
    host="mystique",
    user="xpecp_prd_ro",
    passwd="wE@7thuQaTHA",
    database="xpecp_prd"
)

#print(mydb)

mycursor = mydb.cursor()

mycursor.execute("select max(date) from budget_history")

myresult = mycursor.fetchall()

for x in myresult:
    dbDate = x

now = datetime.now() - timedelta(minutes=5)
print(dbDate[0])
print(now)

if now < dbDate[0]:
    print("Execute")
    subprocess.call("./run_budget.sh", shell=True)
else:
    print("Do Nothing")
