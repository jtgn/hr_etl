#/bin/bash

. ~/.profile
. /etc/profile

export LC_CTYPE="en_US.UTF-8"

BASEDIR=$(dirname $0)

cd $BASEDIR

cd scripts

./erp_etl.sh

./timesheets_etl.sh

./atlassian_etl.sh

./xpagile_etl.sh

#./crm_etl.sh

#./fingerprint_etl.sh

#./perfomance_etl.sh

#./socialcontent_etl.sh

./cp_etl.sh

./jiracloud_etl.sh

./prospects_etl.sh
