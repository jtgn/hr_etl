#!/bin/bash

BASEDIR=$(dirname $0)

cd $BASEDIR

. ./config
. ./functions

var=`psql -t -h polaris -p 5433 -U xperp -d xperp -c "select count(*) from xpkms_etl_request where status='SCH';"`

echo $var

if  [ $var -eq 0 ];
	then echo "Nothing to DO"
	else ./kitchen.sh -file="${KETTLE_HOME}/chains/scheduler.kjb"
fi
